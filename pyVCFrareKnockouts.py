"""
Apply similar analysis of complete knockouts in human by Lim et al 2013 "Rare Complete Knockouts in Humans: Population Distribution
and Significant Role in Autism Spectrum Disorders".

28 Feb 2013
sa9@sanger.ac.uk
"""

import pyVCFtoolbox as tlbx
import pysam
import sys


trios = {}
sampleIndices = {} # the index value in VCF for each sample, index=sampleId
sampleIds = {} #sampleId=index
LoFdict = {}

def parsePed(path):
    """

    :param path: path to ped file (familyId, sampleId, fatherId, motherId, gender)
    :return: ped dictionary where the key is familyId,sampleId
    """
    f = open(path,'r')
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            sampleId = line[1]
            fatherId = line[2]
            motherId = line[3]
            gender = line[4]
            if fatherId not in ['0',''] and motherId not in ['0','']:
                trios[sampleId] = {'fatherId':fatherId,
                                   'motherId':motherId,
                                   'gender':gender}
    f.close()


def getParentGenotype(childId, line):
    try:
        motherId = trios[childId]['motherId']
        fatherId = trios[childId]['fatherId']
        motherIdx = sampleIds[motherId]
        fatherIdx = sampleIds[fatherId]
    except KeyError:
        return None, None # one or both parents are not in the VCF file

    motherGT = tlbx.getGT(line[8],line[motherIdx])
    fatherGT = tlbx.getGT(line[8],line[fatherIdx])
    return motherGT, fatherGT


def parseVCF(path):
    f = tlbx.openFile(path)
    lofClass = tlbx.getVarClass("lof")
    print "\t".join(['#sampleId', "chrom","pos", "ref", "alt", "GN", "CQ","OKG_AF",
                     "COHORT_AF", "ESP_AF", "DDD_AF", "CHD_AF", "childGT","motherGT","fatherGT", "childGender"])
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            chrom = line[0]
            pos = line[1]
            ref = line[3]
            alt = line[4]
            FORMAT1 = line[8]
            # if chrom == 'X':
            # for altIdx, alt in enumerate(tlbx.getAlts(line[4])):
            if "," not in alt: # ignore multi-allelic sites
                varType = tlbx.getVarType(ref,alt)
                if varType == 'snv':
                    CQ, AA, GN, OKG_AF, COHORT_AF, ESP_AF, DDD_AF, CHD_AF= tlbx.getInfo(['VCQ','VGN', "VAA",
                                                                              '1KG_AF',
                                                                              'UK10K_cohort_AF',
                                                                              'ESP_AF', 'DDD_AF',
                                                                              "CHD_AF"],line[7], 0)
                    if "ESSENTIAL_SPLICE_SITE" in CQ or "STOP_GAINED" in CQ: #CQ == "SYNONYMOUS_CODING"
                        for idx, FORMAT2 in enumerate(line):
                            if idx > 8:
                                sampleGT = tlbx.getGT(FORMAT1,FORMAT2)
                                sampleId = sampleIndices[idx]
                                if sampleGT != 0: # not 0/0
                                    if sampleId in trios: # i.e. this is a child
                                        motherGT, fatherGT = getParentGenotype(sampleId, line)
                                        if motherGT or fatherGT: #both are available in the VCF file
                                            childGender = trios[sampleId]['gender']
                                            newLine = [sampleId, chrom,pos, ref, alt, GN, CQ,OKG_AF,
                                                       COHORT_AF, ESP_AF, DDD_AF, CHD_AF, sampleGT,motherGT,fatherGT,
                                                       childGender]
                                            print "\t".join([str(x) for x in newLine])
        else:
            if line.startswith("#CHROM"):
                line = line.strip().split("\t")
                for idx, sampleId in enumerate(line):
                    if idx > 8:
                        sampleIndices[idx] = sampleId
                        sampleIds[sampleId] = idx




def main():
    pedPath = "/Users/Macia/Dropbox/Team29/MyProjects/CHD/MERGED/24-02-2013/TABLES/CHD_MASTER_TABLE_24Feb2013.txt"#sys.argv[1]
    vcfPath = "/Users/Macia/Dropbox/Team29/MyProjects/CHD/MERGED/24-02-2013/4.singleVCF_with_new_AFs/chd_643_exomes_5AFs.vcf.gz"#sys.argv[2]
    parsePed(pedPath)
    parseVCF(vcfPath)

main()