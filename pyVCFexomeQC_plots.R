# This script reads the output of counts.py and plot varias releaations.
# Date: 3 March 2013
# Version: 0.5 exome
# Author: Saeed Alturki, sa9@sanger.ac.uk
# Save two PDFs only (8 plots in one (SNV) and 4 for indels in the second)
# HET/HOM ratio added
# Needs two arguments 
# (A) A path to QC_table.txt
# (B) A path to a directory to save two plots
###############################################################################

options <- commandArgs(trailingOnly = TRUE)

data <- read.table(options[1], header = T, sep="\t") 
plots_output_path <- options[2]

save_plot <- function(pdf_name){
	pdf_path <- paste(plots_output_path , pdf_name, sep='')
	dev.copy(pdf,pdf_path, width = 14, height = 10)
	dev.off()
}


no_samples <- seq(1,dim(data)[1]) # for plotting X axis

# plot total number of variants (SNVs+Indels)
pdf(paste(plots_output_path, 'SNV_plots.pdf', sep=''),width = 14, height = 10)

par(mfrow=c(2,4))
plot(no_samples,data$total_pass_non_pass,  type="p",lty="solid", ylim=c(0,130000), xlab="Samples", ylab="Variant frequency per sample", col="black", main="Variant frequency (SNVs + indels) per sample")
lines(no_samples,data$non_pass,  type="p",lty="solid", col="red",pch=2)
lines(no_samples,data$pass,  type="p",lty="solid", col="blue", pch=3)

legend('right', legend=c( "Total", "Non Pass", "Pass"), col=c( "black", "red", "blue"), pch=c(1,2,3), cex=0.5)


#SNVs
plot(no_samples,data$total_snv,  type="p",lty="solid", ylim=c(10000,120000), xlab="Samples", ylab="SNVs frequency per sample", col="blue", main="SNVs frequency per sample")


# SNVs common / rare

plot(no_samples,data$total_common_snv_prcnt,  type="p",lty="solid", ylim=c(80,100), xlab="Samples", ylab="Common SNV ", col="blue", main="% of SNVs as common per sample")


# SNVs LOF 	

plot(no_samples,data$snv_common_lof,  type="p",lty="solid", ylim=c(0,450), xlab="Samples", ylab="LOF freq", col="navy", main="LOF frequency (rare vs. common) per sample")
lines(no_samples,data$snv_rare_lof,  type="p",lty="solid", ylim=c(0,450), col="red")
legend("topright", legend=c( "common LOF", "rare LOF"), col=c("navy", "red"), lty=c(1,1), cex=0.5)


# SNVs  Func / silent / others
plot(no_samples,data$snv_common_functional,  type="p",lty="solid", ylim=c(0,75000), xlab="Samples", ylab="SNVs class frequency per sample", col="green", main="Functional, Silent and other variant classes per sample" )
lines(no_samples,data$snv_common_silent,  type="p",lty="solid", ylim=c(0,75000), xlab="Samples", ylab="SNVs class frequency per sample", col="navy")
lines(no_samples,data$snv_common_others,  type="p",lty="solid", ylim=c(0,75000), xlab="Samples", ylab="SNVs class frequency per sample", col="orange")
legend("topright", legend=c( "Functional", "Silent", "Others"), col=c("green", "navy", "orange"), lty=c(1,1,1), cex=0.5)

# Ts/Tv
plot(no_samples,data$snv_Ts_Tv_ratio,  type="p",lty="solid", ylim=c(2,4), xlab="Samples", ylab="Ts/Tv ratio per sample", col="red", main="Ts/Tv ratio per sample")


# HOM and HET
plot(no_samples, data$total_HET, type="p",lty="solid", ylim=c(0,75000), xlab="Samples", ylab="Frequency per sample", col="navy", main="HET and HOM counts")
lines(no_samples,data$total_HOM,  type="p",lty="solid", ylim=c(0,75000), xlab="Samples", col="orange")
legend("bottomright", legend=c( "HET", "HOM"), col=c("navy", "orange"), lty=c(1,1,1), cex=0.5)

# HET/HOM ratio
het_hom_ratio <- data$total_HET / data$total_HOM
plot(no_samples, het_hom_ratio, type="p",lty="solid", ylim=c(0,5), xlab="Samples", ylab="HET/HOM ratio", col="red", main="HET/HOM ratio")

dev.off()


#indels

pdf(paste(plots_output_path, 'INDEL_plots.pdf', sep=''),width = 14, height = 10)

par(mfrow=c(2,2))
plot(no_samples,data$total_indel,  type="p",lty="solid", ylim=c(0,20000), xlab="Samples", ylab="Indels frequency per sample", col="red", main="Indel frequency per sample")



# Indels common / rare
plot(no_samples,data$total_common_indel_prcnt,  type="p",lty="solid", ylim=c(0,99), xlab="Samples", ylab="% of indels as common per sample", col="red", main="% of indels as common per sample (1KG,ESP or Cohort)")


# indels common and rare coding
plot(no_samples,data$indel_common_coding,  type="p",lty="solid", ylim=c(0,650), xlab="Samples", ylab="Indel frequency", col="navy", main="Coding indels (rare vs. common) per sample")
lines(no_samples,data$indel_rare_coding,  type="p",lty="solid", col="red")
legend("topright", legend=c( "common coding", "rare  coding"), col=c( "navy", "red"), lty=c(1,1), cex=0.5)



# n3/nn3
plot(no_samples,data$indel_n3_nn3_ratio,  type="p",lty="solid", ylim=c(0,2.5), xlab="Samples", ylab="n3/non-n3 ratio per sample", col="red",  main="Coding indel (multiple of 3 vs. non-multiple of 3) per sample")

#save_plot('Indel_plots.pdf')
dev.off()


