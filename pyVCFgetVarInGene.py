"""
Given a merge VCF file  and gene , get count of hom,het and missing per locus in that gene
8 Dec 2012
sa9@sanger.ac.uk
"""

import sys
import pyVCFtoolbox as tlbx

try:
    from cruzdb import Genome
except:
    try:
        sys.path.append("/nfs/users/nfs_s/sa9/.local//lib/python2.6/site-packages/cruzdb")
        from cruzdb import Genome
    except:
        print "Please install cruzdb module and try again."
        sys.exit(1)

try:
    import pysam
except:
    try:
        sys.path.append("/nfs/users/nfs_s/sa9/.local/lib/python2.6/site-packages/pysam-0.5-py2.6-linux-x86_64.egg/")
        import pysam
    except:
        print "pysam module is not installed. Please try to install it and start again."
        print "http://code.google.com/p/pysam/"
        sys.exit(1)


ped = {} # all samples, relations and phenotypes
sampleIndices = {} # vcf index to sanger Id
sampleIds = {} #sanger Id to vcf index

resultsBySample = {}
resultsByGene   = {}

cq = {"functional":["NON_SYNONYMOUS_CODING"],
      "lof":["COMPLEX_INDEL" , "STOP_GAINED","FRAMESHIFT_CODING", "ESSENTIAL_SPLICE_SITE",  "STOP_LOST"],
      "coding":["COMPLEX_INDEL" , "STOP_GAINED","FRAMESHIFT_CODING", "ESSENTIAL_SPLICE_SITE", "NON_SYNONYMOUS_CODING", "STOP_LOST"]
}

def loadPED(path):
    f = open(path, 'r')
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            sampleId = line[1]
            phenotype= line[5]
            study    = line[7]
            if phenotype not in ['','0']:
                ped[sampleId] = study
    f.close()

def mapSampleIdToVcfHeader(header):
    for line in header:
        if line.startswith("#CHROM"):
            line = line.split("\t")
            for idx, sampleId in enumerate(line):
                if idx > 8:
                    if sampleId in ped:
                        sampleIds[idx] = sampleId
                        sampleIndices[sampleId] = idx



def getGeneBoundaries(geneName):
    g = Genome(db="hg19")
    gene = g.refGene.filter_by(name2=geneName).first()
    print "#%s:%d-%d" % (gene.chrom[3:],gene.start,gene.end)
    return gene.chrom[3:],gene.start,gene.end


def getSamplesIds(line):
    global resultsBySample
    for idx, sampleId in enumerate(line):
        if idx > 8:
            resultsBySample[idx] = {'id':sampleId,
                                    'snv':{
                                        'LOF': {"hom":0,"het":0},
                                        'FNC': {"hom":0,"het":0},
                                        },
                                    'indel':{
                                        'LOF': {"hom":0,"het":0},
                                        'FNC': {"hom":0,"het":0},
                                        }
            }

def getGTidx(format1):
    for gt_idx, tag in enumerate(format1.split(":")):
        if tag == "GT":
            break
    return gt_idx


def parseGT(format2, gtIdx, altIdx):
    gt = format2.split(":")[gtIdx]
    if len(gt) == 1: # GT on X or Y?
        gt = gt + "/" +  gt
    if gt[0] == gt[2]: #hom
        if gt[0] == ".":
            return "missing"
        elif gt[0] == "0":
            return "ref"
        elif str(altIdx+1) in gt:
            return "hom"
    elif str(altIdx+1) in gt:
        return "het"
    else:
        return "missing"

def isIndel(ref,alt):
    if len(ref) == len(alt) == 1:
        return False
    else:
        return True

def parseINFO(info, tags):
    holder = dict(zip(tags,['0' for tag in tags]))
    for item in info.split(";"):
        try:
            tag, value = item.split('=')
            if tag in holder:
                if tag == "AF_MAX":
                    if value.startswith(".,"): # this is to avoid AF_MAX=.,0.237533 . Not sure why there is '.,'!!
                        value = value[2:]
                    if value.endswith(",."): # this is to avoid AF_MAX=0.273481,.
                        value = value[:-2]
                holder[tag] = value
        except:
            pass
    return [holder[tag] for tag in tags]

def getCQclass(CQ):
    if CQ in cq['lof']:
        return 'LOF'
    if CQ in cq['functional']:
        return 'FNC'

def populateResults(GN, CQ, varType, gtIdx, line):
    # every variant reaches here is rare/novel
    for idx, format2 in enumerate(line):
        if idx > 8:
            GT       = parseGT(format2, gtIdx)
            if GT not in ['missing','ref'] :
                CQ_class = getCQclass(CQ)
                resultsBySample[idx][varType][CQ_class][GT] += 1

                if not resultsByGene.has_key(GN):
                    resultsByGene[GN] = {'id':GN,
                                         'snv':{
                                             'LOF': {"hom":0,"het":0},
                                             'FNC': {"hom":0,"het":0},
                                             },
                                         'indel':{
                                             'LOF': {"hom":0,"het":0},
                                             'FNC': {"hom":0,"het":0},
                                             }
                    }
                resultsByGene[GN][varType][CQ_class][GT]  += 1

def GTcountingTEMP(gtIdx, line, altIdx):
    hom_count = 0
    het_count = 0
    mis_count = 0
    for idx in sampleIds:
        format2 = line[idx]
        GT       = parseGT(format2, gtIdx, altIdx)
        if GT == 'missing':
            mis_count += 1
        elif GT == 'hom':
            hom_count += 1
        elif GT == 'het':
            het_count += 1
    return hom_count, het_count, mis_count



def parseVCF(lines):
    print "\t".join(['chrom','pos','ref','alt','QUAL','FILTER',
                     'VGN','VCQ', 'GN','CQ','AF_MAX',"DDD_AF","CHD_AF","ESP_AF","UK10K_cohort_AF",
                     'PolyPhen', 'SIFT','Condel','GERP','VAA','VPP',
                     'hom_count','het_count','mis_count'])

    tags = ['VGN','VCQ', 'GN','CQ','AF_MAX',"DDD_AF","CHD_AF","ESP_AF","UK10K_cohort_AF",
            'PolyPhen','SIFT','Condel','GERP','VAA', 'VPP']

    for line in lines:
        if line.startswith("#CHROM"):
            line = line.strip().split("\t")
            getSamplesIds(line)
        elif not line.startswith("#"):
            line = line.strip().split("\t")
            chrom = line[0]
            pos = line[1]
            ref = line[3]
            QAL = line[5]
            FLR = line[6]

            for altIdx, alt in enumerate(tlbx.getAlts(line[4])):
                basicValues = [chrom, pos, ref, alt, QAL, FLR]
                basicValues.extend(tlbx.getInfo(tags,line[7], altIdx))
                gtIdx = getGTidx(line[8])

                basicValues.extend(GTcountingTEMP(gtIdx, line, altIdx))
                print "\t".join([str(x) for x in basicValues])


def printResults(whichResult):
    if whichResult == 'sample':
        holder = resultsBySample
    elif whichResult == 'gene':
        holder = resultsByGene

    print "\t".join(['#' + whichResult,
                     'SNV_LOF_HOM','SNV_LOF_HET','SNV_FNC_HOM','SNV_FNC_HET',
                     'INDL_LOF_HOM','INDL_LOF_HET','INDL_FNC_HOM','INDL_FNC_HET',
                     ])

    for idx in sorted(holder):
        resultLine = [holder[idx]['id']]
        for varType in ['snv','indel']:
            for cq in ['LOF','FNC']:
                for gt in ['hom','het']:
                    resultLine.append(str(holder[idx][varType][cq][gt]))
        print "\t".join(resultLine)


def main():

    loadPED('/Users/Macia/Dropbox/Team29/MyProjects/CHD/MERGED/24-02-2013/TABLES/CHD_MASTER_TABLE_24Feb2013.txt')#sys.argv[3])
    geneName = sys.argv[1]
    vcfFile = pysam.Tabixfile("/Users/Macia/Dropbox/Team29/MyProjects/CHD/MERGED/24-02-2013/4.singleVCF_with_new_AFs/chd_643_exomes_5AFs.vcf.gz")#sys.argv[2])
    chrom, start,end = getGeneBoundaries(geneName)
    header = vcfFile.header
    lines  = vcfFile.fetch(chrom,start,end)
    mapSampleIdToVcfHeader(header)
    parseVCF(lines)
    #printResults('sample')
    #printResults('gene')

main()