#!/usr/bin/env python
"""
Given a VEP output from stdout this script will extract the VEP new CSQ field and chose the mose sever consequence
and split the values into tags/values in the INFO field. It will also adds the headers

This used only with VEP 2.8 (Ensembl v70) human. NOT TESTED WITH ANY OTHER VERSION

gzcat SC_SBMWES5319110.vcf.gz | ~/Downloads/VEP/variant_effect_predictor.pl -o stdout --cache --dir ~/Downloads/VEP/cache/ --everything --offline  --vcf --compress gzcat | ./pyVCFvep2.8.py | bgiz -c


Make sure you use the following options for VEP
if tlbx.getPlatform() == 'local':
    vepPath = '~/Downloads/VEP/variant_effect_predictor.pl'
    vepCache = '~/Downloads/VEP/cache/'
    compress = '--compress gzcat' # for Mac Os option with VEP
else:#farm
    vepPath  = '~sa9/chd/resources/VEP/v70/variant_effect_predictor/variant_effect_predictor.pl'
    vepCache = '~sa9/chd/resources/VEP/v70/cache/'
    compress = '' # On unix , vep will use zcat be default. No need to set this option when running on the farm



10 Mar 2013
sa9@sanger.ac.uk
"""


import pyVCFtoolbox as tlbx
import sys
import argparse



def extractVEPFields(line, f_type):
    # split by " Format: "
    # remove the trailing "">"
    # split by "|"
    vepFields = line.split(" Format: ")[1][:-3].split("|")
    if f_type == 'vcf':
        #write header
        for field in vepFields:
            print '##INFO=<ID=%s,Number=.,Type=String,Description="VEP2.8 %s">' % (field, field)
    return vepFields

def getHeavySOTerm(multiSOTerms):
    if "&" in multiSOTerms:
        multiSOTerms = multiSOTerms.split("&")
        heaviestTerm = None
        for SOTerm in multiSOTerms:
            if not heaviestTerm:
                heaviestTerm = SOTerm
            else:
                oldWeight = tlbx.getSOTermWeightVEP2_8(heaviestTerm)
                newWeight = tlbx.getSOTermWeightVEP2_8(SOTerm)
                if newWeight > oldWeight:
                    heaviestTerm = SOTerm
        return heaviestTerm
    else:
        return multiSOTerms

def selectMostSevere(CSQ, vepFields, f_type):
    mostSevere = {}
    for record in CSQ.split(","):
        record = dict(zip(vepFields, record.split("|")))
        record['Consequence'] = getHeavySOTerm(record['Consequence'])
        if not mostSevere:
            mostSevere = record
        else:
            oldWeight = tlbx.getSOTermWeightVEP2_8(mostSevere['Consequence'])
            newWeight = tlbx.getSOTermWeightVEP2_8(record['Consequence'])
            if newWeight > oldWeight:
                mostSevere = record
    if f_type == 'vcf':
        newCSQ = []
        for tag in vepFields:
            if mostSevere[tag] != '':
                newCSQ.append("%s=%s" % (tag, mostSevere[tag]))
        return ";".join(newCSQ)
    elif f_type == 'bed':
        newCSQ = []
        for tag in vepFields:
            if mostSevere[tag] != '':
                newCSQ.append(mostSevere[tag])
            else:
                newCSQ.append(".") # empty value
        return newCSQ

# CSQ = 'A|ENSG00000188976|ENST00000496938|Transcript|upstream_gene_variant||||||rs2272757|||NOC2L|||||685||||A:0.4739||||,A|ENSG00000187634|ENST00000342066|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||1672|YES|||A:0.4739|ENSP00000342313||CCDS2.2|,A|ENSG00000188976|ENST00000327044|Transcript|synonymous_variant|1893|1843|615|L|Ctg/Ttg|rs2272757|16/19||NOC2L||||||YES|||A:0.4739|ENSP00000317992||CCDS3.1|,A|ENSG00000187634|ENST00000464948|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||3355||||A:0.4739||||,A|ENSG00000187634|ENST00000466827|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||3445||||A:0.4739||||,A|ENSG00000187634|ENST00000474461|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||3253||||A:0.4739||||,A|ENSG00000187634|ENST00000455979|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||1988||||A:0.4739|ENSP00000412228|||,A|ENSG00000188976|ENST00000483767|Transcript|non_coding_exon_variant&nc_transcript_variant|699|||||rs2272757|2/5||NOC2L|||||||||A:0.4739||||,A|ENSG00000187634|ENST00000478729|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||4074||||A:0.4739||||,A|ENSG00000188976|ENST00000477976|Transcript|non_coding_exon_variant&nc_transcript_variant|3290|||||rs2272757|14/17||NOC2L|||||||||A:0.4739||||,A|ENSG00000187634|ENST00000341065|Transcript|downstream_gene_variant||||||rs2272757|||SAMD11|||||1672||||A:0.4739|ENSP00000349216|||'
# line = '##INFO=<ID=CSQ,Number=.,Type=String,Description="Consequence type as predicted by VEP. Format: Allele|Gene|Feature|Feature_type|Consequence|cDNA_position|CDS_position|Protein_position|Amino_acids|Codons|Existing_variation|EXON|INTRON|HGNC|MOTIF_NAME|MOTIF_POS|HIGH_INF_POS|MOTIF_SCORE_CHANGE|DISTANCE|CANONICAL|SIFT|PolyPhen|GMAF|ENSP|DOMAINS|CCDS|CELL_TYPE">'
# f_type = 'vcf'
# vepFields = extractVEPFields(line, f_type)
# selectMostSevere(CSQ, vepFields, f_type)

def parseInputFile( f_path, columnIndexes):
    f = tlbx.openFile(f_path)

    isValidColumns = False
    lineCounter = 0
    for line in f:
        lineCounter += 1
        if lineCounter == 1:
            tlbx.isHeaderExists(line)
            f_type  = tlbx.isBEDorVCF(line)
        if not line.startswith("#"):
            line  = line.strip().split("\t")
            if not isValidColumns:
                isValidColumns = tlbx.isValidColumns(columnIndexes,line)
                chrIdx,posIdx,refIdx,altIdx = [int(x) for x in columnIndexes.split(',')]

            alts  = tlbx.getAlts(line[altIdx])
            for altIdx, alt in enumerate(alts):

                if f_type == 'vcf':
                    CSQ = tlbx.getInfo(['CSQ'],line[7], altIdx)
                    #clean INFO from any known consequence tags from previous annotation
                    for tag in ['CSQ', 'CQ', 'VCQ', 'VCQNC']:
                        line[7] = tlbx.deleteInfoTag(line[7],tag)
                    newCSQ = selectMostSevere(CSQ, vepFields, f_type)
                    line[7] += ";" + newCSQ
                    print "\t".join(line)
                elif f_type =='bed':
                    CSQ = line[7].split("CSQ=")[1]
                    # parse VEP output
                    newCSQ = selectMostSevere(CSQ, vepFields, f_type)
                    line[7] =  line[7].split("CSQ=")[0]
                    line.extend(newCSQ)
                    print "\t".join(line)
                    break # in bed file we should NOT see multiple ALTs
        else:
            #print header for vcf or bed
            tmpLine = '##INFO=<ID=CSQ,Number=.,Type=String,Description="Consequence type as predicted by VEP. Format: Allele|Gene|Feature|Feature_type|Consequence|cDNA_position|CDS_position|Protein_position|Amino_acids|Codons|Existing_variation|DISTANCE|CELL_TYPE">'
            if tmpLine.startswith("##INFO=<ID=CSQ,"):
                vepFields = extractVEPFields(tmpLine, f_type)
            else:
                if f_type == 'vcf':
                    print line.strip()
                elif f_type == 'bed':
                    if line.startswith("#CHROM"):
                        line = line.strip().split("\t")
                        line.extend(vepFields)
                        print "\t".join(line)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', help='A path to the VCF or BED file or stdin if empty', default=None)
    parser.add_argument('-c', '--columnIndexes', help='A 0-based index for chrom, pos, ref and alt columns'
                                                      ' (comma separated) default is 0,1,3,4', default='0,1,3,4')

    args           = parser.parse_args()
    f_path         = args.inputFile
    columnIndexes  = args.columnIndexes
    if not sys.stdin.isatty(): # there is inputFile or there is stdin
        parseInputFile(f_path, columnIndexes)
    else:
        parser.print_help()
        sys.exit(1)



