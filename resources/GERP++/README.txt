10 Mar 2013
sa9@sanger.ac.uk


Goal:
----

Generate BED files for GERP++ to be used for annotating VCF and DNG files


Steps:
------


cd /nfs/users/nfs_s/sa9/chd/resources/gerp++

# download the GERP++ files and decompress them
wget http://mendel.stanford.edu/SidowLab/downloads/gerp/hg19.GERP_scores.tar.gz
tar zxfv hg19.GERP_scores.tar.gz 

#convert to bed format and add chr,from, to then begzip and generate a tabix index file
# from the rates files I will extract the RS column only (the 2nd column). This took about 15 min

for fileName in `ls *rates`;do chr=${fileName:3:${#fileName}-13}; bsub -oo conver2bed.log "cat -n $fileName | awk '{print \"$chr\\t\",\$1,\"\\t\",\$1,\"\\t\",\$3}' | bgzip -c > gerp_score.$chr.bed.gz; tabix -p bed  gerp_score.$chr.bed.gz";done

#delete original files
rm *.rates, rm hg19.GERP_scores.tar.gz