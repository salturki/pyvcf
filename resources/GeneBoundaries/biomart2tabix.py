'''
Give a file of genes boudaries from biomart, rewrite it for tabix and bgzip.


Usage:

python biomart2tabix.py | bgzip -c >  biomart_genes.bed.gz
tabix -p bed biomart_genes.bed.gz

4 Feb 2013
sa9@sangre.ac.uk
'''

f = open('GeneBoundariesBioMart_known_coding4Feb2013.txt','r')

chrom_dict = {}

for line in f:
    if line.startswith("Associated"):
        print "#%s" % line.strip()
    else:
        line   = line.strip().split("\t")
        chrom  = line[2]
        start  = line[3]
        end    = line[4]
        gene   = line[0]
        ensmbl = line[1]
        strand = line[5]
        band   = line[6]
        trans  = line[7]
        biotype= line[8]
        status = line[9]
        if not chrom_dict.has_key(chrom):
            chrom_dict[chrom] = {(int(start),int(end)):[chrom,start,end,gene,ensmbl,strand,band,trans,biotype,status]}
        else:
            if not chrom_dict[chrom].has_key((int(start),int(end))):
                chrom_dict[chrom][(int(start),int(end))] = [chrom,start,end,gene,ensmbl,strand,band,trans,biotype,status]
            else:
                print line

for chrom in sorted(chrom_dict):
    for pos in sorted(chrom_dict[chrom]):
        #print "\t".join(chrom_dict[chrom][pos])
        pass