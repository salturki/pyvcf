"""
Given a a list of VCF files, filter for HET/HOM variants and list shared variants

27 Feb 2013
sa9@sanger.ac.uk
"""

import sys
import pyVCFtoolbox as tlbx

#global variables
sharedVars = {}
sampleIds = []
header = []


def translateGTinMultiSample(altIdx, requestedGT, GT):
    """
    This function will take care of multi-allelic variants and return HET or HOM based on the GT combination
    and the current alt under processing.
    """
    if requestedGT == 2:
        if int(GT[0]) == altIdx and   int(GT[2]) == altIdx:
            return requestedGT
    elif requestedGT == 1:
        if int(GT[0]) != int(GT[2]): # not HOM ref or HOM-non-ref
            if int(GT[0])  == altIdx or int(GT[2])  == altIdx:
                return requestedGT
    return 0

def getRawGT(FORMAT1,FORMAT2):
    for i , tag in enumerate(FORMAT1.split(":")):
        if tag == "GT":
            GTidx = i
            break
    return FORMAT2.split(":")[GTidx]

def parseVariants(f, AFupperCutoff):
    for line in f:
        if not line.startswith("#"):
            line    = line.strip().split("\t")
            chrom   = line[0]
            pos     = line[1]
            ref     = line[3]
            alts    = tlbx.getAlts(line[4])
            FILTER  = line[6]
            FORMAT1 = line[8]
            FORMAT2 = line[9]
            rawGT   = getRawGT(FORMAT1,FORMAT2)
            GT      = tlbx.getGT(FORMAT1,FORMAT2)
            if tlbx.isPass(FILTER):
                for altIdx, alt in enumerate(alts):
                    CQ, GN, OKG_AF, COHORT_AF, ESP_AF, DDD_AF, dbSNP137, GERP, \
                    PolyPhen, SIFT = tlbx.getInfo(['VCQ','VGN','1KG_AF', 'UK10K_cohort_AF','ESP_AF', 'DDD_AF',
                                                'dbSNP137','GERP', 'PolyPhen','SIFT'],line[7], altIdx)
                    key = (chrom,pos, ref, alt)
                    newLine = []
                    newLine.extend(line[:4])
                    newLine.append(alt)
                    newLine.extend(line[5:7])
                    newLine.extend([CQ, GN, OKG_AF, COHORT_AF, ESP_AF, DDD_AF, dbSNP137, GERP, PolyPhen, SIFT])

                    if not tlbx.isCommon([OKG_AF,COHORT_AF,ESP_AF,DDD_AF],AFupperCutoff):
                        if str(altIdx+1) not in rawGT:  # for the second alt to be considered genotype should be 0/2, 1/2 or 2/2
                            GT = 0 # use this as a filler
                        if not sharedVars.has_key(key):
                            sharedVars[key] = {'basic':newLine,'genotypes':{sampleId:GT}}
                        else:
                            tmp =  sharedVars[key]
                            sharedVars[key]['genotypes'][sampleId] = GT
        elif line.startswith("#CHROM"):
            global header
            line    = line.strip().split("\t")
            if not header:
                header.extend(line[:7])
                header.extend(['VCQ','VGN','1KG_AF',
                           'UK10K_cohort_AF','ESP_AF', 'DDD_AF',
                           'dbSNP137','GERP',
                           'PolyPhen','SIFT',
                            'shared_By'])
            sampleId = line[9]
            if sampleId not in sampleIds:
                sampleIds.append(sampleId)
            if sampleId not in header:
                header.append(sampleId)


def printSharedVariants(requestedGT, AppearInAtLeastNbr):
    print "\t".join(header)
    for key in sorted(sharedVars):
        line = sharedVars[key]['basic']
        genotypes = []
        for sampleId in sampleIds:
            if sampleId in sharedVars[key]['genotypes']:
                genotypes.append(sharedVars[key]['genotypes'][sampleId])
            else:
                genotypes.append(0)

        frq = genotypes.count(requestedGT)
        if frq >= AppearInAtLeastNbr:
            line.append(frq)
            line.extend(genotypes)
            print "\t".join([str(x) for x in line])


def main():
    requestedGT        = 1    # 1 for het and 2 for hom
    exludeGT           = 2    # if one of the samples has hom, ignore it
    AFupperCutoff      = 1 # Anything above this cutoff is considered common
    AppearInAtLeastNbr = 7  # print the genotype if appear in at least n samples
    paths = [
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5228099.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5236930.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5236933.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5236937.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5319107.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5319108.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5319109.vcf.gz',
        '/Users/Macia/Dropbox/Team29/MyProjects/SpinaBifida/27-Feb-2013/new_AF/SC_SBMWES5319110.vcf.gz',
    ]
    for path in paths:#sys.argv:
        f = tlbx.openFile(path)
        parseVariants(f, AFupperCutoff)
    printSharedVariants(requestedGT, AppearInAtLeastNbr)

if __name__ == '__main__':
    main()