"""
Take three folders (one for SamTools, GATK and Dindel) from GAPI pipeline and use merge_GSD_VCFs_v5.py script to
 merge them into the output folder.

Matching files is based on the first number in their names (GAPI pipeline only).
VCFtools (vcf-isec is used to merge. Dindel takes over Samtools and Samtools takes over GATK files when variant
 overlap.

3 Mar 2013
sa9@sanger.ac.uk
"""

import os, sys, fnmatch

files_dict = {}

def walker(DIR, caller):
    global files_dict
    for path, subdirs, files in os.walk(DIR):
        for name in files:
            if fnmatch.fnmatch(name, "*.vcf.gz"):
                ID = name.split("_")[0]
                f_path = os.path.join(path, name)
                if not files_dict.has_key(ID):
                    files_dict[ID] = {caller:f_path}
                else:
                    files_dict[ID][caller] = f_path
                    
def run_merge_script(output_folder, includePass):
    global files_dict
    if includePass == 'pass':
        filterParameter = '-a'
    else:
        filterParameter = ''
    for ID in files_dict.keys():
        o_path = os.path.join(output_folder, ID + ".vcf.gz")
        #bsub -M 300000 -R"select[mem>300] rusage[mem=300]" -oo merge.log
        try:
            cmd = 'vcf-isec -n +1 %s %s %s %s | bgzip -c > %s; tabix -p vcf %s' % \
                  (filterParameter, files_dict[ID]['D'], files_dict[ID]['S'], files_dict[ID]['G'], o_path, o_path )
        except KeyError as e:
            print "ERROR: %s" % e
            print files_dict[ID]
            sys.exit(1)
        print cmd
    

def main():
    f = open(sys.argv[1],'r')
    output_folder = sys.argv[2]
    includePass   = sys.argv[3] # pass or all
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            SamTools_folder = line[1]
            GATK_folder     = line[2]
            Dindel_folder   = line[3]
            if SamTools_folder and GATK_folder and Dindel_folder:
                walker(SamTools_folder, "S")
                walker(GATK_folder,     "G")
                walker(Dindel_folder,   "D")
            else:
                print "Please make sure you have supplied a path to tab file with project name, samtoolVCFs, GATKVCFs,\
                 DindelVCFs paths."
                sys.exit(1)
    if includePass.lower() in ['pass','all']:
        run_merge_script(output_folder, includePass.lower())
    else:
        print "Third argument must be pass or all"
        print "Usage: python file.py gapi_paths.txt outputDir pass|all"
        sys.exit(1)

if __name__ == '__main__':
    main()
