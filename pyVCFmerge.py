"""
Goal:
-----

Merge a list of VCF files

Steps:
------
Given a list of paths to tabixed VCF files in a file:
- split by chr
- merge them by chr into single files
- concatenate the merge files
- delete the single split vcf files


Tested with vcftools v0.10

2 Feb 2013
sa9@sanger.ac.uk
"""

import sys,os
import pyVCFtoolbox as tlbx

def getFilesByChr(root,chrom):
    paths = []
    for path in os.listdir(root):
        if path.endswith(".vcf.gz"):
            fileName = os.path.basename(path)
            if fileName.split(".")[-3].split("_")[-1] == chrom:
                paths.append(path)
    return paths

def splitByChr(outputRootPath, listOfVCFsPath):
    fileByChrPaths = {}
    fPaths     = tlbx.openFile(listOfVCFsPath)
    chrList    = tlbx.createChrlist()
    cmdFilePath    = os.path.join(outputRootPath,"splitByChr_cmd.txt")
    o = open(cmdFilePath,'w')
    nbrOfJobs = 0
    for chrom in chrList:
        fileByChrPaths[chrom] = []
        for line in fPaths:
            line = line.strip()
            if not os.path.isfile(line+".tbi"):
                print "No tabix file for %s" % line
                o.close()
                os.remove(cmdFilePath)
                sys.exit(1)
            try:
                os.path.isfile(line)
                inputPath = line
                outputName = os.path.basename(inputPath).split('.')[0] + "_%s.vcf.gz" % chrom
                outputPath = os.path.join(outputRootPath,outputName)
                o.write('tabix -h %s %s | bgzip -c > %s ;tabix -p vcf %s\n' % (inputPath, chrom, outputPath, outputPath))
                nbrOfJobs +=1
                fileByChrPaths[chrom].append(outputPath) # for the next step
            except Exception as e:
                print e
                sys.exit(1)
        fPaths.seek(0,0)
    o.close()
    jobName = tlbx.stampJobName("splitVCFs")
    nbrOfParallelJobs = round(float(nbrOfJobs)/20,0)
    tlbx.runJobArray(cmdFile=cmdFilePath,jobName=jobName,nbrOfJobs=nbrOfJobs, nbrOfParallelJobs=nbrOfParallelJobs,previousJobs=None)
    return fileByChrPaths, jobName
    
def mergeByChr(outputRootPath, fileByChrPaths, prevJobs):
    mergedFiles = []
    chrList  = tlbx.createChrlist()
    mergedVCFsDir   = os.path.join(outputRootPath,"mergedVCFs")
    tlbx.makedir(mergedVCFsDir)
    cmdFilePath    = os.path.join(mergedVCFsDir,"mergeByChr_cmd.txt")
    o = open(cmdFilePath,'w')
    nbrOfJobs = 0
    for chrom in chrList:
        inputVCFs = fileByChrPaths[chrom]
        outputPath = os.path.join(mergedVCFsDir,"%s.vcf.gz" % chrom)
        o.write('vcf-merge -R 0/0 -t  %s | bgzip -c > %s ;tabix -p vcf %s\n' % (" ".join(inputVCFs), outputPath, outputPath))# -R 0/0 replaces . with 0/0 and -t removes duplicated ALT
        nbrOfJobs +=1
        mergedFiles.append(outputPath)
    o.close()
    nbrOfParallelJobs = nbrOfJobs
    jobName = tlbx.stampJobName("mergedVCFs")
    tlbx.runJobArray(cmdFile=cmdFilePath,jobName=jobName,nbrOfJobs=nbrOfJobs, nbrOfParallelJobs=nbrOfParallelJobs,previousJobs=prevJobs,memoryMb=4500)
    return mergedVCFsDir, mergedFiles, jobName

def concatenateVCFs(outputRootPath, mergedVCFsDir, mergedFiles, prevJobs):
    finalMergedVCFsDir = os.path.join(outputRootPath,"finalMergedVCFs")
    tlbx.makedir(finalMergedVCFsDir)

    inputVCFs = mergedFiles
    outputPath = os.path.join(finalMergedVCFsDir,"singleFinal.vcf.gz")
    cmd = 'vcf-concat %s | bgzip -c > %s ;tabix -p vcf %s' % (" ".join(inputVCFs), outputPath, outputPath)

    jobName = tlbx.stampJobName("concatVCFs")
    tlbx.runSingleJob(cmd=cmd,jobName=jobName,outputPath=outputRootPath,previousJobs=prevJobs)
    return jobName

def cleanFiles(outputRootPath, prevJobs):
    cmd  = "cd %s;rm *.err *.out *.vcf.gz *.tbi;"  % outputRootPath
    cmd += "cd %s/mergeVCFs;rm *.err *.out"        % outputRootPath
    jobName = tlbx.stampJobName("cleanTmpVCFs")
    tlbx.runSingleJon(cmd=cmd,jobName=jobName,outputPath=outputRootPath,previousJobs=prevJobs)

def main():
    listOfVCFsPath = sys.argv[1]
    outputRootPath = sys.argv[2]
    fileByChrPaths, prevJobs = splitByChr(outputRootPath, listOfVCFsPath)
    mergedVCFsDir, prevJobs, mergedFiles = mergeByChr(outputRootPath, fileByChrPaths, prevJobs)
    prevJobs = concatenateVCFs(outputRootPath, mergedVCFsDir, prevJobs, mergedFiles)
    cleanFiles(outputRootPath, prevJobs)

if __name__ == '__main__':
    main()