'''
This script generates many stats to desribe the number and type of variants in GAPI files
called using diffrent callers (Samtools , GATK and Dindel). It assume the VCF file is merged
using merge_GSD_VCFs_v4.py script.

sa9@sanger.ac.uk
21 Feb 2013
'''
import os, sys, pysam, copy
import pyVCFtoolbox as tlbx
tabixFiles = {'S':0,'G':0,'D':0}

CQ_groups  = {"LOF":0,"FNC":0,"SIL":0,"OTH":0}
AF_groups  = {
    "1KG_AF"         :{"NOVL":0, "RARE":0,"COMN":0},
    "UK10K_cohort_AF":{"NOVL":0, "RARE":0,"COMN":0},
    "ESP_AF"         :{"NOVL":0, "RARE":0,"COMN":0},
    "DDD_AF"         :{"NOVL":0, "RARE":0,"COMN":0}
        }

ALT_groups = {"same":0,"diff":0}
GT_groups  = {"same":0,"diff":0}
callers    = {
    "unkonwn":0,
    "snv":
        {
        'G' :{"G_pass":{},"G_nPass":{}},
        'S' :{"S_pass":{},"S_nPass":{}},
        'GS':{
            "G_pass_S_pass"  :{},
            "G_nPass_S_pass" :{},
            "G_pass_S_nPass" :{},
            "G_nPass_S_nPass":{},
            },
        },
    "indel":{
        'S' :{"S_pass":{},"S_nPass":{}},
        'D' :{"D_pass":{},"D_nPass":{}},
        'DS':{
            "D_pass_S_pass"  :{},
            "D_nPass_S_pass" :{},
            "D_pass_S_nPass" :{},
            "D_nPass_S_nPass":{},
            },
    }
}

def populateResultsHolder():
    global callers
    for varType in ['snv','indel']:
        for caller in callers[varType]:
            for Filter in callers[varType][caller]:
                callers[varType][caller][Filter]['AF'] = copy.deepcopy(AF_groups)
                callers[varType][caller][Filter]['CQ'] = copy.deepcopy(CQ_groups)
                if caller in ['GS','DS']:
                    callers[varType][caller][Filter]['ALT'] = copy.deepcopy(ALT_groups)
                    callers[varType][caller][Filter]['GT']  = copy.deepcopy(GT_groups)

def parseCallerInfo(GATK,SAMTOOLS,DINDEL):
    raw        = {"G":GATK,"S":SAMTOOLS,"D":DINDEL}
    processed  = {"G":{},"S":{},"D":{}}
    tags = ["ref","alt","qual","gt","gq","cq","fltr"] #REF, ALT, QUAL, GT,str(GQ),CQ, FILTER
    for caller in raw:
        if raw[caller] != ".":
            processed[caller] = dict(zip(tags, raw[caller].split("_")))
    return processed
    
def generateNewFilterName(results, CALLERS, varType):
    ''' Given list of callers letter (G,S,D) and the status of FILTER (PASS or else),
        this function create a name (e.g. G_pass_D_pass or G_pass) in order to use it
        to add count to the results dict (i.e. callers dict)
    '''
    callerIdx = {'snv':{'G':0,'S':1}, 'indel':{'D':0,'S':1}}
    newFilterName = []
    for caller in CALLERS:
        if len(CALLERS) == 2:
            idx = callerIdx[varType][caller]
        else:
            idx = 0 
        FILTER = results['FILTER'][idx]
        if FILTER != ".":
            if FILTER == 'PASS':
                fltr = 'pass'
            else:
                fltr = 'nPass'
            newFilterName.extend([caller,fltr])
    return "_".join(newFilterName)

def isRareOrCommon(AFvalue):
    if AFvalue == '.':
        return "NOVL"
    elif tlbx.isCommon([AFvalue],0.01):
        return "COMN"
    else:
        return "RARE"

def checkALTorGTconcordance(callerA_value, callerB_value):
    if callerA_value == callerB_value:
        return "same"
    else:
        return "diff"

def doCount(results, CALLER, varType):
    callerIdx = {'snv':{'G':0,'S':1}, 'indel':{'D':0,'S':1}}
    newFilterName  = generateNewFilterName(results,CALLER,varType)
    
    # add CQ group
    if len(CALLER) == 2:
        idx  = callerIdx[varType][CALLER[0]]
    else:
        idx = 0 
    CQgroup = results['VCQ'][idx]
    callers[varType][CALLER][newFilterName]['CQ'][CQgroup] += 1

    # add AF count
    for AFname in AF_groups:
        AFvalue  = results[AFname][idx]
        AFgroup  = isRareOrCommon(AFvalue)
        callers[varType][CALLER][newFilterName]['AF'][AFname][AFgroup] += 1
    
    if len(CALLER) > 1: #i.e. GS or SD
        # count ALT concordance
        callerA_ALT = results['ALT'][callerIdx[varType][CALLER[0]]]
        callerB_ALT = results['ALT'][callerIdx[varType][CALLER[1]]]
        ALTconcordance = checkALTorGTconcordance(callerA_ALT, callerB_ALT)
        callers[varType][CALLER][newFilterName]['ALT'][ALTconcordance] += 1
        
        # count GT concordance
        callerA_GT = results['GT'][callerIdx[varType][CALLER[0]]]
        callerB_GT = results['GT'][callerIdx[varType][CALLER[0]]]
        ALTconcordance = checkALTorGTconcordance(callerA_GT, callerB_GT)
        callers[varType][CALLER][newFilterName]['GT'][ALTconcordance] += 1

def fetchVariants(locus, callers):
    global tabixFiles
    holder = {}
    chrom, pos = locus
    start = int(pos) - 1
    end   = int(pos)
    for caller in sorted(callers):
        if callers[caller] == 1:
            lines = tabixFiles[caller].fetch(chrom,start,end)
            for line in lines:
                holder[caller] = line
                break
    return holder

def parseLoci(loci,varType):
    for locus in sorted(loci):
        holder  = fetchVariants(locus, loci[locus])
        results =  {
            "REF"   :[],
            "ALT"   :[],
            "FILTER":[],
            "VCQ"   :[],
            "1KG_AF":[],
            "ESP_AF":[],
            "DDD_AF":[],
            "UK10K_cohort_AF":[],
            "GT"    :[],
        }
        foundCallers = '' # G , S, D, GS, DS
        for caller in sorted(holder):
            if holder[caller] != {}:
                line = holder[caller].strip().split("\t")
                foundCallers += caller
                ref = line[3]
                for altIdx, alt in enumerate(tlbx.getAlts(line[4])):
                    results['REF'].append(ref)
                    results['ALT'].append(alt)
                    if varType == tlbx.getVarType(ref, alt):
                        results['FILTER'].append(line[6])
                        requiredTags  = ['VCQ',"1KG_AF","ESP_AF","UK10K_cohort_AF","DDD_AF"]
                        tagsValues    = tlbx.getInfo(requiredTags, line[7], altIdx)
                        infoHolder    = dict(zip(requiredTags,tagsValues))
                        CQgroup       = tlbx.mapVarClass(infoHolder['VCQ'])
                        results['VCQ'].append(CQgroup)
                        results['1KG_AF'].append(infoHolder['1KG_AF'])
                        results['ESP_AF'].append(infoHolder['ESP_AF'])
                        results['DDD_AF'].append(infoHolder['DDD_AF'])
                        results['UK10K_cohort_AF'].append(infoHolder['UK10K_cohort_AF'])
                        results['GT'].append(tlbx.getGT(line[8],line[9]))
                        break # this doesn't handle multiallelic sites 
            else:
                for item in results:
                    results[item].append(".")                
        doCount(results, foundCallers, varType)

def getLoci(callerGroup):
    '''
    Load all loci in all three files to make sure we will fill them later.
    '''
    loci = {}
    for caller, path , varType in callerGroup:
        f = tlbx.openFile(path)
        for line in f:
            if not line.startswith("#"):
                line = line.strip().split("\t")
                chrom = line[0]
                pos = line[1]
                ref = line[3]
                for alt in tlbx.getAlts(line[4]):
                    if varType == tlbx.getVarType(ref,alt):
                        if not loci.has_key((chrom,pos)):
                            loci[(chrom,pos)]= {caller:1}
                        else:
                            loci[(chrom,pos)][caller] = 1
        f.close()
    return loci


def main():
    global tabixFiles
    testingArgs = ['doCount',
                   "/Users/Macia/Desktop/SC_CHDB/GAPI/S.vcf.gz",#S
                   "/Users/Macia/Desktop/SC_CHDB/GAPI/G.vcf.gz",#G
                   "/Users/Macia/Desktop/SC_CHDB/GAPI/D.vcf.gz",#D
                   ]
    command, S_path, G_path, D_path = testingArgs#sys.argv
    snvCallers   = [('S',S_path,'snv'),('G',G_path,'snv')]
    indelCallers = [('S',S_path,'indel'),('D',D_path,'indel')]
    if command == 'doCount':
        populateResultsHolder()
        for callerGroup in [indelCallers,snvCallers]:
            for caller, path,varType in callerGroup:
                tabixFiles[caller] = pysam.Tabixfile(path)
            loci = getLoci(callerGroup)
            parseLoci(loci,varType)
        print callers
    elif command == 'doAverage':
        getAverage(path) # path to a directory where we stored *_callersStats.txt files

if __name__ == '__main__':
    main()