"""
Given a list of tabix VCF fiels for trios (child,mother,father):

- The VCF files should be annotated with [AF/RELEASE-21-01-2013]

- Filter the data in each (rare coding pass variants) where
    * Rare is < 1% in any (AF_MAX, UK10K_Cohort or ESP)
    * Coding variants including (LOF, Functional)
    * PASS in the FILTER column

- Genes boudaries are taken from BioMart (Knwon and Protein-coding genes).
    * The script used to get the process the gene list is biomart2tabix.py in the recources dir.

- Count the number of variants under (AR, AD and Compound in Autoaomal and X-linked)
    The table from FEVA configuration tables filled by Hellen and David

- This is a multithreaded script. The maxmium number of threads is 20 (20 genes at once).

7 Feb 2013
sa9@sanger.ac.uk

"""

import sys,os,gzip
import pyVCFtoolbox as tlbx
import pysam
import itertools

genotypesHolder = {} # where we keep the count results and print when finish
allowedGenes    = {} # in case you want to include variants in a given list of genes
allowedVarType  = 'both'

def loadGenes(path):
    f = open(path,'r')
    for line in f:
        line = line.strip().strip("\t")
        allowedGenes[line] = 0

def createGenotypesHolder():
    global genotypesHolder
    single, compound = tlbx.getTrioGenotypes()
    single    = dict(zip(single,[0 for x in single]))
    compound  = dict(zip(compound,[0 for x in compound]))
    genotypesHolder = {
        'autosomal':{'single':single.copy() ,'compound':compound.copy()},
        'Xfemale'  :{'single':single.copy() ,'compound':compound.copy()},
        'Xmale'    :{'single':single.copy() ,'compound':compound.copy()},
        'Y'        :{'single':single.copy()},
        'MT'       :{'single':single.copy()},
    }
    return genotypesHolder


def getCompoundPermutations(genotypesMatrix):
    # only when the child GT is 1 (het) include in possiable compound hets
    allowedGTs = [x for x in genotypesMatrix if abs(x[0])==1]
    return list(itertools.permutations(allowedGTs,2))

def addCounts(chromClass,GTgroup, genotypes):
    global genotypesHolder
    #print chromClass,GTgroup, genotypes
    for GT in genotypes:
        try:
            genotypesHolder[chromClass][GTgroup][GT] += 1
        except:#try the reversed compound genotype
            rGT = (GT[1],GT[0])
            try:
                genotypesHolder[chromClass][GTgroup][rGT] += 1
            except:
                pass

def countGenotypes(chrom, childGender, genotypesMatrix):
    """
    Count variants in different chromosomes / genotype configurations
    genotypesMatrix is the current genotypes in one gene
    genotypesHolder is the global result holder
    """
    global genotypesHolder

    if chrom not in ['X','Y','MT']: # i.e. autosomal
        addCounts('autosomal','single', genotypesMatrix)
        compoundsGenotyeps = getCompoundPermutations(genotypesMatrix) # returns all possible compound combination
        addCounts('autosomal','compound', compoundsGenotyeps)
    elif chrom == 'X':
        if childGender.lower() == 'male':
            addCounts('Xmale','single', genotypesMatrix)
            compoundsGenotyeps = getCompoundPermutations(genotypesMatrix) # returns all possible compound combination
            addCounts('Xmale','compound', compoundsGenotyeps)
        elif childGender.lower() == 'female':
            addCounts('Xfemale','single', genotypesMatrix)
            compoundsGenotyeps = getCompoundPermutations(genotypesMatrix) # returns all possible compound combination
            addCounts('Xfemale','compound', compoundsGenotyeps)
    elif chrom == 'Y':
        addCounts('Y','single', genotypesMatrix)
    elif chrom == 'MT':
        addCounts('MT','single', genotypesMatrix)

def isVarRareCoding(altIdx,info, AFs):
    isCoding = False
    isRare   = True

    holder = {"VCQ":".",
              "VGN":".",
              "AF_MAX":".",
              "1KG_AF":".",
              "UK10K_cohort_AF":".",
              "ESP_AF":".",
              "DDD_AF":".",
              }
    for item in info.split(";"):
        try:
            key,values = item.split("=")
            try:
                value = values.split(",")[altIdx]
            except:
                value = values
        except:
            value = "1"
        if key in holder:
            holder[key] = value
    if holder['VCQ'] in tlbx.getVarClass('lof_functional'):
        isCoding = True
    for AF in AFs:
        try:
            holder[AF] = float(holder[AF])
            if holder[AF] > 0.01:
                isRare = False
                break
        except:
            pass
    return isRare, isCoding, holder

def getLocusGenotypes(childLocus,childVCF, motherVCF,fatherVCF,holder):
    genotypes = {"child":0,"mother":0,"father":0}
    rawLines  = {"child":0,"mother":0,"father":0}
    chrom,start,end = childLocus
    for name, VCF in [('child',childVCF),
                      ('mother',motherVCF),
                      ('father',fatherVCF)]:
        lines = VCF.fetch(chrom,start-1,end+1)
        if lines:
            for line in lines:
                try:
                    line = line.strip().split("\t")
                    rawLines[name] = line
                    GT = tlbx.getGT(line[8],line[9])
                    if not tlbx.isPass(line[6]): # when variant is not PASS, make the GT negative (if 2 make it -2 , if 1 make it -1)
                        GT = GT * -1
                    genotypes[name] = GT
                except Exception as e:
                    print e
                    pass
                break # should process only one line/variant

    return tuple(genotypes[x] for x in ['child','mother','father']), rawLines

def printResults(childPath, outputDir, AFname, groupName):
    global genotypesHolder
    global allowedVarType
    sampleName   = os.path.basename(childPath).split(".")[0]
    outputPath   = outputDir#os.path.abspath(os.path.join(childPath, os.path.pardir))# get the parent directory of childPath
    singlePath   = os.path.join(outputPath, "%s_singleGenotypesCount+%s+%s+%s.txt"% (sampleName,groupName,AFname, allowedVarType))
    compoundPath = os.path.join(outputPath, "%s_compoundGenotypesCount+%s+%s+%s.txt"% (sampleName,groupName,AFname, allowedVarType))
    single_o     = open(singlePath,'w')
    compound_o   = open(compoundPath,'w')


    single, compound = tlbx.getTrioGenotypes()
    header = ['#genotype','autosomal','Xmale','Xfemale','Y','MT']
    single_o.write("\t".join(header) + "\n")
    for GT in single:
        line = [str(GT)]
        for chromGroup in ['autosomal','Xmale','Xfemale','Y','MT']:
            line.append(genotypesHolder[chromGroup]['single'][GT])
        single_o.write( "\t".join([str(x) for x in line]) + "\n")

    header = ['#genotype','autosomal','Xmale','Xfemale']
    compound_o.write("\t".join(header) + "\n")
    for GT in compound:
        line = [str(GT)]
        for chromGroup in ['autosomal','Xmale','Xfemale']:
            line.append(genotypesHolder[chromGroup]['compound'][GT])
        compound_o.write( "\t".join([str(x) for x in line])+ "\n")
    single_o.close()
    compound_o.close()

def isInAllowedGenes(GN):
    global allowedGenes
    if allowedGenes:
        if GN in allowedGenes:
            return True
        else:
            return False
    else:
        return True #allow all genes

def isAllowedVarType(ref,alt):
    if allowedVarType == 'both':
        return True
    if allowedVarType == 'indels':
        if len(ref) != len(alt) or alt.startswith("<"): #"<DEL>
            return True
        else:
            return False
    if allowedVarType == 'snvs':
        if len(ref) == len(alt) and not alt.startswith("<"):#"<DEL>
            return True
        else:
            return False

def parseTrio(childPath, motherPath , fatherPath, childGender, AFs):
    childVCF  = pysam.Tabixfile(childPath)
    motherVCF = pysam.Tabixfile(motherPath)
    fatherVCF = pysam.Tabixfile(fatherPath)

    geneGenotypesHolder = []
    currentGeneName = ''
    childFile = gzip.open(childPath, 'r')
    for line in childFile:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            chrom = tlbx.removeChrPrefix(line[0])
            #if chrom != '1':
            #    break
            alts = tlbx.getAlts(line[4])
            #if tlbx.isPass(line[6]): # include variants that are PASS in child
            for altIdx, alt in enumerate(alts):
                if isAllowedVarType(line[3],alt):
                    isRare, isCoding, holder = isVarRareCoding(altIdx, line[7], AFs)
                    inAllowedGenes = isInAllowedGenes(holder['VGN'])
                    if inAllowedGenes:
                        if isRare and isCoding:
                            childLocus = tlbx.getLocus(line[0], line[1], line[3], alt)
                            locusGenotypes, rawlines = getLocusGenotypes(childLocus, childVCF, motherVCF, fatherVCF,
                                                                         holder)
                            # The following assumes the variants are sorted by locus,
                            # if the VCF is tabixed then the file should be sorted by position
                            if currentGeneName == '': # the first gene
                                currentGeneName = holder['VGN']
                                geneGenotypesHolder.append(locusGenotypes)
                            elif currentGeneName != holder['VGN']:
                                # we have reached to the end of this gene. Do the count and move to the next gene
                                countGenotypes(chrom, childGender, geneGenotypesHolder) # do the count
                                # reset the geneName and the genotypes holder for this gene
                                currentGeneName = holder['VGN']
                                geneGenotypesHolder = []
                                geneGenotypesHolder.append(locusGenotypes)
                            elif  currentGeneName == holder['VGN']:
                                # we still at the same gene, keep adding locus genotypes
                                geneGenotypesHolder.append(locusGenotypes)

def main():
    ##### FOR TESTING
    args_exact = ['/Users/Macia/Desktop/TestTrioCounts/exact/correct/DDD_MAIN5194250_merged.vcf.gz',
                   '/Users/Macia/Desktop/TestTrioCounts/exact/correct/DDD_MAIN5250850_merged.vcf.gz',
                   '/Users/Macia/Desktop/TestTrioCounts/exact/correct/DDD_MAIN5250853_merged.vcf.gz',
                   'Female',
                   '/Users/Macia/Desktop/TestTrioCounts/exact/correct/detailed_counts/',
                ]

    # args_lenient = ['/Users/Macia/Desktop/TestTrioCounts/lenient/correct/DDD_MAIN5194250_merged.vcf.gz',
    #               '/Users/Macia/Desktop/TestTrioCounts/lenient/correct/DDD_MAIN5250850_merged.vcf.gz',
    #               '/Users/Macia/Desktop/TestTrioCounts/lenient/correct/DDD_MAIN5250853_merged.vcf.gz',
    #               'Female',
    #               '/Users/Macia/Desktop/TestTrioCounts/lenient/correct/detailed_counts/',
    #               ]
    #
    # args_old = ['/Users/Macia/Desktop/TestTrioCounts/old/DDD_MAIN5194250_merged.vcf.gz',
    #             '/Users/Macia/Desktop/TestTrioCounts/old/DDD_MAIN5250850_merged.vcf.gz',
    #             '/Users/Macia/Desktop/TestTrioCounts/old/DDD_MAIN5250853_merged.vcf.gz',
    #             'Female',
    #             '/Users/Macia/Desktop/TestTrioCounts/old/detailed_counts/',
    #             ]
    ######

    AFs = {
         # 'AF_MAX':['AF_MAX'],
         # '1KG_AF':['1KG_AF'],
         # 'UK10K_cohort_AF':['UK10K_cohort_AF'],
         # 'ESP_AF':['ESP_AF'],
         # 'DDD_AF':['DDD_AF'],
         '3AFs':['1KG_AF','UK10K_cohort_AF','ESP_AF'],
         # '4AFs':['1KG_AF','UK10K_cohort_AF','ESP_AF','DDD_AF']
        }
    geneGroups = {'all': 0,}
                  # "DDG2P": os.path.join(os.path.dirname(os.path.abspath(__file__)),
                  #                       'resources/DDD_genes/DDDG2P_7Feb13.txt')}


    for groupName, groupFile in geneGroups.items():
        if groupName == 'all':
            global allowedGenes
            allowedGenes = {} # i.e. include every gene
        else:
            loadGenes(geneGroups[groupName])
        for AFname,AFList in AFs.items():
            for varType in ['snvs','indels','both']:
                global allowedVarType
                allowedVarType = varType
                print "Processing %s+%s+%s" % (groupName,AFname,varType)
                childPath, motherPath , fatherPath, childGender, outputDir = args_exact#sys.argv[1:6]#
                createGenotypesHolder()
                parseTrio(childPath, motherPath , fatherPath, childGender, AFList)
                printResults(childPath, outputDir,AFname, groupName)

if __name__ == '__main__':
    main()