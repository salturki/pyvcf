"""
Goal:
-----

Create an allele frequency file (bed) from a list of VCF files.

Steps:
------
Given a list of paths to tabixed VCF files:
    - split by chr
    - merge them by chr into single files
    - run vcftools -freq option to create frq files for each chr
    - convert each frq file to bed file (bgzip)
    - concatenate the merge bed files into one (bgzip and tabix)
    - delete the single split vcf files

3 Feb 2013
sa9@sanger.ac.uk
"""

import sys,os
import pyVCFtoolbox as tlbx
import pyVCFmerge as mrg

def createFrqFiles(outputRootPath, mergedFiles, prevJobs):
    frqFiles = []
    frqFilesDir   = os.path.join(outputRootPath,"frqFiles")
    tlbx.makedir(frqFilesDir)
    cmdFilePath    = os.path.join(frqFilesDir,"frqFiles_cmd.txt")
    o = open(cmdFilePath,'w')
    nbrOfJobs = 0
    for inputPath in mergedFiles:
        outputPath = os.path.join(frqFilesDir,"%s" % os.path.basename(inputPath).split(".")[0])
        cmd = 'vcftools --gzvcf %s --out %s --freq' % (inputPath, outputPath) # generate the frq files
        # this is a mini-bash script to check if the input file exits first. This avoid breaking the pipeline
        checkFileExist = "if [ -f %s ];then  %s;fi\n" % (inputPath, cmd)
        o.write(checkFileExist)
        nbrOfJobs +=1
        frqFiles.append(outputPath+".frq") # append the bgzip files only
    o.close()
    nbrOfParallelJobs = nbrOfJobs
    jobName = tlbx.stampJobName("createFrqFiles")
    tlbx.runJobArray(cmdFile=cmdFilePath,jobName=jobName,nbrOfJobs=nbrOfJobs, nbrOfParallelJobs=nbrOfParallelJobs,
                     previousJobs=prevJobs,memoryMb=4500)
    return frqFiles, jobName 

def convertFrq2Bed(outputRootPath, frqFiles, prevJobs ):
    bedFiles = []
    bedFilesDir   = os.path.join(outputRootPath,"bedFiles")
    tlbx.makedir(bedFilesDir)
    cmdFilePath    = os.path.join(bedFilesDir,"bedFiles_cmd.txt")
    o = open(cmdFilePath,'w')
    nbrOfJobs = 0
    for inputPath in frqFiles:
        outputPath = os.path.join(bedFilesDir,"%s.bed.gz" % os.path.basename(inputPath).split(".")[0])
        cmd = 'python %s/frq2bed.py %s | bgzip -c > %s' % (os.path.dirname(os.path.abspath(__file__)),
                                                                     inputPath, outputPath)
        # this is a mini-bash script to check if the input file exits first. This avoid breaking the pipeline
        checkFileExist = "if [ -f %s ];then  %s;fi\n" % (inputPath, cmd)
        o.write(checkFileExist)
        nbrOfJobs +=1
        bedFiles.append(outputPath)
    o.close()
    nbrOfParallelJobs = nbrOfJobs
    jobName = tlbx.stampJobName("covertFrq2Bed")
    tlbx.runJobArray(cmdFile=cmdFilePath,jobName=jobName,nbrOfJobs=nbrOfJobs, nbrOfParallelJobs=nbrOfParallelJobs,
                     previousJobs=prevJobs)
    return bedFiles , jobName

def concatenateBedFiles(outputRootPath, bedFiles, prevJobs ):
    finalMergedBEDsDir = os.path.join(outputRootPath,"finalMergedBEDs")
    tlbx.makedir(finalMergedBEDsDir)

    outputPath = os.path.join(finalMergedBEDsDir,"singleFinal.bed")
    #add header from a single file
    header = "#CHROM\tFROM\tTO\tREF\tALT\tAF\n"
    o = open(outputPath,'w')
    o.write(header)  # write the header and close the file
    o.close()
    cmd = ''
    for bedFile in bedFiles:
        cmd += "zcat %s  >> %s; " % (bedFile, outputPath)
    cmd += "bgzip %s;tabix -p bed %s" % (outputPath, outputPath+".gz")
    jobName = tlbx.stampJobName("concatBEDs")
    tlbx.runSingleJob(cmd=cmd,jobName=jobName,outputPath=outputRootPath,previousJobs=prevJobs)
    return jobName

def cleanFiles(outputRootPath, prevJobs):
    cmd  = "cd %s; rm *.err *.out *.vcf.gz *.tbi;"  % outputRootPath
    cmd += "cd %s; rm *.err *.out;cd ..;"           % 'mergedVCFs'
    cmd += "cd %s; rm *.err *.out;cd ..;"           % 'frqFiles'
    cmd += "cd %s; rm *.err *.out"                  % 'bedFiles'
    jobName = tlbx.stampJobName("cleanTmpFiles")
    tlbx.runSingleJob(cmd=cmd,jobName=jobName,outputPath=outputRootPath,previousJobs=prevJobs)

def main():
    listOfVCFsPath = sys.argv[1]
    outputRootPath = sys.argv[2]
    # workflow 
    fileByChrPaths, prevJobs              = mrg.splitByChr(outputRootPath, listOfVCFsPath)
    mergedVCFsDir, mergedFiles, prevJobs  = mrg.mergeByChr(outputRootPath, fileByChrPaths, prevJobs)
    frqFiles , prevJobs                   = createFrqFiles(outputRootPath, mergedFiles, prevJobs)
    bedFiles , prevJobs                   = convertFrq2Bed(outputRootPath, frqFiles, prevJobs )
    prevJobs                              = concatenateBedFiles(outputRootPath, bedFiles, prevJobs )
    prevJobs                              = mrg.concatenateVCFs(outputRootPath, mergedVCFsDir, mergedFiles, prevJobs)
    
    cleanFiles(outputRootPath, prevJobs)

if __name__ == '__main__':
    main()
