"""
Merge GATK , Smtools and Dindel raw VCF files (from GAPI pipeline).

21 Feb 2013 (version 6.0)
	- Move to pyVCF library
	- Include PASS variants only from any VCF file
	- Change the priority of caller (Dindel > Samtools > GATK) similar to Uber-VCF
	- Re-direct the output to the stdout not to a file (to make use of pipelines such as bgzip, tabix , etc)
	- Handle multi-allelic sites properly
	- Correct the following errors found by using vcf-validator:
	    - Expected GT as the first genotype field
	    - INFO tag [DS] did not expect any parameters
	    - Could not validate the int
	    - Bad ALT value in the GT field, the index [2] out of bounds [1/2]
	    - INFO tag [SamTools=C_G_99_0/1_99_SPLICE_SITE,INTRONIC_PASS] expected different number of values (1)
	    - INFO tag [DS] did not expect any parameters, got [1]
	    - FORMAT tag [DP] not listed in the header
	    - AC is 1, should be 2


sa9@sanger.ac.uk
"""

import re
import optparse
import pyVCFtoolbox as tlbx
from math import floor

#global variables
header = []
merged_dict = {}
tags = {}
mapCallers = {"G":"GATK","S":"SamTools","D":"Dindel"}

def checkTagDataType(line):
    if line.startswith("##INFO") or line.startswith("##FORMAT"):
        fieldType = re.match("^##.*=<", line).group(0)[2:-2] #removes the first ## and the last =< signs
        tagName = ""
        tagType = ""
        numberOfTags = "0"
        tagDescription = "NA"
        raw = re.search("<.*>",line).group(0)[1:-1].split(",")
        for item in raw:
            if item.startswith("ID"):
                tagName = item.split("=")[1]
            elif item.startswith("Number"):
                numberOfTags = item.split("=")[1]
                try:
                    numberOfTags = int(numberOfTags)
                except ValueError:
                    pass # "."
            elif item.startswith("Type"):
                tagType = item.split("=")[1]
            elif item.startswith("Description"):
                tagDescription = item.split("=")[1]
        if not tags.has_key(tagName):
            tags[tagName] = {
                'Type':tagType,
                'Number':numberOfTags,
                'Description':tagDescription
            }


def convertTagValueType(tagName, tagValue):
    """
    Given a tag's value from INFO or FORMAT columns, this function ensures that its type is the same as  the canonical
    data type. If GQ data type in GATK is a float while it is an integer in Samtools check the canonical for the first
    VCF file which should be Dindel > Samtools > GATK and change it accordingly. The gaol is to make sure we pass all
    of the vcftools (vcf-validator) tests. One of them is that the value types in the VCF are in agreement with their
    definitions in the VCF header (which could be different between callers).
    """
    holder = []
    if tagName in tags and  tags[tagName]['Number'] != '.' and tags[tagName]['Type'] in ['Integer','Float']:
        if tags[tagName]['Number'] == 1:
           tagValues = [tagValue]
        elif tags[tagName]['Number'] > 1: # i.e. multiple values separated by commas
            tagValues = tagValue.split(",")
        for value in tagValues:
            if tags[tagName]['Type'] == 'Float':
                holder.append(str(float(value)))
            elif tags[tagName]['Type'] == 'Integer':
                try:
                    value = int(value)
                except ValueError:
                    pass
                try:
                    value = float(value)
                    value = int(floor(value)) # round the float
                except ValueError:
                    pass
                holder.append(str(value))
    else:
        holder = [tagValue]

    return ",".join(holder)


def stored_header(line):
    global header
    checkTagDataType(line)
    header.append(line)


def remove_duplicates_header():
    # Now that we had two lines starting with CHROM; remove one of them, and clean any non-essential header lines
    ID_list = []
    global header
    chrom_header = header[-1]
    version = header[0]
    cleaned_header = []
    for line in header:
        if re.search("=<ID=", line):
            ID = line.split(",")[0]
            if ID not in ID_list:
                ID_list.append(ID)
                cleaned_header.append(line)

    cleaned_header.append('##INFO=<ID=GATK,Number=.,Type=String,Description="GATK:REF,ALT,QUAL,GT,CQ,FILTER">')
    cleaned_header.append('##INFO=<ID=SamTools,Number=.,Type=String,Description="SamTools:REF,ALT,QUAL,GT,CQ,FILTER">')
    cleaned_header.append('##INFO=<ID=Dindel,Number=.,Type=String,Description="Dindel:REF,ALT,QUAL,GT,CQ,FILTER">')
    cleaned_header.append('##INFO=<ID=CALLER,Number=.,Type=String,Description="Called by Dindel, SamTools and/or \
     GATK">')
    cleaned_header.append('##INFO=<ID=CNCRD,Number=.,Type=String,Description="Concordance of alternative allele \
     and/or genotype between the0 callers">')
    cleaned_header.append(chrom_header)
    cleaned_header.insert(0,version)
    return cleaned_header


def merge_info(info_a, info_b):
    for key in info_a.keys():
        if not info_b.has_key(key):
            info_b[key] = info_a[key]
    return info_b


def checkGT(GT1, GT2):
    try:
        GT1 = int(GT1[0]) + int(GT1[2])
        GT2 = int(GT2[0]) + int(GT2[2])
        if GT1 == GT2:
            return None
        else:
            return "GT_conflict"
    except ValueError:
        return None

def checkALT(alts1, alts2):
    try:
        alts1 = alts1.split(",")
    except:
        alts1 = [alts1]

    try:
        alts2 = alts2.split(",")
    except:
        alts2 = [alts2]

    for a1 in alts1:
        for a2 in alts2:
            if a1 != a2:
                return False
    return True

def check_CNCRD_GT_ALT(new_record, current_record):
    CNCRD_status = []
    # check ALT conflict
    if not checkALT(new_record['ALT'], current_record['ALT']):
        CNCRD_status.append("ALT_conflict")
    # check GT conflict
    GT1 = new_record['FORMAT']['GT']
    GT2 = current_record['FORMAT']['GT']
    GTStatus = checkGT(GT1, GT2)
    if GTStatus:
        CNCRD_status.append(GTStatus)
    if CNCRD_status:
        return "|".join(CNCRD_status)
    else:
        return "."


def merge_format(format_a, format_b):
    for key in format_a.keys():
        if not format_b.has_key(key):
            format_b[key] = format_a[key]
    return format_b


def merge(key, record_dict):
    global merged_dict
    if not merged_dict.has_key(key):
        merged_dict[key] = record_dict
    else:
        callerName = mapCallers[record_dict['INFO']['CALLER']]
        merged_dict[key]['INFO'][callerName] = record_dict['INFO'][callerName]
        # merged_dict[key]['FORMAT'] = merge_format(record_dict['FORMAT'], merged_dict[key]['FORMAT'])
        try:
            # if there is a previous conflict , then no need to run this again since this variant is already flagged
            if merged_dict[key]['INFO']['CNCRD']  == "":
                merged_dict[key]['INFO']['CNCRD'] = check_CNCRD_GT_ALT(record_dict,merged_dict[key])
        except:
            merged_dict[key]['INFO']['CNCRD'] = check_CNCRD_GT_ALT(record_dict,merged_dict[key])
    if record_dict['INFO']['CALLER'] not in  merged_dict[key]['INFO']['CALLER']:
        merged_dict[key]['INFO']['CALLER'] += record_dict['INFO']['CALLER']


def joinInfo(infoDict,sep):
    INFO_list = []
    for key,value in sorted(infoDict.items()):
        if value: # not None
            value = convertTagValueType(key, value)
            item = "%s=%s" % (key,value)
        else:
            item = key
        INFO_list.append(item)
    return sep.join(INFO_list)


def joinFormat(FORMAT_dict):
    #make sure the first tag is the GT
    FORMAT1 = ['GT']
    FORMAT2 = [convertTagValueType('GT', FORMAT_dict['GT'])]
    del FORMAT_dict['GT'] # since we added already, delete to avoid duplicate GT
    for key in sorted(FORMAT_dict.keys()):
        FORMAT1.append(key)
        FORMAT2.append(convertTagValueType(key, FORMAT_dict[key]))
    return ":".join(FORMAT1), ":".join(FORMAT2)


def printMergedVCF():
    header = remove_duplicates_header()
    for item in header:
        print item

    for key in sorted(merged_dict.keys()):
        INFO = joinInfo(merged_dict[key]['INFO'], ";")
        FORMAT1, FORMAT2 = joinFormat(merged_dict[key]['FORMAT'])
        line = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (
        key[0] ,
        key[1] ,
        merged_dict[key]['ID'],
        merged_dict[key]['REF'],
        merged_dict[key]['ALT'],
        merged_dict[key]['QUAL'],
        merged_dict[key]['FILTER'],
        INFO,
        FORMAT1,
        FORMAT2,
        )
        print line

def loadVCF(path, caller):
    if path is not None:
        f = tlbx.openFile(path)
        for line in f:
            if not line.startswith("#"):
                line = line.strip().split("\t")
                if line[6] == 'PASS':
                    key = (tlbx.removeChrPrefix(line[0]), line[1])
                    ID = line[2]
                    REF = line[3]
                    ALT =  line[4]
                    QUAL = line[5]
                    FILTER = line[6]
                    INFO = tlbx.getInfoDictionary(line[7])
                    FORMAT = tlbx.getFormatDictionary(line[8], line[9])
                    GQ = FORMAT['GQ']
                    GT = FORMAT['GT']
                    if 'CQ' in INFO:
                        CQ = INFO['CQ']
                    elif 'VCQ' in INFO:
                        CQ = INFO['VCQ']
                    elif 'VCQNC' in INFO:
                        CQ = INFO['VCQNC']
                    else:
                        CQ = '.'
                    INFO["CALLER"] = caller
                    INFO[mapCallers[caller]] = "%s_%s_%s_%s_%s_%s_%s" % (REF, ALT.replace(",","|"), QUAL, GT, GQ,
                                                                         FILTER, CQ.replace(",","|"))
                    record_dict = {
                    "ID": ID,
                    "REF": REF,
                    "ALT": ALT,
                    "QUAL": QUAL,
                    "FILTER": FILTER,
                    "INFO": INFO,
                    "FORMAT": FORMAT,
                    }
                    merge(key, record_dict)

            else:
                stored_header(line.strip("\n"))


def main():
    parser = optparse.OptionParser()
    parser.add_option('-G', '--GATK', help='The path to a VCF file of the variants called by GATK')
    parser.add_option('-S', '--SAMTOOLS', help='The path to a VCF file of the variants by SAMTOOLS')
    parser.add_option('-D', '--DINDEL', help='The path to a VCF file of the variants by DINDEL')
    (opts, args) = parser.parse_args()

    # loadVCF(opts.DINDEL, "D")
    # loadVCF(opts.SAMTOOLS, "S")
    # loadVCF(opts.GATK, "G")
    loadVCF('tests/pyVCFcallersStats/GAPI/new/D.vcf.gz', "D")
    loadVCF('tests/pyVCFcallersStats/GAPI/new/S.vcf.gz', "S")
    loadVCF('tests/pyVCFcallersStats/GAPI/new/G.vcf.gz', "G")

    printMergedVCF()

main()