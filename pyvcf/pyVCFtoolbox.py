"""
Generic functions required in various scripts that deal with VCF and BED files

10 Mar 2013
sa9@sanger.ac.uk
"""

import sys
if sys.version_info[0] != 2 or sys.version_info[1] < 6:
    print("This script requires Python version 2.6 or higher")
    sys.exit(1)

import os
import time
import fnmatch
import gzip
import re

def openFile(path):
    if path:
        try:
            if path.endswith(".gz"):
                return gzip.open(path,'r')
            else:
                return open(path,'r')
        except Exception as e:
            print >> sys.stderr, e
            print path
            sys.exit(1)
    else:
        #  None i.e. no file supplied so we need to read from the stdin
        return sys.stdin


def isHeaderExists(line):
    """
    Get the first line in the input file and check if it starts with "#" otherwise exit the script.
    """
    if line.startswith('#'):
        return True
    else:
        print line
        print "Couldn't find a header line in the input file "
        print "Please add a header line to the input file. Use'#' to mark a header line."
        sys.exit(1)


def isBEDorVCF(line):
    """
    Check if the first line in the input file is a vcf file or a bed (or any tab-based file).
    """
    if line.startswith("##fileformat=VCFv"):
        return "vcf"
    else:
        return "bed"

def getSOTermWeightVEP2_8(SOterm):
    SOterms = {
        "transcript_ablation": 34,
        "splice_donor_variant": 33,
        "splice_acceptor_variant": 32,
        "stop_gained": 31,
        "frameshift_variant": 30,
        "stop_lost": 29,
        "initiator_codon_variant": 28,
        "inframe_insertion": 27,
        "inframe_deletion": 26,
        "missense_variant": 25,
        "transcript_amplification": 24,
        "splice_region_variant": 23,
        "incomplete_terminal_codon_variant": 22,
        "synonymous_variant": 21,
        "stop_retained_variant": 20,
        "coding_sequence_variant": 19,
        "mature_miRNA_variant": 18,
        "5_prime_UTR_variant": 17,
        "3_prime_UTR_variant": 16,
        "intron_variant": 15,
        "NMD_transcript_variant": 14,
        "non_coding_exon_variant": 13,
        "nc_transcript_variant": 12,
        "upstream_gene_variant": 11,
        "downstream_gene_variant": 10,
        "TFBS_ablation": 9,
        "TFBS_amplification": 8,
        "TF_binding_site_variant": 7,
        "regulatory_region_variant": 6,
        "regulatory_region_ablation": 5,
        "regulatory_region_amplification": 4,
        "feature_elongation": 3,
        "feature_truncation": 2,
        "intergenic_variant": 1,
        ".":0,
        "":0
    }
    if SOterm in SOterms:
        return SOterms[SOterm]
    else:
        print >> sys.stderr , "[%s] is not found in SO terms" % SOterm
        sys.exit(1)

def deleteInfoTag(info,tag):
    info = info.strip(";") #remove trailing ';' if any
    newInfo = []
    for item in info.split(";"):
        try:
            k,v = item.split("=")
            if k != tag:
                newInfo.append("%s=%s" % (k,v))
        except:
            newInfo.append( "%s" % item)
    return ";".join(newInfo)


def annotateInfo(info, tag, value, altIdx):
    """
    Annotate info column in VCF file with tag=value. If a tag is already in the info, append or update it with the
    new value but in a way that will preserve multiple values for multiple alternative alleles (if any)
    """
    info = info.strip(";") #remove trailing ';' if any
    newInfo = []
    tagFound = False
    for item in info.split(";"):
        try:
            k,v = item.split("=")
            if k == tag:
                tagFound = True
                if "," in v: # tag has already multiple values for multiple alt
                    v = v.split(",")
                    if len(v) >= altIdx + 1:
                        v[altIdx] = value
                    else:
                        # tag has less multiple values than the number of alternative alleles
                        # this is known issue with VCFtools vcf-merge as it doesn't update the INFO tags with
                        # values for each multiple alternative alleles
                        v.extend(["." for i in range(len(v), altIdx + 1)])
                        v[altIdx] = value
                    v = ",".join(v)
                else:
                    # no multiple values found in this tag
                    if altIdx == 0:
                        # just replace the old value with the new one
                        v = value
                    else:
                        # altIdx is more than one then add "." for the other alternative alleles
                        v = ["." for i in range(0, altIdx + 1)]
                        v[altIdx] = value
                    v = ",".join(v)
                newInfo.append("%s=%s" % (k,v))
            else:
                newInfo.append("%s=%s" % (k,v))
        except Exception as e:
            # print e
            newInfo.append( "%s" % item)
    if not tagFound:
        # There is no tag. Add the tag and its values for single or multiple ALT
        if altIdx == 0:
            newInfo.append("%s=%s" % (tag,value))
        else:
            v = ["." for i in range(0,altIdx+1)]
            v[altIdx] = value
            newInfo.append("%s=%s" % (tag,",".join(v)))
    return ";".join(newInfo)


def getPARregions():
    return [(60001,2699520),(154931044,155260560)]


def getPlatform():
    """
    return the name of platform running the script. Important to decide where to get resource files (on the farm or
    locally.
    """
    import platform
    if platform.system() == 'Darwin': # i.e. on my Mac laptop #TODO: this WILL break on windows or non-FARM unix OS
        return 'local'
    else:
        return 'farm'

def getTrioGenotypes():
    single    = []
    compound  = []
    # 1 is het , 2 is hemizygous or hom, -1 is non-pass het and -2 is non-pass hemizygous or non-pass hom.
    # No 0 variants because we are not interested
    child = [1,2,-1,-2]
    father = mother = [0,1,2,-1,-2] # 0 is under-called or hom-ref
    for c in child:
        for m in mother:
            for f in father:
                single.append((c,m,f))

    for g1 in single:
        for g2 in single:
            if (g2,g1) not in compound and abs(g1[0]) == abs(g2[0]) == 1:
                # mother should be both het or both hom
                if not ((g1[1]==g2[1]==1) and (g1[1]==g2[1]==2)) and not ((g1[2]==g2[2]==1) and (g1[2]==g2[2]==2)) :
                    compound.append((g1,g2))
    return single, compound


def walker(dirPath, fileExt):
    paths = []
    for path, subdirs, files in os.walk(dirPath):
        for name in files:
            if fnmatch.fnmatch(name, "*" + fileExt):
                f_path = os.path.join(dirPath, name)
                if f_path not in files:
                    paths.append(f_path)
    return paths


def createChrlist():
    chrList = [str(x) for x in range(1,23)]
    chrList.append('X')
    chrList.append('Y')
    #chrList.append('MT')
    return chrList


def stampJobName(jobName):
    time.sleep(0.0001)
    timeStamp = str(long(time.time() * 1000))
    return "%s_%s" % (jobName,timeStamp)


def removeChrPrefix(chrom):
    """
    Removes the CHR prefix from the first chrom value, if any.
    """
    if chrom.upper().startswith("CHR"):
        chrom = chrom[3:].strip(' ')
    return chrom

def isValidColumns(columnIndexes, line):
    try:
        chrIdx, posIdx, refIdx, altIdx = [int(x) for x in columnIndexes.split(',')]
        if len(set([chrIdx,posIdx,refIdx,altIdx])) != 4:
            print >> sys.stderr , "Please don't use the same column index more than once [%s]" % columnIndexes
            print >> sys.stderr,"Please check -c option for valid 0-based column indexes (e.g. -c 0,1,3,4) for \
         (chr,pos,ref,alt) columns"
            sys.exit(1)
        chrom = line[chrIdx].strip(' ')
        pos   = line[posIdx]
        ref   = line[refIdx]
        alt   = line[altIdx]
    except Exception as e:
        print >> sys.stderr, e
        print >> sys.stderr, 'Please check -c option for valid 0-based column indexes (e.g. -c 0,1,3,4) for \
         (chr,pos,ref,alt) columns'
        sys.exit(1)
    if removeChrPrefix(chrom) not in createChrlist():
        print >> sys.stderr, "[%s] in the first row is not a valid chromosome name 1..22,X,Y" % chrom
        print >> sys.stderr,"Please check -c option for valid 0-based column indexes (e.g. -c 0,1,3,4) for \
         (chr,pos,ref,alt) columns"
        sys.exit(1)
    try:
        pos = int(pos)
    except:
        print >> sys.stderr, "[%s] in the first row is not a valid position (not integer)" % pos
        print >> sys.stderr, "Please check -c option for valid 0-based column indexes (e.g. -c 0,1,3,4) for \
         (chr,pos,ref,alt) columns"
        sys.exit(1)
    for letter in ref:
        if letter not in 'NEUTRALACGTNX.<DEL><DUP>,':
            print >> sys.stderr, '"[%s] in the first row is not a valid ref (not in "ACGTNX.<DEL><DUP>,")' % ref
            print >> sys.stderr, "Please check -c option for valid 0-based column indexes (e.g. -c 0,1,3,4) for \
             (chr,pos,ref,alt) columns"
            sys.exit(1)
    for letter in alt:
        if letter not in 'NEUTRALACGTNX.<DEL><DUP>,':
            print >> sys.stderr, '"[%s] in the first row is not a valid alt (not in "ACGTNX.<DEL><DUP>,")' % alt
            print >> sys.stderr, "Please check -c option for valid 0-based column indexes (e.g. -c 0,1,3,4) for \
         (chr,pos,ref,alt) columns"
    return True

def getVCFSamplesIdsAndIdx(line):
    """
    Given the CHROM line in the VCF file, this function
    return a map of both the sampleIds and their index
    """
    samples = {}
    line = line.strip().split("\t")
    for idx, sampleId in enumerate(line):
        if idx > 8:
            samples[sampleId] = idx
            samples[idx]      = sampleId
    return samples

def getAlts(alts):
    """
    Returns a list of alternative alleles when they are separated by comma.
    """
    try:
        return alts.split(",")
    except:
        return [alts]


def getLocus(chrom,pos,ref,alt):
    """
    Return the locus coordinates after adjusting the end position based on
    the length of the variant.
    """
    pos = int(pos)
    start = pos
    end = pos + abs(len(ref)-len(alt))
    return chrom,start,end


def getGT(FORMAT1, FORMAT2):
    """
    Returns the genotype value e.g. 0/1
    Requires the FORMAT tags and values (column 9 and 10 in single-VCF files)
    """
    for i , tag in enumerate(FORMAT1.split(":")):
        if tag == "GT":
            GTidx = i
            break
    GT = cleanGT(FORMAT2.split(":")[GTidx])
    return GT

def getMultiGTs(FORMAT1, FORMATs2):
    """
    Returns the genotypes value e.g. 0/1
    Requires the FORMAT tags and values (column 9 and 10:toThEnd in multi-VCF files)
    """
    for i , tag in enumerate(FORMAT1.split(":")):
        if tag == "GT":
            GTidx = i
            break
    GTs = []
    for FORMAT2 in FORMATs2:
        GT = cleanGT(FORMAT2.split(":")[GTidx])
        GTs.append(GT)
    return GTs


def getFormatDictionary(FORMAT1, FORMAT2):
    """
    This function converts FORMAT fields into a python dictionary (tag,value).
    """
    FORMAT1 = FORMAT1.split(":")
    FORMAT2 = FORMAT2.split(":")
    holder  = {}
    for idx , tag in enumerate(FORMAT1):
        holder[tag] = FORMAT2[idx]
    return holder

def getGeneBoundaries(geneName):
    try:
        from cruzdb import Genome
    except:
        print "please install cruzdb module"
        return None
    g = Genome(db="hg19")
    gene = g.refGene.filter_by(name2=geneName).first()
    print "#%s:%d-%d" % (gene.chrom[3:],gene.start,gene.end)
    return gene.chrom[3:],gene.start,gene.end


def getInfoDictionary(info):
    """
    This function converts INFO field into a python dictionary (tag,value).
    """
    holder = {}
    for item in info.split(";"):
        try:
            tag,v = item.split("=")
        except:
            tag = item
            v = None
        holder[tag] = v
    return holder


def getInfo(tags,info,altIdx):
    """
    Return the tag values in the INFO field that match the input tag and the altIdx
    """
    holder = dict(zip(tags,['.' for x in tags]))
    for item in info.split(";"):
        try:
            k,v = item.split("=")
            if ',' in v:
                v = v.split(',')[altIdx]
        except:
            k = item
            v = None
        if k in holder:
            holder[k] = v
    return [holder[k] for k in tags]


def getVarClass(varClass, ensemblVersion=69):
    if ensemblVersion < 70:
        varClasses = {"lof":["ESSENTIAL_SPLICE_SITE", "STOP_GAINED", "COMPLEX_INDEL", "FRAMESHIFT_CODING"],
                      "functional":["NON_SYNONYMOUS_CODING",'STOP_LOST'],
                      "silent":["SYNONYMOUS_CODING"],
                      "lof_functional":["ESSENTIAL_SPLICE_SITE", "STOP_GAINED", "COMPLEX_INDEL", "FRAMESHIFT_CODING",
                                        "NON_SYNONYMOUS_CODING","STOP_LOST"],
                      "all":["ESSENTIAL_SPLICE_SITE", "STOP_GAINED", "COMPLEX_INDEL", "FRAMESHIFT_CODING",
                             "NON_SYNONYMOUS_CODING","STOP_LOST","SYNONYMOUS_CODING"],
                      }
    else: # new SO terms
        varClasses = {"lof":["transcript_ablation",
                             "splice_donor_variant",
                             "splice_acceptor_variant",
                             "stop_gained",
                             "frameshift_variant",
                             "stop_retained_variant"],
              "functional":["inframe_insertion",
                            "inframe_deletion",
                            "missense_variant",
                            "coding_sequence_variant",
                            "initiator_codon_variant",
                            "stop_lost"],
              "silent":["synonymous_variant"],
              "lof_functional":["transcript_ablation",
                             "splice_donor_variant",
                             "splice_acceptor_variant",
                             "stop_gained",
                             "frameshift_variant",
                             "stop_lost",
                             "stop_retained_variant",
                             "inframe_insertion",
                            "inframe_deletion",
                            "missense_variant",
                            "coding_sequence_variant",
                            "initiator_codon_variant"],
              "all":["transcript_ablation",
                             "splice_donor_variant",
                             "splice_acceptor_variant",
                             "stop_gained",
                             "frameshift_variant",
                             "stop_lost",
                             "stop_retained_variant",
                             "inframe_insertion",
                            "inframe_deletion",
                            "missense_variant",
                            "coding_sequence_variant",
                            "initiator_codon_variant",
                            "synonymous_variant"
                            ],
              }
    return varClasses[varClass]


def getVarType(ref,alt):
    """ Returns indel, snv or SVorUnknown"""
    if not alt.startswith("<"):
        if len(ref) == len(alt):
            return "snv"
        elif len(ref) != len(alt):
            return "indel"
    else:
        return "SVorUnknown" # <DUP>, <INS> , <DEL>


def mapVarClass(varClass,vepVersion=69):
    if vepVersion <= 69:
        varClasses = {
            "ESSENTIAL_SPLICE_SITE":"LOF",
            "STOP_GAINED":"LOF",
            "COMPLEX_INDEL":"LOF",
            "FRAMESHIFT_CODING":"LOF",
            "STOP_LOST":"FNC",
            "NON_SYNONYMOUS_CODING":"FNC",
            "SYNONYMOUS_CODING":"SIL",
            }
        try:
            return varClasses[varClass]
        except:
            return "OTH"
    else:
        varClasses = {
            "splice_donor_variant":"LOF",
            "splice_acceptor_variant":"LOF",
            "stop_gained":"LOF",
            "COMPLEX_INDEL":"LOF",
            "frameshift_variant":"LOF",
            "stop_lost":"FNC",
            "missense_variant":"FNC",
            "initiator_codon_variant":"FNC",
            "inframe_insertion":"FNC",
            "inframe_deletion":"FNC",
            "silent_variant":"SIL",
            }
        try:
            return varClasses[varClass]
        except:
            return "OTH"


def cleanGT(rawGT):
    if rawGT.startswith("."): # for "." or "./."
        return -1
    if len(rawGT) == 1: # 0 or 1 hemizygous
        if rawGT == '1':
            return 2
        elif rawGT == '0':
            return 0
        else:
            print "Unknown Genotype in function cleanGT() in pyVCFtoolbox.py [%s]" % rawGT
            sys.exit(1)
    else: # 0/0 , 1/0 etc
        if rawGT[0] == rawGT[2]:
            if rawGT[0] != "0":
                return 2
            else:
                return 0
        else:
            return 1


def isPass(FILTER):
    if FILTER == "PASS":
        return True
    else:
        return False


def isCommon(AFs, upperCutoff):
    """
    returns a boolean.
    AFs is a list  (floats or '.') and upperCutoff is
    the upper limit after which a variant is considered
    a common variant e.g. 0.01 or 0.03 (non inclusive)
    """
    isCommon = False
    for AF in AFs:
        try:
            AF = float(AF)
            if AF > upperCutoff:
                isCommon = True
                break
        except:
            pass #i.e. AF = '.'
    return isCommon


def isMatchingAllele(ref_a, alt_a, ref_b, alt_b ):
    """
    An implementation of Ray Miller's function to match SNVs and INDELs
    My old method compared to Ray's match 101224 and mismatch 261 (all indels).
    Ray's method seems to work better.
    """
    if ref_a == ref_b and alt_a == alt_b:
        # exact match
        return 1

    size_a = len(ref_a) - len(alt_a)
    size_b = len(ref_b) - len(alt_b)

    if size_a == 0 or size_a != size_b:
        # SNV, or indels of different size
        return 0

    # Comparing indels is tricky as the ref and alt for the same indel
    # may differ depending on the caller. The idea here is that indels
    # such as CTGTGGT , C should match CTGTGGTTG , CTG.

    if len(ref_a) > len(ref_b):
        ref_a, alt_a, ref_b, alt_b = ref_b, alt_b, ref_a, alt_a

    if size_a < 0:
        # insertions of same size
        sliced  = re.sub("^%s"%ref_a,'', alt_a)
        remains = re.sub("%s"%sliced,'', alt_b,count=1)
        return remains==ref_b

    if size_a > 0:
        # deletions of same size
        sliced  = re.sub("^%s"%alt_a,'', ref_a)
        remains = re.sub("%s"%sliced,'',ref_b , count=1)
        return remains==alt_b

    return 0


def test_isMatchingAllele():
    lst = [
        [ 'A',       'T',      'A',         'T'     , 1, 'exact match snv'        ],
        [ 'A',       'T',      'A',         'G'     , 0, 'mismatch snv'           ],
        [ 'ATT',     'AT',     'ATT',       'AT'    , 1, 'exact match 1-base del' ],
        [ 'AT',      'ATT',    'AT',        'ATT'   , 1, 'exact match 1-base ins' ],
        [ 'ATT',     'AT',     'ATTT',      'ATT'   , 1, 'slice match 1-base del' ],
        [ 'AT',      'ATT',    'ATT',       'ATTT'  , 1, 'slice match 1-base ins' ],
        [ 'AT',      'A',      'AT',        'AT'    , 0, 'mismatch indel'         ],
        [ 'AAA',     'TTT',    'AAA',       'TTT'   , 1, 'match tri-nucleotide'   ],
        [ 'ATT',     'AT',     'ATT',       'AA'    , 0, 'mismatch indel'         ],
        [ 'CTGTGGT', 'C',      'CTGTGGTTG', 'CTG'   , 1, 'slice match 6-base del' ],
        [ 'TG',      'TTAG',   'T',         'TTA'   , 1, 'match slice'            ],
        [ 'TA',      'TTCAAA', 'T',         'TTCAA' , 1, 'match slice'            ],
        [ 'TG',      'TAG',    'T',         'TA'    , 1, 'match slice'            ],
        [ 'GCCC',    'GGCCCC', 'G',         'GGC'   , 1, 'match slice'            ],
        ]

    for record in lst:
        ref_a, alt_a, ref_b, alt_b , expected, msg = record
        x = isMatchingAllele(ref_a, alt_a, ref_b, alt_b )
        print "\t".join([str(x) for x in [ref_a, alt_a, ref_b, alt_b , expected, msg , x, x==expected]])


def getIndelDirection(ref,alt):
    if len(ref) > len(alt):
        return "del"
    elif len(ref) < len(alt):
        return "ins"
    else:
        return "unkown"
        # print "Couldn't determine the direction of this indel (%s,%s)" % (ref,alt)
        # sys.exit(1)

def getSlice(ref, alt):
    IndelSlice = "."
    if len(ref) > len(alt): #deletion
        IndelSlice = ref[len(alt):]
    elif len(alt) > len(ref): #insertion
        IndelSlice = alt[len(ref):]
    return IndelSlice

def getExactKeys(chrom, pos, ref, alt):
    """
    Requires chrom, pos, ref and alt
    Return the exact I and exact II match keys
    exact I key is (chrom,pos,ref,alt)
    exact II key is (chrom,pos,dnaSlice,direction)
    dnaSlice is the nucleotide difference between ref and alt
    direction is insertion or deletion (ins or del)
    """
    dnaSlice = getSlice(ref,alt)
    direction = getIndelDirection(ref,alt)
    exactI  = (chrom,pos,ref,alt)
    exactII = (chrom,pos,dnaSlice,direction)
    return exactI, exactII

def getExactMatchStatus(vcfLines, fileA_exact_I_key, fileA_exact_II_key):
    """
    Given a list of records in VCF / BED,
    exact_I_key (chrom, pos, ref, alt) and
    exact_II_key (chrom,pos,slice,direction)
    This function tries to match the keys to the VCF/BED variant and returns
         1  -->  match exact I and it is PASS
        -1  -->  match exact I and it is non-PASS
         2  -->  match exact II and it is PASS
        -2  -->  match exact II and it is non-PASS
         0  -->  No exact I or II match was found
    """
    fileB_excact_I_keys  = {}
    fileB_excact_II_keys = {}
    for line in vcfLines:
        line = line.strip().split("\t")
        chrom = line[0]
        pos = line[1]
        ref = line[3]
        alts = line[4]
        for alt in getAlts(alts): # in case of multi-allelic sites
            dnaSlice = getSlice(ref, alt)
            indelDirection = getIndelDirection(ref,alt)
            fileB_excact_I_keys[(chrom, pos, ref, alt)] = line[6]
            fileB_excact_II_keys[(chrom, pos, dnaSlice, indelDirection)] = line[6]

    #check if fileB has a match in fileA by exact match type I
    if fileA_exact_I_key in fileB_excact_I_keys:
        if fileB_excact_I_keys[fileA_exact_I_key] == 'PASS':
            return "1"
        else:
            return "-1"
            # if exact I match is not found , try exact match II
    if fileA_exact_II_key in fileB_excact_II_keys:
        if fileB_excact_II_keys[fileA_exact_II_key] == 'PASS':
            return  "2"
        else:
            return "-2"

    #if not exact match type I or II was found, assign the member status with 0
    return  "0"

def extractVarInfo(chrom,pos,ref,alt):
    varInfo = {
        'type': '', # snv or indel
        'chr':'',
        'pos':'',
        'start':'',
        'end':'',
        'ref':'',
        'alt':'',
        'direction':'',  # ins or del
        'slice':'' ,
        }

    # get the type of the variant
    if len(ref) != len(alt):
        varInfo['type'] = 'indel'
    else:
        varInfo['type'] = 'snv'

    #annotate basic info
    varInfo['chr'] = chrom
    varInfo['pos'] = pos
    varInfo['ref'] = ref
    varInfo['alt'] = alt
    # annotate with indel info
    if varInfo['type'] == 'indel':
        varInfo['direction']  = getIndelDirection(ref,alt)
        varInfo['slice']      = getSlice(ref,alt)
        varInfo['start']      = int(pos) - 10 # pysam uses 0-based indexing
        varInfo['end']        = int(pos)  + len(varInfo['slice']) + 10
    else:
        varInfo['start']      = int(pos) - 1 # pysam uses 0-based indexing
        varInfo['end']        = int(pos) + 1

    return varInfo


def addPrevJobs(jobs):
    job_ids = []

    if jobs:
        if type(jobs) != list:
            jobs = [jobs]
        for job in jobs:
            job_ids.append('done("%s")' % job)
        w_paramter = '-w \'%s\'' % " && ".join(job_ids)
        return w_paramter
    else:
        return ''


def makedir(path):
    if not os.path.isdir(path):
        os.mkdir(path)


def formatMemorySize(mb=None):
    if mb:
        kb = mb * 1000
        memoryOption = '-M %d -R"select[mem>%d] rusage[mem=%s]"' % (kb,mb,mb)
        return memoryOption
    else:
        return ''


def runSingleJob(**keys):
    """
    Submit a single job to the LSF (FARM). The input parameters are:
        memoryMb=memory size in Mb (int)
        jobName=jobName (chr)
        outputPath=the full path to the output folder
        cmd=the full command line to be ran
   """
    if not 'memoryMb' in keys:
        keys['memoryMb'] = None

    cmd = '''bsub %s %s -J %s -o %s/%s.out -e %s/%s.err -q normal "%s"'''% (formatMemorySize(keys['memoryMb']),
                                                                            addPrevJobs(keys['previousJobs']),
                                                                            keys['jobName'],
                                                                            keys['outputPath'],
                                                                            keys['jobName'],
                                                                            keys['outputPath'],
                                                                            keys['jobName'],
                                                                            keys['cmd'],
    )
    print cmd
    os.system(cmd)
    print


def runJobArray(**keys):
    """
    Submit a job array. The input parameters are:
        memoryMb=memory size in Mb (int)
        jobName=jobName (chr)
        nbrOfJobs=total number of jobs (int)
        nbrOfParallelJobs=how many jobs should run in parallel (int)
        cmdFile=the full path to the commands text file (path)
    """
    if not 'memoryMb' in keys:
        keys['memoryMb'] = None

    if os.path.isfile(keys['cmdFile']):
        print keys['cmdFile']
        cmd = '''bsub %s %s -J "%s[1-%d]%%%d" -o %s/%%J.%%I.out -e %s/%%J.%%I.err -q normal''' \
              ''' perl -e 'my @cmds = map { chomp; $_ } <>; die "LSB_JOBINDEX not \set" unless $ENV{LSB_JOBINDEX}; ''' \
              ''' die "LSB_JOBINDEX out of range" unless $ENV{LSB_JOBINDEX} > 0 and $ENV{LSB_JOBINDEX} <= @cmds;''' \
              ''' exec @cmds[$ENV{LSB_JOBINDEX}-1]' %s'''% (formatMemorySize(keys['memoryMb']),
                                                           addPrevJobs(keys['previousJobs']),
                                                           keys['jobName'],
                                                           keys['nbrOfJobs'],
                                                           keys['nbrOfParallelJobs'],
                                                           os.path.split(os.path.abspath(keys['cmdFile']))[0],
                                                           os.path.split(os.path.abspath(keys['cmdFile']))[0],
                                                           keys['cmdFile']
              )
        print cmd
        os.system(cmd)
        print
    else:
        print "No job array was submitted."
        print "Couldn't find %s" % keys['cmdFile']
        sys.exit(1)

def getHWEexactPvalues(obs_hets, obs_hom1, obs_hom2):
    # An implementation of exact HWE as in http://www.sph.umich.edu/csg/abecasis/Exact/index.html
    # common and rare homozygotes
    if obs_hets == 0 or obs_hom1 == 0 or obs_hom2 == 0:
        return None
    obs_homr = min(obs_hom1, obs_hom2)
    obs_homc = max(obs_hom1, obs_hom2)

    rare        = 2 * obs_homr + obs_hets
    genotypes   = obs_homr + obs_homc + obs_hets

    probs = [0.0 for i in range(0,rare+1)]

    # check to ensure that midpoint and rare alleles have same parity
    mid = int(rare * (2 * genotypes - rare) / (2 * genotypes))
    if (mid % 2) != (rare % 2):
        mid += 1

    # Calculate probabilities from midpoint down
    curr_hets = mid
    curr_homr = (rare - mid) / 2
    curr_homc = (genotypes - curr_hets - curr_homr)

    probs[mid] = 1.0
    mysum = 1.0

    while curr_hets >=  2:
        probs[curr_hets - 1]  = probs[curr_hets + 1] * curr_hets * (curr_hets - 1.0) / (4.0 * (curr_homr + 1.0)  * (curr_homc + 1.0))
        mysum    += probs[curr_hets - 1]
        # 2 fewer heterozygotes -> add 1 rare homozygote, 1 common homozygote
        curr_hets -= 2
        curr_homr += 1
        curr_homc += 1

    # Calculate probabilities from midpoint up
    curr_hets = mid
    curr_homr =(rare - mid) / 2
    curr_homc =  genotypes - curr_hets - curr_homr

    while curr_hets <= rare - 3: ###### 2
       probs[curr_hets + 3] = probs[curr_hets + 1] * 4.0 * curr_homr * curr_homc / ((curr_hets + 2.0) * (curr_hets + 1.0))
       mysum += probs[curr_hets + 3]
       # add 2 heterozygotes -> subtract 1 rare homozygtote, 1 common homozygote
       curr_hets += 2
       curr_homr -= 1
       curr_homc -= 1

    # P-value calculation
    target = probs[obs_hets + 1]
    plow  = min(1.0, sum(probs[1:obs_hets + 1]) / float(mysum))
    phigh = min(1.0, sum(probs[obs_hets + 1: rare + 1]) / float(mysum))
    p     = min(1.0, sum([x for x in probs if x <= target])/ float(mysum))
    return p