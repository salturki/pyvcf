"""
Pefrom PCA on a list VCF files and the HapMap VCF files.

Steps:

1) load the loci (chrom, pos, ref, alt ) in both HapMap and VCF files. Only bioallelic
2) for each loci see how many samples have (het, hom, ref, missing)
3) include position appear in 90% of HapMap and 90% of VCF , include.
4) load 10,000 only

24 May 2013
sa9@sanger.ac.uk

"""

import mdp
import pysam
import pyVCFtoolbox as tlbx
from numpy import array
import matplotlib.pyplot as plt

loci    = []

def loadHapMap(path, allowedSamples=None):
    f = tlbx.openFile(path)
    allowedIdx = {}
    nbrOfSamples = 0
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            chrom, pos, ref, alt = line[0], line[1], line[3], line[4]
            key = (chrom, pos, ref, alt)
            if "," not in alt: # only biAllelic
                GTs = []
                for idx in sorted(allowedIdx):
                    GTs.append(float(tlbx.getGT(line[8],line[idx])))
                countRef = GTs.count(0)
                countHet = GTs.count(1)
                countHom = GTs.count(2)
                if float(countRef) <= 0.2 * nbrOfSamples and \
                        (0.4 * nbrOfSamples) <= float(countHet) <=  (0.6 * nbrOfSamples) and \
                        (0.4 * nbrOfSamples) <= float(countHom) <=  (0.6 * nbrOfSamples):
                    loci.append(GTs)
                    if len(loci) > 1500:
                        loci_array = array(loci)
                        # pca = mdp.pca(loci_array, reduce=True)
                        pcan = mdp.nodes.PCANode(reduce=True)
                        pcar = pcan.execute(loci_array, n=3)
                        print pcar
                        #Graph the results
                        fig = plt.figure()
                        ax = fig.add_subplot(111)
                        ax.plot(pcar[:,0], pcar[:,1], 'bo')
                        # ax.plot(pcar[10:,0], pcar[10:,1], 'ro')
                        #Show variance accounted for
                        ax.set_xlabel('PC1 (%.3f%%)' % (pcan.d[0]))
                        ax.set_ylabel('PC2 (%.3f%%)' % (pcan.d[1]))
                        plt.show()
                        break



        elif line.startswith("#CHROM"):
            sampleIndices  = tlbx.getVCFSamplesIdsAndIdx(line)
            if allowedSamples:
                for sampleId in sampleIndices:
                    if  sampleId in allowedSamples:
                        idx = sampleIndices[sampleId]
                        allowedIdx[idx] = sampleId

            nbrOfSamples = len(allowedIdx)

def loadAllowedSamples(path):
    """
    NA06984 	CEU
    NA06985 	CEU
    """
    race = {}
    f = open(path,'r')
    for line in f:
        sampleId, raceName = line.strip().split("\t")
        race[sampleId] = raceName
    f.close()
    return race

def loadVCFs(paths):
    pass


def main():
    HapMapPath     = '/Users/Macia/Desktop/PCA_FET_Burden/PCA/old/VCF/HapMap/hapmap_3.3.hg19_no_chr.vcf.gz'
    HapMapRacePath = '/Users/Macia/Desktop/PCA_FET_Burden/PCA/old/VCF/HapMap/507_HapMap.Mapping.Sample.Population.txt'
    race  = loadAllowedSamples(HapMapRacePath)
    loadHapMap(HapMapPath, race)

if __name__ == "__main__":
    main()