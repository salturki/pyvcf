import sys
sys.path.append("..")
import pyvcf.pyVCF_tdt_table as pyVCF_tdt_table
import pyVCFtoolbox as tlbx

def count():
    print "\t".join(['CQ',"GT","all","rare_uk10k_esp", "rare_local"])
    variants = pyVCF_tdt_table.variants
    holder = {}
    for gene in variants:
        for key in variants[gene]:
            for trioId in variants[gene][key]:
                ESP_AF = UK10K_AF = CQ =  None
                for sampleId in variants[gene][key][trioId]:
                    member   = variants[gene][key][trioId][sampleId]['member']
                    GT       = variants[gene][key][trioId][sampleId]['GT']
                    ESP_AF   = variants[gene][key][trioId][sampleId]['ESP_AF']
                    UK10K_AF = variants[gene][key][trioId][sampleId]['UK10K_AF']
                    local_AF = variants[gene][key][trioId][sampleId]['local_AF']
                    if GT is None:
                        GT = 0
                    if GT:
                        CQ      = variants[gene][key][trioId][sampleId]['CQ']
                    if member == 'child':
                        childGT = GT
                    elif member == 'mother':
                        motherGT = GT
                    elif member == 'father':
                        fatherGT = GT
                GTkey = (childGT,motherGT, fatherGT)

                if not CQ in holder:
                    holder[CQ] = {GTkey:{"all":0,"rare_uk10k_esp":0,"rare_local":0}}
                else:
                    if not GTkey in holder[CQ]:
                        holder[CQ][GTkey] = {"all":0,"rare_uk10k_esp":0,"rare_local":0}

                holder[CQ][GTkey]['all']  += 1
                if not tlbx.isCommon([ESP_AF, UK10K_AF],0.01):
                    holder[CQ][GTkey]['rare_uk10k_esp'] += 1
                if not tlbx.isCommon([local_AF], 0.01):
                    holder[CQ][GTkey]['rare_local'] += 1


    for CQ in holder:
        if CQ:
            for GTkey in sorted(holder[CQ]):
                print CQ, "\t", GTkey, "\t", holder[CQ][GTkey]['all'],\
                    "\t", holder[CQ][GTkey]['rare_uk10k_esp'],\
                    "\t", holder[CQ][GTkey]['rare_local']



def main():
    trio_list_path = '/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/dng/trios_list.txt'
    vcfDir         = '/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/vcf/GAPI/merged_GDS_maf'
    pyVCF_tdt_table.loadTriosList(trio_list_path, vcfDir)
    pyVCF_tdt_table.parseTrios()
    pyVCF_tdt_table.calculate_local_AF()
    count()



if "__main__" == __name__:
    main()