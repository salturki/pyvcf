"""
Given a directory with EVAR (R) output files, this scripts grap every gene and list its variants under three models recessive , compound and dominant

15 May 2013
sa9@sanger.ac.uk
"""

import  os
import xlrd

evar_dir = "/Users/Macia/Dropbox/Team29/MyProjects/CHD/MERGED/11-05-2013/EVAR/trios/0.03_chd&ddd_0.01_others"
genes    = {}
var_classes = {"lof":["ESSENTIAL_SPLICE_SITE", "STOP_GAINED", "COMPLEX_INDEL", "FRAMESHIFT_CODING"],
                "functional":["NON_SYNONYMOUS_CODING",'STOP_LOST'],
              "silent":["SYNONYMOUS_CODING"],
              "lof_functional":["ESSENTIAL_SPLICE_SITE", "STOP_GAINED", "COMPLEX_INDEL", "FRAMESHIFT_CODING",
                                "NON_SYNONYMOUS_CODING","STOP_LOST"],
              "all":["ESSENTIAL_SPLICE_SITE", "STOP_GAINED", "COMPLEX_INDEL", "FRAMESHIFT_CODING",
                     "NON_SYNONYMOUS_CODING","STOP_LOST","SYNONYMOUS_CODING"],
}
IDs = {}

def loadPhenotypeIds():
    wb = xlrd.open_workbook('/Users/Macia/Dropbox/Team29/MyProjects/CHD/MERGED/11-05-2013/TABLES/from_marc/CHD_MASTER_TABLE_11May2013_mph1.xls')
    sh = wb.sheet_by_index(0)
    samples = {}
    phenotypes_dict = {"ALL":0}
    for rownum in range(1,sh.nrows):
        childId    = sh.row_values(rownum)[1]
        fatherId   = sh.row_values(rownum)[2]
        motherId   = sh.row_values(rownum)[3]
        if fatherId != 0.0 and motherId != 0.0:
            phenotypes = sh.row_values(rownum)[6]
            try:
                phenotypes = [x.strip(" ") for x in phenotypes.split(",")]
            except:
                phenotypes = list(phenotypes)
            samples[childId] = phenotypes
            for p in phenotypes:
                if not phenotypes_dict.has_key(p):
                    phenotypes_dict[p] = 0
                phenotypes_dict[p] += 1
            phenotypes_dict["ALL"] += 1 # all samples any phenotype
    return samples, phenotypes_dict


def run(samples, phenotypes, allowedCQ):
    for evar_path in os.listdir(evar_dir):
        if evar_path.endswith(".trio.txt"): # and evar_path.startswith("TOF"):
            sampleId  = evar_path.split(".")[0]
            f = open(os.path.join(evar_dir,evar_path), 'r')
            for line in f:
                if not line.startswith("#"):
                    line = line.strip().split("\t")
                    model = line[0].strip(" ")
                    gene  = line[9]
                    cq    = line[10]
                    if cq in var_classes[allowedCQ]:
                        if not genes.has_key(gene):
                            genes[gene] = {phenotype:{"1":0,"2":0,"3":0} for phenotype in phenotypes}
                        for sample_phenotype in samples[sampleId]:
                            genes[gene][sample_phenotype][model] += 1
                        genes[gene]["ALL"][model] += 1 # all samples and phenotype

            f.close()

    header = ["gene"]
    for phenotype in sorted(phenotypes):
        for model in ["recessive", "compound", "dominant"]:
            header.append("%s_%s_n=%d" % (phenotype, model, phenotypes[phenotype]))
    print "\t".join(header)
    for gene in sorted(genes):
        newLine = [gene]
        for phenotype in sorted(genes[gene]):
            for model in sorted(genes[gene][phenotype]):
                newLine.append(str(genes[gene][phenotype][model]))
        print "\t".join(newLine)

def main():
    allowedCQ = 'lof'
    samples, phenotypes  = loadPhenotypeIds()
    run(samples, phenotypes, allowedCQ)


if __name__ == "__main__":
    main()


