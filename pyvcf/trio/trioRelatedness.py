import os
import sys
import gzip

def run(path):
    print "\t".join(["#child","nbrOfVarChild","sharedWithMother","sharedWithFather", "%sharedWithMother","%sharedWithFather"])
    trio_list = open(path,'r')
    for trioRecord in trio_list:
        trioRecord = trioRecord.strip().split("\t")
        cVCF = gzip.open(trioRecord[0], 'r')
        mVCF = gzip.open(trioRecord[1], 'r')
        fVCF = gzip.open(trioRecord[2], 'r')
    
        cVariants = {}
        counts = {"father":0,"mother":0, "child":0}
        for line in cVCF:
            if not line.startswith("#"):
                line = line.strip().split("\t")
                key = (line[0],line[1], line[3],line[4])
                cVariants[key] = ''
                counts['child'] += 1
        
        for member, fileObj in [('mother',mVCF),('father',fVCF)]:        
            for line in fileObj:
                if not line.startswith("#"):
                    line = line.strip().split("\t")
                    key = (line[0],line[1], line[3],line[4])
                    if key in cVariants:
                        counts[member] += 1
        
        shared_withMom = round(counts['mother'] / float(counts['child']) * 100)
        shared_withDad = round(counts['father'] / float(counts['child']) * 100)
        Results = [os.path.basename(trioRecord[0][:-7]), counts['child'], counts['mother'] , counts['father'], shared_withMom , shared_withDad]
        print "\t".join([str(x) for x in Results])

def main():
    trioListPath = "/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/vcf/GAPI/relatedness/trios_vcf_local.txt"#sys.argv[1]
    run(trioListPath)

main()