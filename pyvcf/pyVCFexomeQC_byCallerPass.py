"""
This is based on pyVCFexomeQC.py but for each sample it reports the following 5 liens

PASS/PASS
PASS/nonPASS
PASS/NotCalled
nonPASS/PASS
NotCalled/PASS

For the main callers (GS / SG) for SNVs and (DS / SD) for INDELs

Variant to include in each of the above 5 groups are defined as:

Callset Options	Name	Include
		PASS/PASS	PASS/nonPASS	nonPASS/PASS	PASS/NotCalled	NotCalled/PASS
1	bothPASS	yes
2	anyPASS	yes	yes	yes	yes	yes
3	PriorityPASS (singleCaller)	yes	yes		yes
4	anyPASSstringent	yes	yes	yes
5	PriorityPASSstringent	yes	yes
6	PriorityPASSplus	yes	yes		yes	yes
7	NoConflicts	yes			yes	yes



10 Mar 2013
sa9@sanger.ac.uk
"""

import os
import pyVCFtoolbox as tlbx
from pyVCFmafAnnotate import *
import mafGenericFunctions as mgf
from pyVCFexomeQC import *
import copy

##########################################
# These are the global variables.
# Change them to match what you have in your VCF files.

LOF_classes        = tlbx.getVarClass("lof")
functional_classes = tlbx.getVarClass("functional")
silent_classes     = tlbx.getVarClass("silent")
coding_classes     = tlbx.getVarClass("all")

# A super list for all coding classes to calculate Ts/Tv and n3/nn3

for lst in (LOF_classes,functional_classes, silent_classes):
    coding_classes.extend(lst)


# for printing
snvs_keys = ["lof","functional","silent", "others"]
indels_keys = ["coding","nonCoding"]

generalResultHolder = {}
callersLables = []

def generateResultsHolder():
    global generalResultHolder

    snvs = {"total":0,
            "rare":{"total":0,"functional":0,"lof":0,"silent":0,"others":0},
            "common":{"total":0,"functional":0,"lof":0,"silent":0, "others":0},
            "Ts":0,"Tv":0,
            "genotype":{"2":0,"1":0,}
    }

    indels = {"total":0,
             "rare":{"total":0,"coding":0,"nonCoding":0},
              "common":{"total":0,"coding":0,"nonCoding":0},
              "n3":0,"nn3":0,
              "genotype":{"2":0,"1":0,}
        }


    generalResultHolder = {"snvs":{},"indels":{}}
    for callerA in ['g','G','.']:
        for callerB in ['s','S','.']:
            generalResultHolder['snvs'][(callerA,callerB)] = copy.deepcopy(snvs)
            generalResultHolder['indels'][(callerA,callerB)] = copy.deepcopy(indels)
            if (callerA,callerB) not in callersLables:
                callersLables.append((callerA,callerB))
    for callerA in ['d','D','.']:
        for callerB in ['s','S','.']:
            generalResultHolder['snvs'][(callerA,callerB)] = copy.deepcopy(snvs)
            generalResultHolder['indels'][(callerA,callerB)] = copy.deepcopy(indels)
            if (callerA,callerB) not in callersLables:
                callersLables.append((callerA,callerB))


def prepareAF():
    """
    Load required tabix files to annotate each variant with 1KG, ESP and UK10K_cohort
    """
    currentPlatform = tlbx.getPlatform()
    if currentPlatform == 'local':
        AFpathsFile =  'resources/AF/maf_master_table_3AFs_QC_local.txt'
    else:
        AFpathsFile =  'resources/AF/maf_master_table_3AFs_QC_farm.txt'

    mafBedPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), AFpathsFile )
    MAF_BED_dict = loadAFs(mafBedPath) # load the tabix index files for the MAF files
    return MAF_BED_dict


def getAFs(chrom,pos,ref,alt, MAF_BED_dict):
    sourceVarInfo = mgf.extractVarInfo(chrom,pos,ref,alt)
    AFList = []
    for mafType in sorted(MAF_BED_dict):
        mafReturned = None
        targetVariants = getMAF(sourceVarInfo, mafType)
        if targetVariants: # not empty
            mafReturned = findMatch(sourceVarInfo, targetVariants)
            pass
        if mafReturned:
            AFList.append(mafReturned)
        else:
            AFList.append('.')
    return AFList


def getTsTv(REF, ALT):
    purines = ['A','G']
    pyrimidines = ['C','T']
    if (REF in purines and ALT in purines) or \
            (REF in pyrimidines and ALT in pyrimidines):
        return "Ts"
    elif (REF in purines and ALT in pyrimidines) or \
            (REF in pyrimidines and ALT in purines):
        return "Tv"
    else:
        return None


def get_n3_nn3(REF, ALT):
    R = len(REF)
    A = len(ALT)
    size = R - A
    if size % 3 == 0:
        type = "n3"
    else:
        type = "nn3"
    return type

def getVarClass(varType,CQs):
    CQ = '.'
    if varType == "snv":
        for CQ in CQs:
            if CQ != ".":
                if CQ in LOF_classes:
                    return "lof", CQ
                elif CQ in functional_classes:
                    return "functional", CQ
                elif CQ in silent_classes:
                    return "silent", CQ
        return "others", CQ
    elif varType == "indel":
        for CQ in CQs:
            if CQ != ".":
                if CQ in LOF_classes or CQ in functional_classes or CQ in silent_classes:
                    return "coding", CQ
        return "nonCoding", CQ
    else:
        return varType, CQ # <DEL>, <DUP>, et al.


def division(dm, nm):
    try:
        return float(dm)/float(nm)
    except ZeroDivisionError:
        return 0

def getHeader():
    header = [
        "sample_id","callers",
         "total_snv","total_snv_HOM", "total_snv_HET","total_snv_HET_HOM_ratio",
         "snv_common_total","snv_common_lof", "snv_common_functional", "snv_common_silent", "snv_common_others",
         "snv_rare_total", "snv_rare_lof", "snv_rare_functional", "snv_rare_silent","snv_rare_others",
         "snv_Ts", "snv_Tv", "snv_Ts_Tv_ratio",
         "total_indel","total_indel_HOM", "total_indel_HET","total_indel_HET_HOM_ratio",
         "indel_common_total","indel_common_coding", "indel_common_NonCoding",
         "indel_rare_total", "indel_rare_coding", "indel_rare_NonCoding",
         "indel_n3", "indel_nn3","indel_n3_nn3_ratio"
    ]
    return "\t".join(header)

def getCALLERkey(CALLER, varType ):
    # DGS
    if varType == 'snv':
        callerA = CALLER[1] #G/g/.
        callerB = CALLER[2] #S/s/.
    else:
        callerA = CALLER[0] #D/d/.
        callerB = CALLER[2] #S/s/.
    return callerA,callerB

def run(path, MAF_BED_dict):
    global generalResultHolder
    sample_id = os.path.basename(path)
    f = tlbx.openFile(path)


    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            chrom = tlbx.removeChrPrefix(line[0])
            pos   = line[1]
            ref   = line[3]
            for altIdx, alt in enumerate(tlbx.getAlts(line[4])):
                varType = tlbx.getVarType(line[3], alt)
                # to make sure we get any tag for CQ
                CQ, VCQ,CSQ, CALLER = tlbx.getInfo(['CQ','VCQ','CSQ','CALLER'], line[7],altIdx)

                resultsKey = tuple(getCALLERkey(CALLER,varType)) #(G,S), (G,.) , (G,s) , (.,S), etc

                AFs_list = getAFs(chrom, pos, ref, alt, MAF_BED_dict)
                var_class, CQ = getVarClass(varType,[CQ,VCQ,CSQ])
                GT = tlbx.getGT(line[8],line[9])

                varIsCommon = tlbx.isCommon(AFs_list, 0.01)
                if varType == "snv" and resultsKey[0] in '.Gg':
                    generalResultHolder["snvs"][resultsKey]['total'] += 1
                    generalResultHolder["snvs"][resultsKey]["genotype"][str(GT)] += 1
                    if not varIsCommon:
                        generalResultHolder["snvs"][resultsKey]["rare"]['total'] += 1
                        generalResultHolder["snvs"][resultsKey]["rare"][var_class] += 1
                    else:
                        generalResultHolder["snvs"][resultsKey]["common"]['total'] += 1
                        generalResultHolder["snvs"][resultsKey]["common"][var_class] += 1
                    if CQ in coding_classes:
                        TsTv = getTsTv(ref, alt)
                        if TsTv:
                            generalResultHolder["snvs"][resultsKey][TsTv] += 1

                elif varType == "indel" and resultsKey[0] in '.Dd':
                    generalResultHolder["indels"][resultsKey]['total'] += 1
                    generalResultHolder["indels"][resultsKey]["genotype"][str(GT)] += 1
                    if not varIsCommon:
                        generalResultHolder["indels"][resultsKey]["rare"]['total'] += 1
                        generalResultHolder["indels"][resultsKey]["rare"][var_class] += 1
                    else:
                        generalResultHolder["indels"][resultsKey]["common"]['total'] += 1
                        generalResultHolder["indels"][resultsKey]["common"][var_class] += 1
                    if CQ in coding_classes:
                        n3_nn3 = get_n3_nn3(ref, alt)
                        generalResultHolder["indels"][resultsKey][n3_nn3] += 1

    f.close()
    # for printing
    for callers in callersLables:
        if callers not in [(".","."),("g","."),("d","."),(".","s"),("d","s"),("g","s")]:
            output = [sample_id, "%s%s" % (callers[0],callers[1])]

            output.append(str(generalResultHolder['snvs'][callers]['total']))
            output.append(str(generalResultHolder['snvs'][callers]['genotype']['2'])) # HOM
            output.append(str(generalResultHolder['snvs'][callers]['genotype']['1'])) # HET
            output.append(str(division(generalResultHolder['snvs'][callers]['genotype']['1'],
                                   generalResultHolder['snvs'][callers]['genotype']['2']))[:4])

            output.append(str(generalResultHolder['snvs'][callers]['common']["total"]))
            output.append(str(generalResultHolder['snvs'][callers]['common']["lof"]))
            output.append(str(generalResultHolder['snvs'][callers]['common']["functional"]))
            output.append(str(generalResultHolder['snvs'][callers]['common']["silent"]))
            output.append(str(generalResultHolder['snvs'][callers]['common']["others"]))
            output.append(str(generalResultHolder['snvs'][callers]['rare']["total"]))
            output.append(str(generalResultHolder['snvs'][callers]['rare']["lof"]))
            output.append(str(generalResultHolder['snvs'][callers]['rare']["functional"]))
            output.append(str(generalResultHolder['snvs'][callers]['rare']["silent"]))
            output.append(str(generalResultHolder['snvs'][callers]['rare']["others"]))

            output.append(str(generalResultHolder['snvs'][callers]['Ts']))
            output.append(str(generalResultHolder['snvs'][callers]['Tv']))
            output.append(str(division(generalResultHolder['snvs'][callers]['Ts'],
                                       generalResultHolder['snvs'][callers]['Tv']))[:4])


            output.append(str(generalResultHolder['indels'][callers]['total']))
            output.append(str(generalResultHolder['indels'][callers]['genotype']['2'])) # HOM
            output.append(str(generalResultHolder['indels'][callers]['genotype']['1'])) # HET

            output.append(str(division(generalResultHolder['indels'][callers]['genotype']['1'],
                                       generalResultHolder['indels'][callers]['genotype']['2']))[:4])
            output.append(str(generalResultHolder['indels'][callers]['common']["total"]))
            output.append(str(generalResultHolder['indels'][callers]['common']["coding"]))
            output.append(str(generalResultHolder['indels'][callers]['common']["nonCoding"]))
            output.append(str(generalResultHolder['indels'][callers]['rare']["total"]))
            output.append(str(generalResultHolder['indels'][callers]['rare']["coding"]))
            output.append(str(generalResultHolder['indels'][callers]['rare']["nonCoding"]))
            output.append(str(generalResultHolder['indels'][callers]['n3']))
            output.append(str(generalResultHolder['indels'][callers]['nn3']))
            output.append(str(division(generalResultHolder['indels'][callers]['n3'],
                                       generalResultHolder['indels'][callers]['nn3']))[:4])


            print "\t".join(output)


def main():
    print getHeader()
    generateResultsHolder()
    MAF_BED_dict = prepareAF()
    run(sys.argv[1], MAF_BED_dict) #'/Users/Macia/Desktop/gerp/qc_sorted.vcf.gz'


if __name__ == "__main__":
    if len(sys.argv) == 2:
        print getHeader()
    main()