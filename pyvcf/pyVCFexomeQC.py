"""
Before using this script make sure you have both UK10K and 1KG MAF otherwise change the script
Given a path to a VCF directory (vcf.gz or vcf version 4.0 or above),
for each sample and for each FILTER (e.g. PASS or inDel in the 6th column)
this script will print a table of SNVs and indels counts:

IMPORTANT note:
rare SNVs are those with 1KG MAF <= 0.01
rare INDELS are not in dbSNP (i.e. without rs ID). At the time, there is no indels MAF from the 1KG (communication with Petr Damneck)

Total -->
	total
	total_HOM
	total_HET

SNVs ->
	Total
		Common 
			Funcntional
			LOF
			Slient
			Others
		RARE (MAF =< 1% 1000 genomes )
			Funcntional
			LOF
			Slient
			Others
		Ts
		Tv
		Ts/Tv ratio

Indels ->
	Total
		common (in dbSNP)
			Coding
			Non-coding
		rare (not in dbSNP)
			Coding
			Non-coding
		multiple of three "n3" (coding)
		non-multiple of three "n3" (coding)
		n3/nn3 ratio (coding only)

3 Mar 2013
    - Move script to Git/pyVCF
    - Remove redundant function and use those in pyVCFtoolbox
    - Correct for multiple ALTs
    - Add support for auto run with pyVCFexomeQC_auto.py
    - Add support for CQ to include (CQ, VCQ and CSQ) tags in info to make sure we get any CQ tag variation
    - Rare variant is any variant not common in 1KG, ESP or UK10K (cutoff is >0.01 as common). This is no longer
    depend on the VCF file itself but actually are matched using exact I/II and using the AF files at
    /resources/AF/maf_master_table_3AFs_QC_local.txt or /resources/AF/maf_master_table_3AFs_QC_farm.txt. The goal is
    to make QC independent from the pipeline that generate the VCF and may use different version of AF.

"""

import os
import sys
import pyVCFtoolbox as tlbx
from pyVCFmafAnnotate import *
import mafGenericFunctions as mgf


##########################################
# These are the global variables.
# Change them to match what you have in your VCF files.

ensemble_version   = 69


# for printing
snvs_keys = ["lof","functional","silent", "others"]
indels_keys = ["coding","nonCoding"]

def prepareAF():
    """
    Load required tabix files to annotate each variant with 1KG, ESP and UK10K_cohort
    """
    currentPlatform = tlbx.getPlatform()
    if currentPlatform == 'local':
        AFpathsFile =  'resources/AF/maf_master_table_3AFs_QC_local.txt'
    else:
        AFpathsFile =  'resources/AF/maf_master_table_3AFs_QC_farm.txt'

    mafBedPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), AFpathsFile )
    MAF_BED_dict = loadAFs(mafBedPath) # load the tabix index files for the MAF files
    return MAF_BED_dict

def getAFs(chrom,pos,ref,alt, MAF_BED_dict):
    sourceVarInfo = mgf.extractVarInfo(chrom,pos,ref,alt)
    AFList = []
    for mafType in sorted(MAF_BED_dict):
        mafReturned = None
        targetVariants = getMAF(sourceVarInfo, mafType)
        if targetVariants: # not empty
            mafReturned = findMatch_Ray(sourceVarInfo, targetVariants)
            pass
        if mafReturned:
            AFList.append(mafReturned)
        else:
            AFList.append('.')
    return AFList


def getTsTv(REF, ALT):
    purines = ['A','G']
    pyrimidines = ['C','T']
    if (REF in purines and ALT in purines) or \
            (REF in pyrimidines and ALT in pyrimidines):
        return "Ts"
    elif (REF in purines and ALT in pyrimidines) or \
            (REF in pyrimidines and ALT in purines):
        return "Tv"
    else:
        return None


def get_n3_nn3(REF, ALT):
    R = len(REF)
    A = len(ALT)
    size = R - A
    if size % 3 == 0:
        type = "n3"
    else:
        type = "nn3"
    return type

def getVarClass(varType,CQs):
    CQ = '.'
    if varType == "snv":
        for CQ in CQs:
            if CQ != ".":
                if CQ in LOF_classes:
                    return "lof", CQ
                elif CQ in functional_classes:
                    return "functional", CQ
                elif CQ in silent_classes:
                    return "silent", CQ
        return "others", CQ
    elif varType == "indel":
        for CQ in CQs:
            if CQ != ".":
                if CQ in LOF_classes or CQ in functional_classes or CQ in silent_classes:
                    return "coding", CQ
        return "nonCoding", CQ
    else:
        return varType, CQ # <DEL>, <DUP>, et al.


def division(dm, nm):
    try:
        return float(dm)/float(nm)
    except ZeroDivisionError:
        return 0

def getHeader():
    header = [
        "sample_id",
        "total_pass_non_pass","non_pass", "pass",
        "total_HOM", "total_HET","total_hom_het_ratio",
        "rare_HOM","rare_HET","rare_hom_het_ratio",
        "total_snv", "total_common_snv", "total_common_snv_prcnt", "total_rare_snv", "total_rare_snv_prcnt",
        "total_indel", "total_common_indel", "total_common_indel_prcnt", "total_rare_indel", "total_rare_indel_prcnt",
        "snv_common_lof", "snv_common_functional", "snv_common_silent", "snv_common_others",
        "snv_rare_lof", "snv_rare_functional", "snv_rare_silent","snv_rare_others",
        "snv_Ts", "snv_Tv", "snv_Ts_Tv_ratio",
        "indel_common_coding", "indel_common_NonCoding",
        "indel_rare_coding", "indel_rare_NonCoding",
        "indel_n3", "indel_nn3","indel_n3_nn3_ratio"
    ]
    return "\t".join(header)

def run(path, MAF_BED_dict, ensemble_version):
    sample_id = os.path.basename(path)
    f = tlbx.openFile(path)

    results = {
        "snvs":{"rare":{"functional":0,"lof":0,"silent":0,"others":0},
                "common":{"functional":0,"lof":0,"silent":0, "others":0},
                "Ts":0,"Tv":0},
        "indels":{"rare":{"coding":0,"nonCoding":0},
                  "common":{"coding":0,"nonCoding":0},
                  "n3":0,"nn3":0},
        "genotype":{
            "common":{
                "2":0,
                "1":0,
                },
            "rare":{
                "2":0,
                "1":0,
                },
            },
        }
    total = 0
    total_non_pass = 0
    total_pass = 0
    total_snv = 0
    total_common_snv = 0
    total_rare_snv = 0
    total_indel = 0
    total_common_indel = 0
    total_rare_indel = 0

    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            chrom = tlbx.removeChrPrefix(line[0])
            pos   = line[1]
            ref   = line[3]
            total += 1
            if not tlbx.isPass(line[6]):
                total_non_pass += 1
            else:
                total_pass += 1
                for altIdx, alt in enumerate(tlbx.getAlts(line[4])):
                    varType = tlbx.getVarType(line[3], alt)
                    CQ, VCQ,CSQ, VCQNC = tlbx.getInfo(['CQ','VCQ','CSQ','VCQNC'], line[7],altIdx) # to make sure we get any tag for CQ
                    AFs_list = getAFs(chrom, pos, ref, alt, MAF_BED_dict)
                    var_class, CQ = getVarClass(varType,[CQ,VCQ,CSQ,VCQNC])
                    GT = tlbx.getGT(line[8],line[9])
                    varIsCommon = tlbx.isCommon(AFs_list, 0.01)
                    if varType == "snv":
                        total_snv += 1
                        if not varIsCommon:
                            total_rare_snv += 1
                            results["snvs"]["rare"][var_class] += 1
                            if CQ in coding_classes:#hom/het ratio now includes coding variants only
                                results["genotype"]['rare'][str(GT)] += 1
                        else:
                            total_common_snv += 1
                            results["snvs"]["common"][var_class] += 1
                            if CQ in coding_classes:#hom/het ratio now includes coding variants only
                                results["genotype"]['common'][str(GT)] += 1
                        if CQ in coding_classes:
                            TsTv = getTsTv(ref, alt)
                            if TsTv:
                                results["snvs"][TsTv] += 1

                    elif varType == "indel":
                        total_indel += 1
                        if not varIsCommon:
                            total_rare_indel += 1
                            results["indels"]["rare"][var_class] += 1
                            if CQ in coding_classes:#hom/het ratio now includes coding variants only
                                results["genotype"]['rare'][str(GT)] += 1
                        else:
                            total_common_indel += 1
                            results["indels"]["common"][var_class] += 1
                            if CQ in coding_classes:#hom/het ratio now includes coding variants only
                                results["genotype"]['common'][str(GT)] += 1
                        if CQ in coding_classes:
                            n3_nn3 = get_n3_nn3(ref, alt)
                            results["indels"][n3_nn3] += 1

    f.close()
    # for printing
    output = [sample_id]
    common_het = results['genotype']['common']['1']
    common_hom = results['genotype']['common']['2']
    rare_het   = results['genotype']['rare']['1']
    rare_hom   = results['genotype']['rare']['2']
    total_hom_het_ratio = round(float(common_het+rare_het) / float(common_hom+rare_hom),2)
    rare_hom_het_ratio  = round(float(rare_het) / float(rare_hom),2)
    output.append(str(total))
    output.append(str(total_non_pass))
    output.append(str(total_pass))
    output.append(str(common_hom + rare_hom)) # total HOM
    output.append(str(common_het + rare_het)) # total HET
    output.append(str(total_hom_het_ratio))
    output.append(str(rare_hom))
    output.append(str(rare_het))
    output.append(str(rare_hom_het_ratio))
    output.append(str(total_snv))
    output.append(str(total_common_snv))
    output.append(str(division(total_common_snv, total_snv)*100)[:4])
    output.append(str(total_rare_snv))
    output.append(str(division(total_rare_snv,total_snv)*100)[:4])
    output.append(str(total_indel))
    output.append(str(total_common_indel))
    output.append(str(division(total_common_indel,total_indel)*100)[:4])
    output.append(str(total_rare_indel))
    output.append(str(division(total_rare_indel,total_indel)*100)[:4])

    output.append(str(results['snvs']['common']["lof"]))
    output.append(str(results['snvs']['common']["functional"]))
    output.append(str(results['snvs']['common']["silent"]))
    output.append(str(results['snvs']['common']["others"]))

    output.append(str(results['snvs']['rare']["lof"]))
    output.append(str(results['snvs']['rare']["functional"]))
    output.append(str(results['snvs']['rare']["silent"]))
    output.append(str(results['snvs']['rare']["others"]))

    output.append(str(results['snvs']['Ts']))
    output.append(str(results['snvs']['Tv']))
    output.append(str(division(results['snvs']['Ts'],results['snvs']['Tv']))[:4])

    output.append(str(results['indels']['common']["coding"]))
    output.append(str(results['indels']['common']["nonCoding"]))
    output.append(str(results['indels']['rare']["coding"]))
    output.append(str(results['indels']['rare']["nonCoding"]))
    output.append(str(results['indels']['n3']))
    output.append(str(results['indels']['nn3']))
    output.append(str(division(results['indels']['n3'],results['indels']['nn3']))[:4])

    print "\t".join(output)


def main(filePath, ensemble_version):
    MAF_BED_dict = prepareAF()
    run(filePath, MAF_BED_dict, ensemble_version) 

if __name__ == "__main__":
	try:
		filePath = sys.argv[1]
		ensemble_version =(sys.argv[2])
		noHeader = sys.argv[3]
		if noHeader == "noHeader":
			pass
		else:
			print getHeader()
		
		LOF_classes        = tlbx.getVarClass("lof", ensemble_version)
		functional_classes = tlbx.getVarClass("functional", ensemble_version)
		silent_classes     = tlbx.getVarClass("silent",ensemble_version)
		coding_classes     = tlbx.getVarClass("all", ensemble_version)
		
		# A super list for all coding classes to calculate Ts/Tv and n3/nn3
		
		for lst in (LOF_classes,functional_classes, silent_classes):
			coding_classes.extend(lst)
	
		main(filePath, ensemble_version)
	except Exception as e:
		print e
		print "Incomplete arguments. Usage example:"
		print "\tpython pyVCFexomeQC.py vcf ensemble noHeader"
		sys.exit(1)