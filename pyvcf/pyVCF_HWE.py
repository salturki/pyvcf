from pyvcf import pyVCFtoolbox as tlbx
import sys


def annotateVCF():
    path = "/Users/Macia/Desktop/UK10K_replication/REL-2013-05-16/single-sample/v1_merged/3.maf_merged/chd_pass_merged_13Jun13_EVA_3maf.vcf.gz"#sys.argv[1]
    f = tlbx.openFile(path)
    print "\t".join(["chrom", "pos", "ref", "alt", "obs_hets", "obs_hom1", "obs_hom2", "eHWE_p_value"])
    for line in f:
        if not line.startswith("#"):
            line  = line.strip().split("\t")
            chrom = line[0]
            pos   = line[1]
            ref   = line[3]
            alt   = line[4]
            if "," not in alt: # only bi-alleleic sites
                GTs   = tlbx.getMultiGTs(line[8], line[9:])
                obs_hets = GTs.count(1)
                obs_hom1 = GTs.count(2)
                obs_hom2 = GTs.count(0)
                eHWE_p_value = tlbx.getHWEexactPvalues(obs_hets, obs_hom1, obs_hom2)
                if eHWE_p_value:
                    print "\t".join([chrom, pos, ref, alt, str(obs_hets), str(obs_hom1), str(obs_hom2), str(eHWE_p_value)])
                else:
                    print "\t".join([chrom, pos, ref, alt, str(obs_hets), str(obs_hom1), str(obs_hom2), "NA"])

def hwePerChromStats():
    # in a vcf that already annotated with annotateVCF(), this function print stats about HWE
    # for each chrom
    # rare vs. common variant
    # classes snvs vs indels
    path = "/Users/Macia/Desktop/UK10K_replication/REL-2013-05-16/single-sample/v1_merged/8.maf_merged_HWE_filtered/vcf/raw/find_pass_merged_13Jun13_EVA_3maf_eHWE.vcf.gz"
    f = tlbx.openFile(path)
    hwe = {}
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            chrom = line[0]
            pos   = line[1]
            ref   = line[3]
            alt   = line[4]
            if "," not in alt:
                AF_MAX, eHWE, CQ = tlbx.getInfo(["1KG_AF", "eHWE", "CQ"],line[7],0)
                if not hwe.has_key(chrom):
                    hwe[chrom] = {"low":[],"high":[]}
                if float(eHWE) > 0.25:
                    hwe[chrom]["high"].append(float(eHWE))
                else:
                    hwe[chrom]["low"].append(float(eHWE))
    print "\t".join(["chrom","totalVar", "eHWE_high", "eHWE_low"])
    for chrom in sorted(hwe):
        highVar  = len(hwe[chrom]["high"])
        lowVar   = len(hwe[chrom]["low"])
        totalVar = highVar + lowVar
        print "\t".join([chrom,str(totalVar),str(highVar),str(lowVar)])


def main():
    # annotateVCF()
    hwePerChromStats()

if __name__ == "__main__":
    main()