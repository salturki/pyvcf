"""
Run this script after generating the output in pyVCFrareKnockouts.py

28 Feb 2013
sa9@sanger.ac.uk
"""

import pyVCFtoolbox as tlbx


lofDict = {}

def loadLOF(f):
    for line in f:
        if line.startswith("#"):
            header = line.strip().split("\t")
        else:
            line = line.strip().split("\t")
            sampleId, chrom, pos, ref, alt,GN, CQ, OKG, COHORT, ESP, DDD, CHD, childGT, motherGT, fatherGT, gender = line
            key = (chrom,pos,ref,alt)
            if tlbx.isCommon([OKG, COHORT, ESP, DDD, CHD],0.05):
                varSpread = 'common'
            else:
                varSpread = 'rare'
            if not lofDict.has_key(sampleId):
                lofDict[sampleId] = {
                    GN:{
                        varSpread:[(childGT,motherGT,fatherGT)]
                        }
                    }
            else:
                if not lofDict[sampleId].has_key(GN):
                    lofDict[sampleId][GN] = {
                        varSpread:[(childGT,motherGT,fatherGT)]
                    }
                else:
                    if not lofDict[sampleId][GN].has_key(varSpread):
                        lofDict[sampleId][GN][varSpread] = [(childGT,motherGT,fatherGT)]
                    else:
                        lofDict[sampleId][GN][varSpread].append((childGT,motherGT,fatherGT))

def countByMode():
    genes = {
        'rare':{
            "AR":[],
            "AD":[],
            },
        'common':{
            "AR":[],
            "AD":[],
            },
    }
    samples = {}
    for sampleId in lofDict:
        if not samples.has_key(sampleId):
            samples[sampleId] = {"rare":{"AR":0,"AD":0},"common":{"AR":0,"AD":0}}
            for GN in lofDict[sampleId]:
                for varSpread in lofDict[sampleId][GN]:
                    genotypes = []
                    for GT in lofDict[sampleId][GN][varSpread]:
                        genotypes.append(GT)
                        if GT == ("2","1","1"):
                            samples[sampleId][varSpread]["AR"] += 1
                            if GN not in genes[varSpread]["AR"]:
                                genes[varSpread]["AR"].append(GN)
                        if GT[0] == "1":
                            samples[sampleId][varSpread]["AD"] += 1
                            if GN not in genes[varSpread]["AD"]:
                                genes[varSpread]["AD"].append(GN)
                    if ("1","0", "1") in genotypes and ("1","1","0") in genotypes:
                        samples[sampleId][varSpread]["AR"] += 1
                        if GN not in genes[varSpread]["AR"]:
                            genes[varSpread]["AR"].append(GN)
    print "nbrOfGenes\tAR\tAD"
    print "Rare\t",   len(genes['rare']["AR"]),   "\t",   len(genes['rare']["AD"])
    print "Common\t", len( genes['common']["AR"]), "\t",   len(genes['common']["AD"])
    print "-------------"


    for varSpread in ['rare','common']:
        for model in ['AR', 'AD']:
            sampleCounter = 0
            nbrOfLof = 0
            for sampleId in samples:
                sampleCounter += 1
                nbrOfLof += samples[sampleId][varSpread][model]
            print "%s|%s\t" % (varSpread,model), float(nbrOfLof)/sampleCounter



def main():
    f  = open("/Users/Macia/Dropbox/Team29/MyProjects/CHD/Rare_knockouts/01-03-2013/rareKnockouts_LOF_all_Chr_137trios.txt",'r')
    loadLOF(f)
    countByMode()
main()