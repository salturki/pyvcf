"""
Given a list of trios VCF files (trio_list.txt), this script will generate table of each variants
transmitted (vs non-transmitted). Only HET variants are considered
We need to filter for QUAL / GQ and see how this affected transmission rate.

26 Apr 2013
sa9@sanger.ac.uk
"""


import pyVCFtoolbox as tlbx
import pyVCF_tdt_table



def getRecessiveVariants():
    print "\t".join(['trioId',"sampleId",
                     "chrom", "pos", "ref", "alt",
                     "gene", "CQ",
                     "ESP_AF", "UK10K_AF", "local_AF",
                     "childGT", "motherGT", "fatherGT"])
    variants = pyVCF_tdt_table.variants
    holder = {}
    for gene in variants:
        compuondHolder = {}
        for key in variants[gene]:
            chrom, pos, ref, alt = key
            for trioId in variants[gene][key]:
                ESP_AF = UK10K_AF = CQ =  None
                for sampleId in variants[gene][key][trioId]:
                    member   = variants[gene][key][trioId][sampleId]['member']
                    GT       = variants[gene][key][trioId][sampleId]['GT']
                    ESP_AF   = variants[gene][key][trioId][sampleId]['ESP_AF']
                    UK10K_AF = variants[gene][key][trioId][sampleId]['UK10K_AF']
                    local_AF = variants[gene][key][trioId][sampleId]['local_AF']
                    if GT is None:
                        GT = 0
                    if GT:
                        CQ      = variants[gene][key][trioId][sampleId]['CQ']
                    if member == 'child':
                        childGT = GT
                    elif member == 'mother':
                        motherGT = GT
                    elif member == 'father':
                        fatherGT = GT
                GTkey = (childGT,motherGT, fatherGT)

                if CQ in tlbx.getVarClass('lof_functional') and not tlbx.isCommon([local_AF],0.01):
                    newLine = [trioId, sampleId, chrom, pos, ref, alt,gene, CQ, ESP_AF, UK10K_AF,
                               local_AF,childGT, motherGT, fatherGT]
                    if GTkey in [(1,0,1),(1,1,0)]:
                        if not compuondHolder.has_key(trioId):
                            compuondHolder[trioId] = {GTkey:[newLine]}
                        else:
                            if not compuondHolder[trioId].has_key(GTkey):
                                compuondHolder[trioId][GTkey] = [newLine]
                            else:
                                compuondHolder[trioId][GTkey].append(newLine)
                    if GTkey == [2,1,1]:
                        print "\t".join([str(x) for x in newLine])
        for trioId in compuondHolder:
            if (1,0,1) in compuondHolder[trioId] and (1,1,0) in compuondHolder[trioId]:
                for GTkey in [(1,0,1),(1,1,0)]:
                    for newLine in compuondHolder[trioId][GTkey]:
                        print "\t".join([str(x) for x in newLine])

    for CQ in holder:
        if CQ:
            for GTkey in sorted(holder[CQ]):
                print CQ, "\t", GTkey, "\t", holder[CQ][GTkey]['all'],\
                    "\t", holder[CQ][GTkey]['rare_uk10k_esp'],\
                    "\t", holder[CQ][GTkey]['rare_local']

def main():
    trio_list_path = '/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/dng/trios_list.txt'
    vcfDir    = '/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/vcf/GAPI/merged_GDS_maf'
    pyVCF_tdt_table.loadTriosList(trio_list_path, vcfDir)
    pyVCF_tdt_table.parseTrios()
    pyVCF_tdt_table.calculate_local_AF()
    getRecessiveVariants()


if "__main__" == __name__:
    main()