"""
Given a list of trios VCF files (trio_list.txt), this script will generate table of each variants
transmitted (vs non-transmitted). Only HET variants are considered
We need to filter for QUAL / GQ and see how this affected transmission rate.

23 Apr 2013
sa9@sanger.ac.uk
"""

import os
import copy
import pyVCFtoolbox as tlbx
from copy import deepcopy
from scipy import stats

trios_list   = {}
trios_holder = {}
genes_list   = {}
variants     = {}
vcfFiles     = {}

trio_exclusion_list = [ # has possible contamination
    "848",
    "142",
    "530",
    "545",
    "611",
    "758",
    "848"
]

GT_config_errors = [
    [0,0,0],
    [0,0,2],
    [0,1,2],
    [0,2,0],
    [0,2,1],
    [0,2,2],
    [1,0,0],
    [1,2,2],
    [2,0,0],
    [2,0,1],
    [2,0,2],
    [2,1,0],
    [2,2,0],
    [2,2,2],
]

# rules when we have 2,0,0 genotype configuration
dnm_GT_allowed = {
    "MT":{"male":True,"female":True},
    "X":{"male":True},
    "Y":{"male":True}
}

def loadTriosList(trio_list_path, vcfDir):
    f = open(trio_list_path, 'r')
    for line in f:
        if not line.startswith("trio_id"):
            line = line.strip().split("\t")
            trioId    = line[0]
            childId   = line[1]
            motherId  = line[4]
            fatherId  = line[7]
            gender    = line[10].lower() # child gender
            childVcf  = os.path.join(vcfDir, childId  + ".vcf.gz")
            motherVcf = os.path.join(vcfDir, motherId + ".vcf.gz")
            fatherVcf = os.path.join(vcfDir, fatherId + ".vcf.gz")
            trios_list[trioId] = {childId:childVcf,
                                  motherId:motherVcf,
                                  fatherId:fatherVcf}
            trios_holder[trioId] = {childId: {"GT":None, "member":'child' , "CQ":'.', "varType":".",  "gender":gender,
                                              "AF_MAX":0,"OKG_AF":0, "ESP_AF":0, "UK10K_AF":0, "local_AF":0},
                                    motherId:{"GT":None, "member":'mother', "CQ":'.', "varType":".", "gender":"female",
                                               "AF_MAX":0,"OKG_AF":0, "ESP_AF":0, "UK10K_AF":0, "local_AF":0},
                                    fatherId:{"GT":None, "member":'father', "CQ":'.', "varType":".",  "gender":"male",
                                               "AF_MAX":0, "OKG_AF":0, "ESP_AF":0, "UK10K_AF":0, "local_AF":0}}

            vcfFiles[childId]  = (trioId, childVcf)
            vcfFiles[fatherId] = (trioId, motherVcf)
            vcfFiles[motherId] = (trioId, fatherVcf)

def getGTconfig():
    GTs = [0,1,2]
    holder = {}
    for c in GTs:
        for m in GTs:
            for f in GTs:
               holder[(c,m,f)] = 0
    return holder


def tdt_test(b,c):
    b = float(b)
    c = float(c)
    return (b-c)**2 / (b+c)

def calculate_local_AF():
    """
    Annotate the dictionary of variabts with local_AF
    """
    for gene in variants:
        for key in variants[gene]:
            local_AF = 0
            sampleCount = 0.
            for trioId in variants[gene][key]:
                for sampleId in variants[gene][key][trioId]:
                    sampleCount += 1
                    GT      = variants[gene][key][trioId][sampleId]['GT']
                    if GT is None:
                        GT = 0
                    local_AF += GT
            local_TOF_AF  = round(local_AF / (sampleCount * 2),5) # 627 samples in 209 TOF trios and multiply with 2 alleles
            for trioId in variants[gene][key]:
                for sampleId in variants[gene][key][trioId]:
                    variants[gene][key][trioId][sampleId]['local_AF'] = local_TOF_AF



def getTdtTable(geneData, gene, request, mafType):
    """
    Given all genotypes from trios of a locus, this function returns a 2 by 2 table of TDT counts.
        - Skip HOM variants in parents.
        - Needs at least one HET parent
    request:
        - noCollapse --> print every variant
        - collapseByLocus --> print collapsed variants by Locus
    """

    gene_holder = dict()

    for key in geneData:
        chrom, pos, ref, alt = key
        locus_holder = {"transmitted__AB":0,  "non_transmitted_AB":0}
        for trioId in geneData[key]:
            for sampleId in geneData[key][trioId]:
                member  = geneData[key][trioId][sampleId]['member']
                GT      = geneData[key][trioId][sampleId]['GT']
                if member == 'child':
                    gender = geneData[key][trioId][sampleId]['gender']
                if GT is None:
                    GT = 0
                if GT:
                    CQ       = geneData[key][trioId][sampleId]['CQ']
                    varType  = geneData[key][trioId][sampleId]['varType']
                    AF_MAX   = geneData[key][trioId][sampleId]['AF_MAX']
                    OKG_AF   = geneData[key][trioId][sampleId]['OKG_AF']
                    ESP_AF   = geneData[key][trioId][sampleId]['ESP_AF']
                    UK10K_AF = geneData[key][trioId][sampleId]['UK10K_AF']
                    local_AF = geneData[key][trioId][sampleId]['local_AF']
                    maf_dict = {'local':[local_AF],'external':[UK10K_AF,ESP_AF]}
                # local_AF += GT
                if member == 'child':
                    childGT = GT
                elif member == 'mother':
                    motherGT = GT
                elif member == 'father':
                    fatherGT = GT

            if [childGT, motherGT, fatherGT] not in GT_config_errors:
                # accept DNM as hemizygous only according to rules in dnm_GT_allowed dict
                if [childGT, motherGT, fatherGT] == [2,0,0]:
                    try:
                        forProcessing = dnm_GT_allowed[chrom][gender] # True
                    except:
                        forProcessing = False
                else:
                    forProcessing = True
                if forProcessing and not tlbx.isCommon(maf_dict[mafType],0.01):
                    if not gene_holder.has_key(CQ):
                        gene_holder[CQ] = deepcopy(locus_holder)

                    if request == 'noCollapse':
                        # print the line to show all variants
                        print "\t".join([str(x) for x in [chrom, pos, ref, alt, gene, varType,
                                                      CQ,AF_MAX, OKG_AF, ESP_AF, UK10K_AF, local_AF,trioId,
                                                      childGT, motherGT, fatherGT]])

                    locus_holder['transmitted__AB']       += childGT
                    gene_holder[CQ]['transmitted__AB']    += childGT
                    if (motherGT+fatherGT-childGT) > 0:
                        locus_holder['non_transmitted_AB']    += (motherGT+fatherGT-childGT)
                        gene_holder[CQ]['non_transmitted_AB'] += (motherGT+fatherGT-childGT)



        if request == 'collapseByLocus':
            if  locus_holder['transmitted__AB'] > 0 or  locus_holder['non_transmitted_AB'] > 0:
                b = locus_holder['transmitted__AB']
                c = locus_holder['non_transmitted_AB']
                tdt_value = tdt_test(b,c)
                diff = b - c
                df   = 1  # degree of freedom
                pValue = 1 - stats.chi2.cdf(tdt_value, df)
                print "\t".join([str(x) for x in [chrom, pos, ref, alt, gene,varType,
                                                      CQ, AF_MAX,  OKG_AF, ESP_AF, UK10K_AF, local_AF, locus_holder['transmitted__AB'],
                                                      locus_holder['non_transmitted_AB'],
                                                      tdt_value,pValue, diff]])
    if request == 'collapseByGene':
        for CQ in gene_holder:
            if gene_holder[CQ]['transmitted__AB'] > 0 or gene_holder[CQ]['non_transmitted_AB'] > 0:
                b = gene_holder[CQ]['transmitted__AB']
                c = gene_holder[CQ]['non_transmitted_AB']
                tdt_value = tdt_test(b,c)
                diff   = b - c
                df     = 1 # degree of freedom
                pValue = 1 - stats.chi2.cdf(tdt_value, df)
                print "\t".join([str(x) for x in [ gene, CQ,
                                                   gene_holder[CQ]['transmitted__AB'],
                                                   gene_holder[CQ]['non_transmitted_AB'],
                                                   tdt_value, pValue, diff]])


def parseTrios():
    counter = 0
    for sampleId in vcfFiles:
        # while counter < 50:
            trioId, vcfPath = vcfFiles[sampleId]
            if trioId not in trio_exclusion_list:
                f = tlbx.openFile(vcfPath)
                counter += 1
                for line in f:
                    if not line.startswith("#"):
                        line  = line.strip().split("\t")
                        chrom = line[0]
                        pos   = line[1]
                        ref   = line[3]
                        alt   = line[4]
                        key   = (chrom,pos, ref, alt)
                        if ',' not in alt: # skip multi-allelic sites
                            CQ , GN,AF_MAX, OKG_AF, ESP_AF, UK10K_AF = tlbx.getInfo(['VCQ',
                                                                              'VGN',
                                                                              "AF_MAX",
                                                                              "1KG_AF",
                                                                              "ESP_AF",
                                                                              "UK10K_cohort_AF"], line[7], 0)

                            varType = tlbx.getVarType(ref,alt)
                            if tlbx.isPass(line[6]):
                                if CQ in tlbx.getVarClass('all'):
                                    if not variants.has_key(GN):
                                        variants[GN] = {key:copy.deepcopy(trios_holder)}
                                    else:
                                        if not variants[GN].has_key(key):
                                            variants[GN][key] = copy.deepcopy(trios_holder)
                                    GT = tlbx.getGT(line[8],line[9])

                                    variants[GN][key][trioId][sampleId]["GT"]       = GT
                                    variants[GN][key][trioId][sampleId]["CQ"]       = CQ
                                    variants[GN][key][trioId][sampleId]["varType"]  = varType
                                    variants[GN][key][trioId][sampleId]["AF_MAX"]   = AF_MAX
                                    variants[GN][key][trioId][sampleId]["OKG_AF"]   = OKG_AF
                                    variants[GN][key][trioId][sampleId]["ESP_AF"]   = ESP_AF
                                    variants[GN][key][trioId][sampleId]["UK10K_AF"] = UK10K_AF
                                    pass


def runTDT():
    # change request to noCollapse, collapseByLocus or collapseByGene to output different TDT tables
    requests = ['noCollapse','collapseByLocus','collapseByGene']
    mafs = ['local','external'] # local is local_AF and external is UK10K_AF and ESP_AF
    for request in requests:
        for maf in mafs:
            print 50 * "#" , request , " | ", maf
            if request == 'noCollapse':
                header = ['chrom','pos','ref','alt','gene','varType','CQ',"AF_MAX", "OKG_AF","ESP_AF","UK10K_AF",
                          "local_TOF_AF",
                          'trioId','childGT','motherGT','fatherGT']
            elif request == 'collapseByLocus':
                header = ['chrom','pos','ref','alt','gene','varType','CQ',"AF_MAX",  "OKG_AF","ESP_AF","UK10K_AF",
                          "local_TOF_AF",
                          'transmitted_AB','non_transmitted_AB', "TDT_test", "pValue", "trans_nonTrans_diff"]
            elif request == 'collapseByGene':
                header = ['gene','CQ','transmitted_AB','non_transmitted_AB', "TDT_test", "pValue", "trans_nonTrans_diff"]
            print "\t".join(header)
            for gene in variants:
                getTdtTable(variants[gene], gene, request, maf)


def main():
    trio_list_path = '/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/dng/trios_list.txt'
    vcfDir    = '/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/vcf/GAPI/merged_DGS_DDD2Kmaf'
    loadTriosList(trio_list_path, vcfDir)
    parseTrios()
    calculate_local_AF()
    runTDT()

if "__main__" == __name__:
    main()