"""
Given a directory or a list of VCF files perform Exome QC and save the plots.

3 Mar 2013
sa9@sangre.ac.uk
"""

import sys
import os
import pyVCFtoolbox as tlbx
import pyVCFexomeQC


def createAndRunListOfCommands(outputRootPath, paths, ensemble_version):
    #Write the header for the QC Table and close the file.
    qcTablePath = os.path.join(outputRootPath, "QC_table.txt")
    oTable = open(qcTablePath,'w')
    oTable.write(pyVCFexomeQC.getHeader()+"\n")
    oTable.close()


    if tlbx.getPlatform() == "farm":
        # Write a list of commands to run with as a job array
        cmdFilePath = os.path.join(outputRootPath, "QC_commands.txt")
        oCmd = open(cmdFilePath,'w')
        nbrOfJobs = 0
        for path in paths:
            qcScriptPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pyVCFexomeQC.py")
            cmd = 'python2.7 %s %s %d noHeader >> %s\n' % (qcScriptPath, path, ensemble_version, qcTablePath)
            oCmd.write(cmd)
            nbrOfJobs += 1
        oCmd.close()
        jobName = tlbx.stampJobName("doExomeQC")
        nbrOfParallelJobs = nbrOfJobs
        tlbx.runJobArray(cmdFile=cmdFilePath,
                         jobName=jobName,
                         nbrOfJobs=nbrOfJobs,
                         nbrOfParallelJobs=nbrOfParallelJobs,
                         previousJobs=None)
        return qcTablePath, jobName # to make cleaning job wait for this job array to finish
    else:
        # Run the QC locally, no need to do anything else. Return None
        for path in paths:
            qcScriptPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pyVCFexomeQC.py")
            cmd = 'python %s %s %d noHeader >> %s' % (qcScriptPath, path, ensemble_version, qcTablePath)
            print cmd
            os.system(cmd)
        return qcTablePath, None

def plotInR(qcTablePath,outputRootPath, prevJobs):
    """
    Plot SNV and INDEL QC at the output directory
    """
    Rscrtip = os.path.join(os.path.dirname(os.path.realpath(__file__)), "pyVCFexomeQC_plots.R")
    cmd = 'Rscript %s %s %s' % (Rscrtip, qcTablePath, outputRootPath)
    if tlbx.getPlatform() == "farm":
        jobName = tlbx.stampJobName("Rplots")
        tlbx.runSingleJob(cmd=cmd,jobName=jobName,outputPath=outputRootPath,previousJobs=prevJobs)
        return jobName
    else:
        print cmd
        os.system(cmd)
        return None


def cleanFiles(outputRootPath, prevJobs):
    cmd  = "cd %s; rm *.err *.out "  % outputRootPath
    jobName = tlbx.stampJobName("cleanTmpFiles")
    tlbx.runSingleJob(cmd=cmd,jobName=jobName,outputPath=outputRootPath,previousJobs=prevJobs)

def main():
    inputRootPath  = sys.argv[1]
    outputRootPath = sys.argv[2]
    try:
        ensemble_version = int(sys.argv[3])
    except Exception as e:
        print e
        print "Couldn't find the ensemble version. Assuming it is 69 or older"
        ensemble_version = 69

    paths = tlbx.walker(inputRootPath,'.vcf.gz')
    # workflow
    qcTablePath, prevJobs = createAndRunListOfCommands(outputRootPath, paths, ensemble_version)
    prevJobs = plotInR(qcTablePath,outputRootPath, prevJobs)
    if prevJobs:
        cleanFiles(outputRootPath, prevJobs)
    else:
        # it is running locally and no need to clean *err or *out files
        pass


if __name__ == '__main__':
    main()