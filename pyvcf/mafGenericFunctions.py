import sys
import gzip

def openFile(path):
    try:
        if path.endswith(".gz"):
            return gzip.open(path,'r')
        else:
            return open(path,'r')
    except Exception as e:
        print e
        sys.exit(1)


def createChrLst(chrListCompressed):
    chr_list = chrListCompressed.strip().split(",")
    return chr_list

def removeChrPrefix(chrom):
    if chrom.upper().startswith("CHR"):
        chrom = chrom[3:]
    return chrom

def getInfo(tags,info, altIdx):
    """
    This function is used to extract values in the INFO column in the VCF file.
    Accept a list of tag names and the INFO filed and returns a dictionary values.
    """
    holder = dict(zip(tags,['.' for x in tags]))
    for item in info.split(";"):
        try:
            k,v = item.split("=")
            if "," in v: # if there are multi-values for the multi-alleles
                v = v.split(',')[altIdx]
        except:
            k = item
            v = '1'
        if k in holder:
            holder[k] = v
    return holder


def isIndel(ref, alt):
    if len(ref) != len(alt):
        return True
    else:
        return False

def checkPoint(ref,alt,varType):
    itIsIndel = isIndel(ref,alt)
    if varType == 'indels' and itIsIndel:
        return True
    elif varType == 'snvs' and not itIsIndel:
        return True
    return False


def getIndelDirection(ref,alt):
    if len(ref) > len(alt):
        return "del"
    elif len(ref) < len(alt):
        return "ins"
    else:
        print "Couldn't determine the direction of this indel (%s,%s)" % (ref,alt)
        sys.exit(1)

def getSlice(ref, alt):
    IndelSlice = "."
    if len(ref) > len(alt): #deletion
        IndelSlice = ref[len(alt):]
    elif len(alt) > len(ref): #insertion
        IndelSlice = alt[len(ref):]
    return IndelSlice

def getAlts(alts):
    try:
        alts = alts.split(",")
    except:
        alts = [alts]
    return alts


def REFandALTvalid(ref,alt):
    for char in ref:
        if char not in ['C','T','G','A',',']:
            print "Unexpected char in the REF column, [%s] was found. Only [A,C,G,T, and comma] are allowed." % ref
            print "Please check if you supplied the right column index for REF with option -c"
            return False
    for char in alt:
        if char not in ['C','T','G','A',',']:
            print "Unexpected char in the ALT column, [%s] was found.  Only [A,C,G,T, and comma] are allowed." % alt
            print "Please check if you supplied the right column index for ALT with option -c"
            print "If this is the right ALT column, then these unexpected values need to be corrected first."
            return False
    return True

def extractVarInfo(chrom,pos,ref,alt):
    varInfo = {
        'type': '', # snv or indel
        'chr':'',
        'pos':'',
        'start':'',
        'end':'',
        'ref':'',
        'alt':'',
        'direction':'',  # ins or del
        'slice':'' ,
        }

    # get the type of the variant
    if len(ref) != len(alt):
        varInfo['type'] = 'indel'
    else:
        varInfo['type'] = 'snv'

    #annotate basic info
    varInfo['chr'] = chrom
    varInfo['pos'] = pos
    varInfo['ref'] = ref
    varInfo['alt'] = alt
    # annotate with indel info
    if varInfo['type'] == 'indel':
        varInfo['direction']  = getIndelDirection(ref,alt)
        varInfo['slice']      = getSlice(ref,alt)
        varInfo['start']      = int(pos) - 10 # pysam uses 0-based indexing
        varInfo['end']        = int(pos)  + len(varInfo['slice']) + 10
    else:
        varInfo['start']      = int(pos) - 1 # pysam uses 0-based indexing
        varInfo['end']        = int(pos) + 1

    return varInfo


