"""
For each pathway,
screen input EVAR output for rare coding variants in cases (CHD) and controls (DDD).
Count "1" if the sample has at least 2 genes with rare coding varaints
Finally do Fisher's excat test

18 Aug 2013
sa9@sanger.ac.uk
"""

import os, sys
import fisher
import itertools


pathways  = {}
samples   = {"cases":{},"controls":{}}
genotypes = ["1/1/0","1/0/1"]
results   = {}

def loadPathways(genesTarget=None):
    f = open("resources/pathways/c2.cp.kegg.v3.0.symbols.gmt")
    for line in f:
        line = line.strip().split()
        name = line[0]
        genes = line[2:]
        if genesTarget:
            for gene in genes:
                if gene in genesTarget:
                    pathways[name] = set(genesTarget).intersection(set(genes))
        else:
            pathways[name] = genes
    f.close()

def loadGenes():
    """
    This is only for the ToF replication 122 genes
    """
    genes = []
    f = open("/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/probes/Approach_E/122_genes.summary.txt","r")
    for line in f.readlines()[1:-2]:
        if not line.startswith("#"):
            gene = line.strip().split("\t")[3]
            if gene not in genes:
                genes.append(gene)
    f.close()
    return genes

def praseEVAR(group, path, projectName, genes):
    sampleCount = 0
    codingClass = ["NON_SYNONYMOUS_CODING","missense_variant"]
    for fileName in os.listdir(path):
        if fileName.endswith("evar.txt") or fileName.endswith("trio.txt"):
            if projectName in fileName:
                filePath = os.path.join(path, fileName)
                sampleId = fileName.split(".")[0]
                sampleCount += 1
                f = open(filePath, 'r')
                for line in f:
                    if not line.startswith("#"):
                        line  = line.strip().split("\t")
                        model = line[0].strip()
                        GN    = line[9]
                        GT    = line[14]
                        CQ    = line[10]
                        ref   = line[5]
                        alt   = line[6]
                        if len(ref) == len(alt) == 1: # include SNVs only
                            if model == "3" and (GN in genes or genes == []) and  CQ in codingClass:
                                #if GT in genotypes:
                                    if GN == "NOTCH1"  and GT  == '1/0/0':
                                        print sampleId, "\t".join(line)
                                        #print  "\t".join([line[2],line[3],line[3], "%s/%s" %(line[5],line[6]), "+"]) #sampleId, "\t",
                                    if not samples[group].has_key(sampleId):
                                        samples[group][sampleId] = {}
                                    if not samples[group][sampleId].has_key(GN):
                                        samples[group][sampleId][GN] = GT
                                    else:# the sample has mutliple rare missinse in the same gene
                                        tmp = samples[group][sampleId][GN] 
                                        pass
    return sampleCount


def countGenePerPathway(minNbrOfAffectedGene):
    for group in samples:
        for sampleId in samples[group]:
            for pathway in pathways:
                sampleGenes  = samples[group][sampleId].keys()
                pathwayGenes = pathways[pathway]
                sharedGenes  = set(sampleGenes).intersection(set(pathwayGenes))
                if len(sharedGenes) >= minNbrOfAffectedGene:
                    if not results.has_key(pathway):
                        results[pathway] = {"cases":{},"controls":{}}
                    if not results[pathway].has_key(sampleId):
                        results[pathway][group][sampleId] = []
                    for GN in sharedGenes:
                        GT = samples[group][sampleId][GN]
                        results[pathway][group][sampleId].append((GN,GT))


def doFET(size_A, size_B):
    for pathway in results:    
        groupA_yes = len(results[pathway]["cases"]) # number of cases
        groupA_no  = size_A - groupA_yes
        groupB_yes = len(results[pathway]["controls"])
        groupB_no  = size_B - groupB_yes
        try:
            oddRatio   = (groupA_yes * float(groupB_no)) / (groupB_yes * float(groupA_no))
        except:
            oddRatio = 0
        nbrOfGenes = len(pathways[pathway])
        p = fisher.pvalue(groupA_yes, groupA_no, groupB_yes, groupB_no)
        #if groupA_yes > 0: # ignore pahtway with 0 samples in the cases
            #print "\t".join([str(x) for x in [pathway,nbrOfGenes, groupA_yes, groupA_no, groupB_yes, groupB_no, round(p.left_tail,8), round(p.right_tail,8), round(p.two_tail,8), round(oddRatio,2)]])
            #print "\t".join([str(x) for x in [pathway,nbrOfGenes, groupA_yes, groupA_no, groupB_yes, groupB_no, p.left_tail, p.right_tail, p.two_tail, oddRatio]])
        results[pathway]['pvalues'] = [pathway,nbrOfGenes, groupA_yes, groupA_no, groupB_yes, groupB_no, p.left_tail, p.right_tail, p.two_tail, oddRatio] # save it here for other functions

def printFET():
    print "\t".join(["Pathway","nbrOfGenes","groupA_yes","groupA_no","groupB_yes","groupB_no","FET_left", "FET_right", "FET_both","OR"])
    for pathway in results:
        twoTails   = results[pathway]['pvalues'][8]
        groupA_yes = results[pathway]['pvalues'][2]
        #if twoTails < 0.05:
        if groupA_yes > 0:
            print "\t".join([str(x) for x in results[pathway]['pvalues']])

def getGenesCombination(geneList, minNbrOfUnits):
    combinationsList = []
    for L in range(minNbrOfUnits, len(geneList)+1):
        for subset in itertools.combinations(geneList, L):
           combinationsList.append(subset)
    return combinationsList


def printAffectedGenesCombination(minNbrOfUnits, analysisType):
    if analysisType == 'gene':
        print "\t".join(["Gene","groupA_yes","groupA_no","groupB_yes","groupB_no","FET_left", "FET_right", "FET_both","OR"])
    else:
        print "\t".join(["Genotype","groupA_yes","groupA_no","groupB_yes","groupB_no","FET_left", "FET_right", "FET_both","OR"])
    local_genes   = {} # for this function
    local_samples = {"cases":[],"controls":[]} # for this funcion
    processed     = {"cases":{},"controls":{}} # a holder of gene combination and samples so I don't count them more than once
    for pathway in results:
        if results[pathway].has_key('pvalues'):
            twoTail= results[pathway]['pvalues'][8]
            #if twoTail < 0.05:
            for group in ['cases','controls']:
                for sampleId in results[pathway][group]:
                    gene_holder = []
                    for GN, GT in results[pathway][group][sampleId]:
                        if analysisType == 'gene':
                            gene_holder.append(GN)
                        else:
                            gene_holder.append(GT)
                    
                    cmb_holder = getGenesCombination(gene_holder, minNbrOfUnits)#nbrOfUnits the length of
                    for gene_comb in cmb_holder:
                        found_matching_gene_comb = False
                        for old_gene_comb in local_genes:
                            if len(set(old_gene_comb).intersection( set(gene_comb) )) == max(len(gene_comb),len(old_gene_comb)) and not found_matching_gene_comb:
                                found_matching_gene_comb = True
                                if old_gene_comb not in processed[group]:
                                    processed[group][old_gene_comb] = []
                                if sampleId not in processed[group][old_gene_comb]:
                                    local_genes[old_gene_comb][group] += 1
                                    processed[group][old_gene_comb].append(sampleId)
                                    
                                #if 'MAPK1' in gene_comb: print sampleId, gene_comb, old_gene_comb
                                break
                        if not found_matching_gene_comb:
                            local_genes[gene_comb] = {'cases':0,'controls':0}
                            local_genes[gene_comb][group] += 1
                            found_matching_gene_comb = True
                            
                            #count once
                            if not processed[group].has_key(gene_comb):
                                processed[group][gene_comb] = []
                            processed[group][gene_comb].append(sampleId)
                            #if 'MAPK1' in gene_comb: print sampleId, 'MAPK1'
                        if sampleId not in local_samples[group]:
                            local_samples[group].append(sampleId)
    for gene in local_genes:
        size_A = len(local_samples['cases'])
        size_B = len(local_samples['controls'])
        groupA_yes = local_genes[gene]['cases'] # number of cases
        groupA_no  = 209 - groupA_yes
        groupB_yes = local_genes[gene]['controls'] 
        groupB_no  = 1080 - groupB_yes
        try:
            oddRatio   = (groupA_yes * float(groupB_no)) / (groupB_yes * float(groupA_no))
        except:
            oddRatio = 0
        p = fisher.pvalue(groupA_yes, groupA_no, groupB_yes, groupB_no)
        #if groupA_yes > 0:
        print "\t".join([str(x) for x in [gene, groupA_yes, groupA_no, groupB_yes, groupB_no, p.left_tail, p.right_tail, p.two_tail, oddRatio]])

def printAffectedGenes():
    """
    For every pathway that show burden in cases
    print each sample genotype and genes
    """
    print "\t".join(["Gene","groupA_yes","groupA_no","groupB_yes","groupB_no","FET_left", "FET_right", "FET_both","OR"])
    local_genes   = {} # for this function
    local_samples = {"cases":[],"controls":[]} # for this funcion
    #print "\t".join(["pathway","group","sampleId", "GN","GT"])
    processed_genes = []
    for pathway in results:
        if results[pathway].has_key('pvalues'):
            twoTail= results[pathway]['pvalues'][8]
            #if twoTail < 0.05:
            for group in ['cases','controls']:
                for sampleId in results[pathway][group]:
                    for GN, GT in results[pathway][group][sampleId]:
                        #print "\t".join([pathway, group, sampleId, GN, GT])
                        if not local_genes.has_key(GN):
                            local_genes[GN] = {'cases':0,'controls':0}
                        if (sampleId,GN) not in processed_genes:
                            local_genes[GN][group] += 1
                            processed_genes.append((sampleId,GN))
                        if sampleId not in local_samples[group]:
                            local_samples[group].append(sampleId)
                

        
    for gene in local_genes:
        size_A = len(local_samples['cases'])
        size_B = len(local_samples['controls'])
        groupA_yes = local_genes[gene]['cases'] # number of cases
        groupA_no  = 209 - groupA_yes
        groupB_yes = local_genes[gene]['controls'] 
        groupB_no  = 1080 - groupB_yes
        try:
            oddRatio   = (groupA_yes * float(groupB_no)) / (groupB_yes * float(groupA_no))
        except:
            oddRatio = 0
        p = fisher.pvalue(groupA_yes, groupA_no, groupB_yes, groupB_no)
        if groupA_yes > 0:
            print "\t".join([str(x) for x in [gene, groupA_yes, groupA_no, groupB_yes, groupB_no, p.left_tail, p.right_tail, p.two_tail, oddRatio]])

def main():
    minNbrOfAffectedGene = 1
    genes = loadGenes() # traget genes for replication ToF (122)
    #genes = [] # uncomment this line when you want to include all genes
    loadPathways(genes) 
    DDDpath = "/Users/Macia/Dropbox/Team29/MyProjects/HelpDesk/Matt/numbers_of_candidate_genes_per_trios/evar_results/12Aug2013/all_genotypes"
    TOFpath = "/Users/Macia/Dropbox/Team29/MyProjects/CHD/TOF_trio/Replication.study/Analysis/EVAR/all_genotypes"
    #DDDpath = "/Users/Macia/Dropbox/Team29/MyProjects/DDD/DDD_FEVA_output/13Aug2013/all_genotypes"
    
    size_A = praseEVAR("cases"   , TOFpath, "TOF", genes)
    size_B = praseEVAR("controls", DDDpath, "DDD", genes)
    
    countGenePerPathway(minNbrOfAffectedGene)
    doFET(size_A, size_B)
    printFET()

    printAffectedGenes()
    ##
    printAffectedGenesCombination(minNbrOfAffectedGene, 'gene')
    printAffectedGenesCombination(minNbrOfAffectedGene, 'genotype')
    
if __name__ == '__main__':
    main()