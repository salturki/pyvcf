'''

Take three folders (one for SamTools, GATK and Dindel) from GAPI pipeline and use

merge_GSD_VCFs_v5.py scrtip to merge them into the output folder

Matching files is based on the first number in their names


18 Feb 2013
sa9@sanger.ac.uk
'''

import os, sys, fnmatch
import optparse

files_dict = {}

def walker(DIR, caller):
    global files_dict
    for path, subdirs, files in os.walk(DIR):
        for name in files:
            if fnmatch.fnmatch(name, "*.vcf.gz"):
                ID = name.split("_")[0]
                f_path = os.path.join(path, name)
                if not files_dict.has_key(ID):
                    files_dict[ID] = {caller:f_path}
                else:
                    files_dict[ID][caller] = f_path
                    
def run_merge_script(output_folder):
    global files_dict
    for ID in files_dict.keys():
        o_path = os.path.join(output_folder, ID + ".vcf")
        #bsub -M 300000 -R"select[mem>300] rusage[mem=300]" -oo merge.log
        scriptPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "merge_GSD_VCFs_v5.py")
        cmd = 'python2.6 %s -S %s -G %s -D %s -o %s; vcf-sort %s | bgzip -c > %s.gz; rm %s; tabix -p vcf %s.gz' % \
          (scriptPath, files_dict[ID]['S'], files_dict[ID]['G'], files_dict[ID]['D'], o_path, o_path,o_path,o_path, o_path )
        print cmd
    

def main():
    f = open(sys.argv[1],'r')
    output_folder = sys.argv[2]
    for line in f:
        if not line.startswith("#"):
            line = line.strip().split("\t")
            SamTools_folder = line[1]
            GATK_folder     = line[2]
            Dindel_folder   = line[3]
            if SamTools_folder and GATK_folder and Dindel_folder:
                walker(SamTools_folder, "S")
                walker(GATK_folder,     "G")
                walker(Dindel_folder,   "D")
            else:
                print "Please make sure you have supplied a path to tab file with project name, samtoolVCFs, GATKVCFs, DindelVCFs paths."
                sys.exit(1)
    run_merge_script(output_folder)

if __name__ == '__main__':
    main()

#Example of the input file
'''#project	samtools	gatk	dindel
Newcastle_1	/nfs/gapi_release/SEQCAP/SEQCAP_Tetralogy_of_Fallot_Exome_Trios/2012-03-28/release/samtools_calls	/nfs/gapi_release/SEQCAP/SEQCAP_Tetralogy_of_Fallot_Exome_Trios/2012-03-28/release/GATK_calls	/nfs/gapi_release/SEQCAP/SEQCAP_Tetralogy_of_Fallot_Exome_Trios/2012-03-28/release/Dindel_calls
Newcastle_2	/nfs/gapi_release/SEQCAP/SEQCAP_Tetralogy_of_Fallot_Exome_Trios/2011-11-03/VcfAnno_Samtools_mcall_GRCh37_50MbCTRplus_e63_dbSNP134	/nfs/gapi_release/SEQCAP/SEQCAP_Tetralogy_of_Fallot_Exome_Trios/2011-11-03/VcfAnno_GATK_snp_call_GRCh37_50MbCTRplus_e63	/nfs/gapi_release/SEQCAP/SEQCAP_Tetralogy_of_Fallot_Exome_Trios/2011-11-03/VcfAnno_Dindel_GRCh37_e63_dbSNP134
CHD_UK	/nfs/gapi_release/SEQCAP/SEQCAP_Congenital_Heart_Disease_In_UK_Families/2011-11-10/VcfAnno_Samtools_mcall_GRCh37_50MbCTRplus_e64_dbSNP134	/nfs/gapi_release/SEQCAP/SEQCAP_Congenital_Heart_Disease_In_UK_Families/2011-11-10/VcfAnno_GATK_snp_call_GRCh37_50MbCTRplus_e64	/nfs/gapi_release/SEQCAP/SEQCAP_Congenital_Heart_Disease_In_UK_Families/2011-11-10/VcfAnno_Dindel_GRCh37_50MbCTRplus_e64_dbSNP134
SC_CHDT	/nfs/gapi_release/SEQCAP/SC_CHDT/2012-10-05/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDT/2012-10-05/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDT/2012-10-05/release/Dindel_calls
SC_CHDRB1	/nfs/gapi_release/SEQCAP/SC_CHDRB/2012-06-29_V3/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDRB/2012-06-29_V3/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDRB/2012-06-29_V3/release/Dindel_calls
SC_CHDRB2	/nfs/gapi_release/SEQCAP/SC_CHDRB/2012-06-29_V4/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDRB/2012-06-29_V4/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDRB/2012-06-29_V4/release/Dindel_calls
SC_CHDL1	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-05-17/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-05-17/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-05-17/release/Dindel_calls
SC_CHDL2	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-06-27/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-06-27/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-06-27/release/Dindel_calls
SC_CHDL3	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-04-17/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-04-17/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-04-17/release/Dindel_calls
SC_CHDL4	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-05-22/release/samtools_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-05-22/release/GATK_calls	/nfs/gapi_release/SEQCAP/SC_CHDL/2012-05-22/release/Dindel_calls
'''