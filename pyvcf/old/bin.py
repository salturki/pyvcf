'''
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A  -D hg19
select * from snp135 where chrom="chr1" and chromStart>10000 and chromEnd<15000 and bin in (585, 73, 9, 1, 0);

select * from allHg19RS_BW;
http://hgdownload.cse.ucsc.edu + /gbdb/hg19/bbi/All_hg19_RS.bw
'''
from cruzdb import Genome
from bx.bbi.bigwig_file import BigWigFile
import urllib2
from StringIO import StringIO
from biograpy import Panel, tracks, features
import matplotlib.pyplot as plt
from Bio import SeqFeature
import pysam

class VariantFeatures(features.BaseGraphicFeature):
    '''
     Draws features localized just to one position (SNV), span few bases (INDELs) or very long (CNV)

     Additional valid attributes:
         ===================== ==================================================
         Key                   Description
         ===================== ==================================================
         marker                marker symbol, accepts all matplotlib marker types
                               default is circle marker ``'o'``
         markersize            marker size as in matplotlib. default  is ``2``.
         ===================== ==================================================

     Available matplotlib marker types:


         * ``'.'``     point marker
         * ``','``     pixel marker
         * ``'o'``     circle marker
         * ``'v'``     triangle_down marker
         * ``'^'``     triangle_up marker
         * ``'<'``     triangle_left marker
         * ``'>'``     triangle_right marker
         * ``'1'``     tri_down marker
         * ``'2'``     tri_up marker
         * ``'3'``     tri_left marker
         * ``'4'``     tri_right marker
         * ``'s'``     square marker
         * ``'p'``     pentagon marker
         * ``'*'``     star marker
         * ``'h'``     hexagon1 marker
         * ``'H'``     hexagon2 marker
         * ``'+'``     plus marker
         * ``'x'``     x marker
         * ``'D'``     diamond marker
         * ``'d'``     thin_diamond marker
         * ``'|'``     vline marker
         * ``'_'``     hline marker
     '''

    def __init__(self,variants,**kwargs):
        '''

        '''
        features.BaseGraphicFeature.__init__(self,**kwargs)

        self.variants = variants
        self.start = min(self.variants)
        self.end   = max(self.variants)
        self.marker=kwargs.get('marker','o')
        self.markersize=kwargs.get('markersize',2)



    def draw_feature(self):
        for variant in self.variants:
            self.start, self.end = variant.location.start, variant.location.end
            feat_draw=plt.plot(self.start, self.Y, marker=self.marker, markerfacecolor=self.fc, markeredgecolor='k', markersize=self.markersize, alpha=self.alpha, url = self.url,)
            self.patches=feat_draw



def getVariantsInGene(chr,start,end, path):
    if chr.lower().startswith("chr"):
        chr = chr[3:]
    vcf = pysam.Tabixfile(path)
    variants = vcf.fetch(chr,start,end)
    positions = []
    for line in variants:
        line = line.strip().split("\t")
        ref = line[3]
        alt = line[4]
        start = int(line[1])
        if len(ref) == len(alt) == 1:
            end = start
        else:
            end = start + abs(len(ref)-len(alt))

        positions.append((start,end))
    return positions


def bwQuery(chr,start,end):
    CHUNK = 16 * 1024

    url = urllib2.urlopen("http://hgdownload.cse.ucsc.edu/gbdb/hg19/bbi/All_hg19_RS.bw")
#    fileSize = url.headers['content-length']
    url.headers['Range']= "bytes=0-4" # % (fileSize)
    print url.headers
    f = StringIO.StringIO(url.readline())
    bw = BigWigFile(f)
    print bw.query(chr, start, end, 10)


def connect2UCSC(chr, start, end):
    g = Genome(db="hg19")
    gerp = g.table("allHg19RS_BW")
    bins = g.bins(start,end)
    query  = 'select count(*) from %s where chrom="%s" and chromStart>%d and chromEnd<%d and bin in %s' % (gerp.name, chr, start,end, str(tuple(bins)))
    print query
    

def plotGene(geneName):
    g = Genome(db="hg19")
    gene = g.refGene.filter_by(name2=geneName).first()
    exons = gene.exons
    geneStart = exons[0][0]
    geneEnd   = exons[-1][1]

    exonsLocations = []
    for start, end in sorted(exons):
        feat = SeqFeature.SeqFeature()
        feat.location = SeqFeature.FeatureLocation(int(start),int(end))
        exonsLocations.append(feat)

    panel=Panel(fig_width=900, padding = 25)
    exons_track = tracks.BaseTrack(name = "%s exons", sort_by = None)
    exonsLables = ['' for i in range(1,len(exons)+1)]
    exonDomains = features.DomainFeature(exonsLocations, name = exonsLables, height = 1.5, seq_line = (geneStart,geneEnd))
    exons_track.append(exonDomains)

    vcfPath   = "/Users/Macia/Desktop/DDD/male_trio/DDD_MAIN5194411_merged.vcf.gz"
    variantPositions = getVariantsInGene(chr,geneStart,geneEnd, vcfPath)
    variant_track    = tracks.BaseTrack(name = "%s variants", sort_by = None)
    variantFeatures = []
    for start,end in variantPositions:
        feat = SeqFeature.SeqFeature()
        feat.location = SeqFeature.FeatureLocation(start,end)
        variantFeatures.append(feat)
    variantDomains = VariantFeatures(variantFeatures, marker= 'o',height = 1.5, seq_line = (geneStart,geneEnd))
    variant_track.append(variantDomains)


    panel.add_track(exons_track)
    panel.add_track(variant_track)
    panel.save('/Users/Macia/Desktop/%s.pdf' % geneName)



def bins(start, end):
    if end - start < 536870912:
        offsets = [585, 73, 9, 1, 0]
    else:
        raise Exception("not implemented")
        offsets = [4681, 585, 73, 9, 1, 0]
    binFirstShift = 17
    binNextShift = 3

    start = start >> binFirstShift
    end = (end - 1)  >> binFirstShift

    bins = [1]
    for offset in offsets:
        bins.extend(range(offset + start, offset + end + 1))
        start >>= binNextShift
        end >>= binNextShift
    return frozenset(bins)
    
def getUCSCBins(start,end):
    binOffsets     = [512+64+8+1, 64+8+1, 8+1, 1 ,0]
    _binFirstShift = 17
    _binNextShift  = 3
    
    startBin = start
    endBin   = end - 1
    startBin >>= _binFirstShift
    endBin   >>= _binFirstShift
    
    holder = []
    for bin in binOffsets:
        if startBin == endBin:
            holder.append( bin + startBin)
        startBin >>= _binNextShift
        endBin   >>= _binNextShift
    return tuple(holder)

def createSQLstatment(dbName, chr,start,end):
    bins = getUCSCBins(start,end)
    statment = 'select * from %s where chrom="chr%s" and chromStart>%d and chromEnd<%d and bin in %s;' % (dbName,chr,start,end,str(bins))
    print statment
    
dbName = 'snp129'
chr   = 'chr9'
start = 139414562
end   = 139414571

#print getUCSCBins(start,end)
#print bins(start,end)

#createSQLstatment(dbName, chr,start,end)
#connect2UCSC(chr,start,end)
#bwQuery(chr,start,end)
plotGene("NOTCH1")
    