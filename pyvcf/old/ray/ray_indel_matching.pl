package DDD::Util::Annotation;

use strict;
use warnings FATAL => 'all';

use Sub::Exporter -setup => {
    exports => [
        'is_matching_allele',
        'pos2key'
    ]
};

use DDD::Util::Vcf::UberVcf qw( chr2num );

sub is_matching_allele {
    my ( $ref_a, $alt_a, $ref_b, $alt_b ) = @_;

    if ( $ref_a eq $ref_b && $alt_a eq $alt_b ) {
        # exact match
        return 1;
    }

    my $size_a = length( $ref_a ) - length( $alt_a );
    my $size_b = length( $ref_b ) - length( $alt_b );

    if ( $size_a == 0 || $size_a != $size_b ) {
        # SNV, or indels of different size
        return 0;
    }

    # Comparing indels is tricky as the ref and alt for the same indel
    # may differ depending on the caller. The idea here is that indels
    # such as CTGTGGT => C should match CTGTGGTTG => CTG.

    if ( length($ref_a) > length($ref_b) ) {
        ( $ref_a, $alt_a, $ref_b, $alt_b ) = ( $ref_b, $alt_b, $ref_a, $alt_a );
    }

    if ( $size_a < 0 ) {
        # insertions of same size
        ( my $slice   = $alt_a ) =~ s/^$ref_a//;
        ( my $remains = $alt_b ) =~ s/$slice//; # Hmm, we could return a false negative if $slice appears more than once in $alt_b
        return $remains && $remains eq $ref_b;
    }

    if ( $size_a > 0 ) {
        # deletions of same size
        ( my $slice   = $ref_a ) =~ s/^$alt_a//;
        ( my $remains = $ref_b ) =~ s/$slice//; # Again, possible false negative if $slice appears more than once in $ref_b
        return $remains && $remains eq $alt_b;
    }

    return 0;
}

sub pos2key {
    my ( $chr, $pos ) = @_;

    sprintf( '%02d%09d', chr2num($chr), $pos );
}

1;

__END__
