#!/usr/bin/env perl

use strict;
use warnings FATAL => 'all';

use Test::Most;
use Const::Fast;
use DDD::Util::Annotation qw( is_matching_allele pos2key );

const my @MATCHING_ALLELE_TESTS => (
    [ 'A',       'T',      'A',         'T'     => 1, 'exact match snv'        ],
    [ 'A',       'T',      'A',         'G'     => 0, 'mismatch snv'           ],
    [ 'ATT',     'AT',     'ATT',       'AT'    => 1, 'exact match 1-base del' ],
    [ 'AT',      'ATT',    'AT',        'ATT'   => 1, 'exact match 1-base ins' ],
    [ 'ATT',     'AT',     'ATTT',      'ATT'   => 1, 'slice match 1-base del' ],
    [ 'AT',      'ATT',    'ATT',       'ATTT'  => 1, 'slice match 1-base ins' ],
    [ 'AT',      'A',      'AT',        'AT'    => 0, 'mismatch indel'         ],
    [ 'AAA',     'TTT',    'AAA',       'TTT'   => 1, 'match tri-nucleotide'   ],
    [ 'ATT',     'AT',     'ATT',       'AA'    => 0, 'mismatch indel'         ],
    [ 'CTGTGGT', 'C',      'CTGTGGTTG', 'CTG'   => 1, 'slice match 6-base del' ],
    [ 'TG',      'TTAG',   'T',         'TTA'   => 1, 'match slice'            ],
    [ 'TA',      'TTCAAA', 'T',         'TTCAA' => 1, 'match slice'            ],
    [ 'TG',      'TAG',    'T',         'TA'    => 1, 'match slice'            ],
    [ 'GCCC',    'GGCCCC', 'G',         'GGC'   => 1, 'match slice'            ],
);


for ( @MATCHING_ALLELE_TESTS ) {
    my ( $ref_a, $alt_a, $ref_b, $alt_b, $expect_true, $desc ) = @{$_};
    my $res = is_matching_allele( $ref_a, $alt_a, $ref_b, $alt_b );
    ok $expect_true ? $res : ! $res, $desc;

    $res = is_matching_allele( $ref_b, $alt_b, $ref_a, $alt_a );
    ok $expect_true ? $res : !$res, "$desc should be transitive";

    $res = is_matching_allele( $alt_a, $ref_a, $alt_b, $ref_b );
    ok $expect_true ? $res : !$res, "$desc should succeed when REF and ALT are transposed";

    $res = is_matching_allele( $alt_b, $ref_b, $alt_a, $ref_a );
    ok $expect_true ? $res : !$res, "$desc sohuld be transitive when REF and ALT are transposed";
}

const my @POS2KEY_TESTS => (
    [ '1',  10583,     '01000010583' ],
    [ '1',  249239465, '01249239465' ],
    [ '10', 10583,     '10000010583' ],
    [ 'X',  10583,     '23000010583' ],
    [ 'Y',  249239465, '24249239465' ]
);

for ( @POS2KEY_TESTS ) {
    my ( $chr, $pos, $expected ) = @{$_};

    is pos2key($chr, $pos), $expected, "pos2key $chr $pos => $expected";
}

done_testing();
