'''
Given a mulitVCF file, print chr,pos,ref,alt when shared in all samples.

This script will be used to detected shared regions in the Amish 8 Spina Bifda pateints.

11 Feb 2013
sa9@sanger.ac.uk
'''

import sys
import pyVCFtoolbox as tlbx

def parseVariants(f):#, genotype):
    samplesIds = []
    for line in f:
        if not line.startswith("#"):
            line    = line.strip().split("\t")
            chrom   = line[0]
            pos     = line[1]
            ref     = line[3]
            alts    = tlbx.getAlts(line[4])
            FILTER  = line[6]
            FORMAT1 = line[8]
            if tlbx.isPass(FILTER):
                for alt in alts:
                    CQ, GN, OKG_AF, \
                    COHORT_AF, ESP_AF, \
                    dbSNP137, GERP, \
                    PolyPhen, SIFT = tlbx.getInfo(['VCQ','VGN','1KG_AF',
                                                'UK10K_cohort_AF','ESP_AF',
                                                'dbSNP137','GERP',
                                                'PolyPhen','SIFT'],line[7])
                    genotypes = []
                    for sampleIdx, FORMAT2 in enumerate(line[9:]):
                        GT = tlbx.getGT(FORMAT1,FORMAT2)
                        genotypes.append(GT)
                #if genotype == "HET":
                    TotalHetCount  = genotypes.count(1)
                #elif genotype == "HOM":
                    TotalHomCount  = genotypes.count(2) 
                #if TotalCount == len(samplesIds):
                    newLine = []
                    newLine.extend(line[:4])
                    newLine.append(alt)
                    newLine.extend(line[5:7])
                    newLine.extend([CQ, GN, OKG_AF, COHORT_AF, ESP_AF, dbSNP137, GERP, PolyPhen, SIFT,TotalHetCount, TotalHomCount])
                    newLine.extend([str(x) for x in genotypes])
                    print "\t".join([str(x) for x in newLine])
        elif line.startswith("#CHROM"):
            header = []
            line    = line.strip().split("\t")
            header.extend(line[:7])
            header.extend(['VCQ','VGN','1KG_AF',
                        'UK10K_cohort_AF','ESP_AF',
                        'dbSNP137','GERP',
                        'PolyPhen','SIFT',
                        'totalHetCount', 'totalHomCount'])
            samplesIds.extend(line[9:])
            header.extend(samplesIds)
            print "\t".join(header)

def main():
    f = tlbx.openFile(sys.argv[1])#'/Users/Macia/Desktop/Crosby_Merged/new_AF/singleVCF/corsby_merged_newAF.vcf.gz')#
    #genotype = sys.argv[2]
    parseVariants(f)#, genotype)

if __name__ == '__main__':
    main()