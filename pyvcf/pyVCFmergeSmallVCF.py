# merge 1000s of small vcf files (5% of the exome or 1000 genes)

import pysam
import sys
import os
from collections import OrderedDict


chrList = [str(x) for x in range(1,23)]
chrList.extend(["X","Y"])

header = []

def getSampleId(headers):
    for line in headers:
        if line.startswith("#CHROM"):
            sampleId = line.strip().split("\t")[-1]
            break
    return sampleId
    
def doMerge(sampleInfo):
    for chrom in chrList:
        variants   = OrderedDict()
        for sampleId in sorted(sampleInfo):
            path = sampleInfo[sampleId]
            f = pysam.Tabixfile(path)
            try:
                lines = f.fetch(chrom)
            except:
                lines = []
            for line in lines:
                line = line.strip().split("\t")
                chrom = line[0]
                pos   = line[1]
                ref   = line[3]
                alt   = line[4]
                fltr  = line[6]
                frmt  = line[9]
                if fltr == "PASS":
                    key = (chrom, pos, ref, alt)
                    if not variants.has_key(key):
                        variants[key] = {"basic":line[:9], "formats":{}}
                    variants[key]["formats"][sampleId] = frmt

        sampleIds  = sorted(sampleInfo)
        sortedKeys = {int(key[1]):key for key in variants} # to avoid using vcf-sort later
        for pos in sorted(sortedKeys.keys()):
            key = sortedKeys[pos]
            newLine = variants[key]['basic']
            for sampleId in sampleIds:
                try:
                    newLine.append(variants[key]["formats"][sampleId])
                except:
                    newLine.append("0/0")
            print "\t".join(newLine)

def getSampleInfo(rootDir):
    sampleInfo = {}
    header = []
    for fileName in os.listdir(rootDir):
        if fileName.endswith(".vcf.gz"):
            path = os.path.join(rootDir, fileName)
            f    = pysam.Tabixfile(path)
            if header == []:
                for line in f.header:
                    header.append(line)
                    sampleId = header[-1].strip().split("\t")[-1]
            else:
                sampleId = getSampleId(f.header)
            sampleInfo[sampleId] = path
    print len(sampleInfo)
    return sampleInfo, header

def printHeader(sampleInfo, header):
    for line in header:
        if not line.startswith("#CHROM"):
            print line.strip()
        else:
            line = line.strip().split("\t")[:-1]
            line.extend(sorted(sampleInfo.keys()))
            print "\t".join(line)

def main():
    rootDir = '/Users/Macia/Desktop/UK10K_replication/REL-2013-05-16/single-sample/v1/UK10K_RARE_FIND_WGA'
    sampleInfo, header = getSampleInfo(rootDir)
    printHeader(sampleInfo, header )
    doMerge(sampleInfo)

main()