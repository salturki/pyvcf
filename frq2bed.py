'''
Input a frq file from vcftools and output a bed format file
1) add FROM/TO where both equals the POS in frq file
2) split lines with more than one ALT in the the ALLELE:FREQ 


29 June 2012
sa9@sanger.ac.uk
'''
import gzip, sys
import pyVCFtoolbox as tlbx

f = tlbx.openFile(sys.argv[1]) 

c1 = c2 = 0
for line in f:
    if line.startswith("#CHROM"):
        print "\t".join(['#CHROM','FROM','TO', 'REF', 'ALT', 'FREQ'])
    else:
        line = line.strip().split("\t")
        if len(line) == 6:
            c1 +=1
            CHROM = line[0]
            FROM = int(line[1])
            REF = line[4].split(":")[0]
            ALT,FREQ = line[5].split(":")
            TO = FROM + abs(len(REF)-len(ALT))
            print "\t".join([CHROM,str(FROM),str(TO),REF,ALT,FREQ])
            pass
        elif len(line) > 6:
            c2 += 1
            CHROM = line[0]
            FROM = int(line[1])
            REF = line[4].split(":")[0]
            for colmn in line[5:]:
                ALT,FREQ = colmn.split(":")
                TO = FROM + abs(len(REF)-len(ALT))
                print "\t".join([CHROM,str(FROM),str(TO),REF,ALT,FREQ])
                pass
        else:
            pass
#print c1
#print c3