#!/usr/bin/env python

"""
Annotate BED or VCF file with GERP score for SNVs

9 Mar 2013
sa9@sanger.ac.uk
"""

import pyVCFtoolbox as tlbx
import os
import sys
import argparse



try:
    import pysam
except ImportError:
    try:
        sys.path.append("/nfs/users/nfs_s/sa9/.local/lib/python2.6/site-packages/pysam-0.6-py2.6-linux-x86_64.egg/pysam")
        import pysam
    except ImportError:
        print "pysam module is not installed. Please try to install it and start again."
        print "http://code.google.com/p/pysam/"
        sys.exit(1)

gerpPath = "/Users/Macia/Desktop/gerp/gerp_score.%s.bed.gz"
# gerpPath = "~sa9/chd/resources/gerp++/gerp_score.%s.bed.gz"

def getGerp(chrom,pos,ref,alt):
    if tlbx.getVarType(ref,alt) == 'snv':
        path = os.path.join(gerpPath % chrom)
        f_gerp = pysam.Tabixfile(path)
        lines = f_gerp.fetch(chrom, int(pos)-1, int(pos)+1)
        for line in lines:
            if line is not None:
                line = line.strip().split("\t")
                if line[0] == chrom and line[1] == pos:
                    return line[3]
    return "."


def parseInputFile( f_path, columnIndexes):
    f = tlbx.openFile(f_path)

    isValidColumns = False
    lineCounter = 0
    for line in f:
        lineCounter += 1
        if lineCounter == 1:
            tlbx.isHeaderExists(line)
            f_type  = tlbx.isBEDorVCF(line)
        if not line.startswith("#"):
            line  = line.strip().split("\t")
            if not isValidColumns:
                isValidColumns = tlbx.isValidColumns(columnIndexes,line)
                chrIdx,posIdx,refIdx,altIdx = [int(x) for x in columnIndexes.split(',')]

            chrom = tlbx.removeChrPrefix(line[chrIdx]) #if any
            pos   = line[posIdx]
            ref   = line[refIdx]
            alts  = tlbx.getAlts(line[altIdx])
            for altIdx, alt in enumerate(alts):
                gerpScore = getGerp(chrom,pos,ref,alt)
                if f_type == 'vcf':
                    line[7] = tlbx.annotateInfo(line[7], "GERP", gerpScore, altIdx)
                elif f_type =='bed':
                    line.append(gerpScore)
                print "\t".join(line)
        else:
            #print header for vcf or bed
            if f_type == 'vcf':
                if line.startswith("#CHROM"):
                    print '##INFO=<ID=GERP,Number=.,Type=String,Description="GERP score from ~sa9/chd/resources/gerp_db/hs/">'
                    print line.strip()
                else:
                    print line.strip()
            elif f_type == 'bed':
                line = line.strip().split("\t")
                line.append("GERP")
                print "\t".join(line)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', help='A path to the VCF or BED file or stdin if empty', default=None)
    parser.add_argument('-c', '--columnIndexes', help='A 0-based index for chrom, pos, ref and alt columns'
                                                    ' (comma separated) default is 0,1,3,4', default='0,1,3,4')

    args           = parser.parse_args()
    f_path         = args.inputFile
    columnIndexes  = args.columnIndexes
    if args.inputFile or not sys.stdin.isatty(): # there is inputFile or there is stdin
        parseInputFile(f_path, columnIndexes)
    else:
        parser.print_help()
        sys.exit(0)



