import unittest

import pyVCFtoolbox as tlbx

class TestToolbox(unittest.TestCase):
    def test_removeChrPrefix(self):
        # This function should return the correct chromosome
        self.chrom = tlbx.removeChrPrefix("chr1")
        self.assertEqual(self.chrom, "1", "chr1 should be returned as 1 after removing chr")

    def test_annotateInfo(self):
        # case 1: annotate INFO column with a tag=value when tag is new
        finalInfo = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=.,.,3.1"
        rawInfo   = "AC=1;AF=0.50;DP4=0,2,0,5;"
        tag, value, altIdx = ('GERP',"3.1",2)
        self.info = tlbx.annotateInfo(rawInfo,tag,value,altIdx)
        self.assertEqual(self.info, finalInfo, "annotateInfo failed case 1")

        # case 2: annotate INFO column with a tag=value when tag has less values than the altIdx
        finalInfo = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=.,.,3.1"
        rawInfo   = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=.,."
        tag, value, altIdx = ('GERP',"3.1",2)
        self.info = tlbx.annotateInfo(rawInfo,tag,value,altIdx)
        self.assertEqual(self.info, finalInfo, "annotateInfo failed case 2")

        # case 3: annotate INFO column with a tag=value when tag has already a value that need to be updated
        finalInfo = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=.,3.1,."
        rawInfo   = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=.,2.9,."
        tag, value, altIdx = ('GERP',"3.1",1)
        self.info = tlbx.annotateInfo(rawInfo,tag,value,altIdx)
        self.assertEqual(self.info, finalInfo, "annotateInfo failed case 3")

        # case 4: annotate INFO column with a tag=value when tag has single value but need to be updated to reflect
        # 5 alternative alleles
        finalInfo = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=.,.,.,.,3.1"
        rawInfo   = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=2.9"
        tag, value, altIdx = ('GERP',"3.1",4)
        self.info = tlbx.annotateInfo(rawInfo,tag,value,altIdx)
        self.assertEqual(self.info, finalInfo, "annotateInfo failed case 4")

        # case 5: annotate INFO column with a tag=value when tag is new there is only single alternative allele
        finalInfo = "AC=1;AF=0.50;DP4=0,2,0,5;GERP=3.1"
        rawInfo   = "AC=1;AF=0.50;DP4=0,2,0,5;"
        tag, value, altIdx = ('GERP', "3.1", 0)
        self.info = tlbx.annotateInfo(rawInfo,tag,value,altIdx)
        self.assertEqual(self.info, finalInfo, "annotateInfo failed case 5")

    def test_isValidColumns(self):
        # case 1: good input
        rawLine = ['21','10934273','.','C','T']
        self.isValid = tlbx.isValidColumns("0,1,3,4", rawLine)
        self.assertEqual(self.isValid, True, "isValidColumns failed case 1")

        #case 2: <DEL> in the ALT column
        rawLine = ['21','10934273','.','C','<DEL>']
        self.isValid = tlbx.isValidColumns("0,1,3,4", rawLine)
        self.assertEqual(self.isValid, True, "isValidColumns failed case 2")

        #case 3: wrong input
        rawLine = ['21','WRONG','.','C','<DEL>']
        self.assertRaises(SystemExit,  tlbx.isValidColumns, "0,1,3,4", rawLine)

        #case 4: less than 4 column indexes were supplied
        rawLine = ['21','10934273','.','C','T']
        self.assertRaises(SystemExit,  tlbx.isValidColumns, "0,1,3", rawLine)

        #case 5: same index used more than once
        rawLine = ['21','10934273','.','C','T']
        self.assertRaises(SystemExit,  tlbx.isValidColumns, "0,1,3,3", rawLine)

    def test_deleteInfoTag(self):
        #case 1: normal deletion
        rawInput    = "AC=1;AF=0.50;DP4=0,2,0,5;"
        expectInput = "AC=1;AF=0.50"
        self.returnOutput = tlbx.deleteInfoTag(rawInput,"DP4")
        self.assertEqual(self.returnOutput, expectInput, "deleteInfoTag failed case 1")

        #case 2: nothing to delete but remove the trailing semicolon
        rawInput    = "AC=1;AF=0.50;DP4=0,2,0,5;"
        expectInput = "AC=1;AF=0.50;DP4=0,2,0,5"
        self.returnOutput = tlbx.deleteInfoTag(rawInput,"XX")
        self.assertEqual(self.returnOutput, expectInput, "deleteInfoTag failed case 2")


try:
    unittest.main()
except SystemExit:
    pass